package com.specimen;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.authentication.AppIntroductionActivity;
import com.specimen.cart.CartActivity;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.cart.ICartFacade;
import com.specimen.core.model.CartProduct;
import com.specimen.core.model.Query;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.CartDetailsResponse;
import com.specimen.home.HomeActivity;
import com.specimen.me.ProfileActivity;
import com.specimen.shop.ShopActivity;
import com.specimen.spy.SpyActivity;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "MainActivity";

    protected ImageView me_tab;
    protected static ImageView shop_tab;
    protected static ImageView home_tab;
    protected ImageView spy_tab;
    public static ImageView trunk_tab;

    public static int ME = 0, SHOP = 1, HOME = 2, SPY = 3, TRUNK = 4;

    private View currentSelectedTab;
    private Animation animatorSet;

    static MainActivity mainActivity;
    static TextView lblCartCount;
    static TextView lblSpyCount;
    static TextView lblProductAddedCartView;

    CountDownTimer countDownTimer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainActivity = this;

        setupToolbar();
        animatorSet = AnimationUtils.loadAnimation(this, R.anim.shake);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            int tag = bundle.getInt("tag", -1);

            if (tag != -1) {
                setSelectedTab(home_tab);
                switch (tag) {
                    case 0:
                        me_tab.performClick();
                        break;
                    case 1:
                        shop_tab.performClick();
                        break;
                    case 2:
                        //home_tab.performClick();
                        switchFragment(new HomeActivity(), false, R.id.fragmentContainer);
                        break;
                    case 3:
                        spy_tab.performClick();
                        break;
                    case 4:
                        trunk_tab.performClick();
                        break;
                    default:
                        home_tab.performClick();
                        break;
                }

            }
        } else {

            switchFragment(new HomeActivity(), false, R.id.fragmentContainer);
            setSelectedTab(home_tab);

        }
    }


    public static void pressHomeTabExplicitely () {
        //shop_tab.performClick();
        home_tab.performClick();
    }

    protected void setupToolbar() {
        me_tab = (ImageView) findViewById(R.id.me_tab);
        shop_tab = (ImageView) findViewById(R.id.shop_tab);
        home_tab = (ImageView) findViewById(R.id.home_tab);
        spy_tab = (ImageView) findViewById(R.id.spy_tab);
        trunk_tab = (ImageView) findViewById(R.id.trunk_tab);

        lblCartCount = (TextView) findViewById(R.id.lblCartCount);
        lblSpyCount = (TextView) findViewById(R.id.lblSpyCount);
        lblProductAddedCartView = (TextView) findViewById(R.id.lblProductAddedCartView);

        me_tab.setOnClickListener(this);
        shop_tab.setOnClickListener(this);
        home_tab.setOnClickListener(this);
        spy_tab.setOnClickListener(this);
        trunk_tab.setOnClickListener(this);
    }

    protected void setSelectedTab(View selectedView) {
        currentSelectedTab = selectedView;
        me_tab.setSelected(false);
        shop_tab.setSelected(false);
        home_tab.setSelected(false);
        spy_tab.setSelected(false);
        trunk_tab.setSelected(false);
        currentSelectedTab.setSelected(true);

    }

    @Override
    public void onClick(View v) {

        if (getCurrentSelectedTab().getId() == v.getId()) {
            return;
        }
        Query.getInstance().totalResetQuery();
//        while (getSupportFragmentManager().getBackStackEntryCount() > 0){
//            getSupportFragmentManager().popBackStack();
//        }
        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        switch (v.getId()) {
            case R.id.me_tab:
                switchFragment(new ProfileActivity(), false, R.id.fragmentContainer);
                break;
            case R.id.shop_tab:
                switchFragment(new ShopActivity(), false, R.id.fragmentContainer);
                break;
            case R.id.home_tab:
                switchFragment(new HomeActivity(), false, R.id.fragmentContainer);
                break;
            case R.id.spy_tab:
                switchFragment(new SpyActivity(), false, R.id.fragmentContainer);
                setSpyCount(0);
                break;
            case R.id.trunk_tab:
                switchFragment(new CartActivity(), false, R.id.fragmentContainer);
                setCartCount(0);
                break;
        }

        setSelectedTab(v);
    }

    public View getCurrentSelectedTab() {
        return currentSelectedTab;
    }

    @Override
    protected void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
        recursiveApiCall();
    }

    @Override
    protected void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (null != countDownTimer) {
            countDownTimer.cancel();
        }
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {

        if (response instanceof CartDetailsResponse) {
            if (tag.equals(TAG)) {
                timerSettings((CartDetailsResponse) response);
            } else {
                if (!trunk_tab.isSelected())
                    trunk_tab.startAnimation(animatorSet);
            }
        }
    }

    private void timerSettings(CartDetailsResponse cartDetailsResponse) {
        findViewById(R.id.cart_timer).setVisibility(View.GONE);
        if (null != countDownTimer) countDownTimer.cancel();
        if(cartDetailsResponse.getData() != null) {
            if (null != cartDetailsResponse.getData().getProducts() && 0 < cartDetailsResponse.getData().getProducts().size()) {
                List<Double> remainingTimeList = new ArrayList<>(cartDetailsResponse.getData().getProducts().size());
                remainingTimeList.clear();
                final long[] result = new long[1];

                for (CartProduct cartProduct : cartDetailsResponse.getData().getProducts()) {
                    if (!TextUtils.isEmpty(cartProduct.getRemaining_time()) && !("00:00:00").equals(cartProduct.getRemaining_time())) {
                        String spilttedTime = cartProduct.getRemaining_time();
                        String[] spilttedTimeArray = spilttedTime.split(":");
                        spilttedTime = spilttedTimeArray[1] + "." + spilttedTimeArray[2];
                        remainingTimeList.add(Double.valueOf(spilttedTime));
                    }
                }

                findViewById(R.id.cart_timer).setVisibility(View.GONE);
                if (null != countDownTimer) countDownTimer.cancel();

                if (0 < remainingTimeList.size()) {
                    Collections.sort(remainingTimeList);
                    String spilttedTime = String.valueOf(remainingTimeList.get(0));
                    String[] spilttedTimeArray = spilttedTime.split("\\.");
                    long t = (Integer.parseInt(spilttedTimeArray[0]) * 60L) + Integer.parseInt(spilttedTimeArray[1]);
                    result[0] = TimeUnit.SECONDS.toMillis(t);

                    if (null != countDownTimer) {
                        findViewById(R.id.cart_timer).setVisibility(View.GONE);
                        countDownTimer.cancel();
                    }

                    findViewById(R.id.cart_timer).setVisibility(View.GONE);
                    if (null != countDownTimer) countDownTimer.cancel();

                    countDownTimer = new CountDownTimer(180000, 1000) { //40000 milli seconds is total time, 1000 milli seconds is time interval

                        public void onTick(long millisUntilFinished) {
                            result[0] = (result[0] - 1000);

                            if (result[0] < 0) {
                                if (null != countDownTimer) countDownTimer.cancel();
                                findViewById(R.id.cart_timer).setVisibility(View.GONE);
                            } else {

                                ((TextView) findViewById(R.id.cart_timer)).setText("" + String.format("%02d:%02d",
                                        TimeUnit.MILLISECONDS.toMinutes(result[0]),
                                        TimeUnit.MILLISECONDS.toSeconds(result[0]) -
                                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(result[0]))
                                ));
                                findViewById(R.id.cart_timer).setVisibility(View.VISIBLE);
                            }
                        }

                        public void onFinish() {
                            if (null != countDownTimer) countDownTimer.cancel();
                            findViewById(R.id.cart_timer).setVisibility(View.GONE);
                            recursiveApiCall();
                        }
                    }.start();

                } else {
                    findViewById(R.id.cart_timer).setVisibility(View.GONE);
                    if (null != countDownTimer) countDownTimer.cancel();
                }
            } else {
                findViewById(R.id.cart_timer).setVisibility(View.GONE);
                if (null != countDownTimer) countDownTimer.cancel();
            }
        }
        else
        {
            findViewById(R.id.cart_timer).setVisibility(View.GONE);
            if (null != countDownTimer) countDownTimer.cancel();
        }
    }

    public static void recursiveApiCall() {
        ((ICartFacade) IOCContainer.getInstance().getObject(ServiceName.CART_SERVICE, TAG)).getCart();
    }

    public static void setCartCount(int cartCount) {
        if (0 != cartCount) {
            lblCartCount.setVisibility(View.VISIBLE);
            lblCartCount.setText("" + cartCount);
        } else {
            lblCartCount.setVisibility(View.GONE);
        }
    }

    public static void setSpyCount(int spyCount) {

        if (0 != spyCount) {
            lblSpyCount.setVisibility(View.VISIBLE);
            lblSpyCount.setText("" + spyCount);
        } else {
            lblSpyCount.setVisibility(View.GONE);
        }
    }
}
