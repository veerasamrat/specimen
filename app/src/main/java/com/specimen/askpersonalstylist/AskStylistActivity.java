package com.specimen.askpersonalstylist;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.specimen.BaseActivity;
import com.specimen.R;

import java.util.List;

/**
 * Created by root on 1/9/15.
 */
public class AskStylistActivity extends BaseActivity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_personal_stylist);
        switchStylesFragment(PhotoCaptureFragment.newInstance());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        List<Fragment> stylistFragment = getSupportFragmentManager().getFragments();
        Log.d("Stylist", stylistFragment.toString());
        if (getSupportFragmentManager().getBackStackEntryCount() == 0 || getSupportFragmentManager().getBackStackEntryCount() == 2) {
            finish();
        }
    }

    public void switchStylesFragment(Fragment newFragment) {
        String backStateName = newFragment.getClass().getName();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.replace(R.id.fragment_container, newFragment);
        ft.addToBackStack(backStateName);
        ft.commit();
    }

}
