package com.specimen.askpersonalstylist;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.specimen.BaseFragment;
import com.specimen.R;

/**
 * Created by root on 15/9/15.
 */
@SuppressWarnings("ALL")
public class AskStylistEndFragment extends BaseFragment {

    public static AskStylistEndFragment newInstance() {
        return new AskStylistEndFragment();
    }

    public AskStylistEndFragment() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View stylistEndView = inflater.inflate(R.layout.fragment_ask_stylist_end, container, false);
        setToolbar(stylistEndView);

        return stylistEndView;
    }

    void setToolbar(View photoCaptureView) {
        Toolbar toolbar = (Toolbar) photoCaptureView.findViewById(R.id.toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn, null));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn));
        }
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText("ASK A PERSONAL STYLIST");
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setTitle(null);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
    }
}
