package com.specimen.askpersonalstylist;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Scroller;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.BaseFragment;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.askastylist.IAskAStylistFacade;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.AskStylistResponse;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class PhotoCaptureFragment extends BaseFragment {

    public static final int RESULT_LOAD_IMAGE = 100;

    final String TAG = "PhotoCaptureFragment";

    EditText txtEnterCharacters, txtStyleSize;
    TextView lblCharsCount;
    ImageView image1, image2, image3;
    ImageView image_cancel_1, image_cancel_2, image_cancel_3;
    Button btnUpload, btnStyleme;
    Spinner mSpinnerOccassions;
    String imageTag, imageTag1, imageTag2, imageTag3;
    String imageName_1, imageName_2, imageName_3;
    String picturePath;
    String occassion;
    List<String> occassionArray;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment PhotoCaptureFragment.
     */
    public static PhotoCaptureFragment newInstance() {
        return new PhotoCaptureFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View photoCaptureView = inflater.inflate(R.layout.fragment_ask_stylist_photo_upload, container, false);
        setToolbar(photoCaptureView);
        txtStyleSize = (EditText) photoCaptureView.findViewById(R.id.txtStyleSize);
        txtEnterCharacters = (EditText) photoCaptureView.findViewById(R.id.txtEnterCharacters);
        txtEnterCharacters.addTextChangedListener(mTextEditorWatcher);
        txtEnterCharacters.setMovementMethod(new ScrollingMovementMethod());

        txtEnterCharacters.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_ENTER  && event.getAction() == KeyEvent.ACTION_DOWN) {

                    if ( ((EditText)v).getLineCount() >= 4 )
                        return true;
                }

                return false;
            }
        });
        /*txtEnterCharacters.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                // if enter is pressed start calculating
                if (keyCode == KeyEvent.KEYCODE_ENTER
                        && event.getAction() == KeyEvent.ACTION_UP) {

                    // get EditText text
                    String text = ((EditText) v).getText().toString();

                    // find how many rows it cointains
                    int editTextRowCount = text.split("\\n").length;

                    // user has input more than limited - lets do something
                    // about that
                    if (editTextRowCount >= 3) {

                        // find the last break
                        int lastBreakIndex = text.lastIndexOf("\n");

                        // compose new text
                        String newText = text.substring(0, lastBreakIndex);

                        // add new text - delete old one and append new one
                        // (append because I want the cursor to be at the end)
                        ((EditText) v).setText("");
                        ((EditText) v).append(newText);

                    }
                }

                return false;
            }
        });*/

        lblCharsCount = (TextView) photoCaptureView.findViewById(R.id.lblCharsCount);

        image1 = (ImageView) photoCaptureView.findViewById(R.id.image1);
        image2 = (ImageView) photoCaptureView.findViewById(R.id.image2);
        image3 = (ImageView) photoCaptureView.findViewById(R.id.image3);

        image_cancel_1 = (ImageView) photoCaptureView.findViewById(R.id.image_cancel_1);
        image_cancel_2 = (ImageView) photoCaptureView.findViewById(R.id.image_cancel_2);
        image_cancel_3 = (ImageView) photoCaptureView.findViewById(R.id.image_cancel_3);

        image_cancel_1.setOnClickListener(onClickListener);
        image_cancel_2.setOnClickListener(onClickListener);
        image_cancel_3.setOnClickListener(onClickListener);

        btnUpload = (Button) photoCaptureView.findViewById(R.id.btnUpload);
        btnUpload.setOnClickListener(onClickListener);

        btnUpload = (Button) photoCaptureView.findViewById(R.id.btnUpload);
        btnUpload.setOnClickListener(onClickListener);

        btnStyleme = (Button) photoCaptureView.findViewById(R.id.btnStyleme);
        btnStyleme.setOnClickListener(onClickListener);

        mSpinnerOccassions = (Spinner) photoCaptureView.findViewById(R.id.toolbar_spinner);
        ((IAskAStylistFacade) IOCContainer.getInstance().getObject(ServiceName.ASKSTYLIST, TAG)).askastylistOccasion();
        return photoCaptureView;
    }

    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
        if (!TextUtils.isEmpty(imageName_1)) {
            setBitmapFromSDCard(imageName_1, image1);
        }
        if (!TextUtils.isEmpty(imageName_2)) {
            setBitmapFromSDCard(imageName_2, image2);
        }
        if (!TextUtils.isEmpty(imageName_3)) {
            setBitmapFromSDCard(imageName_3, image3);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    private void setToolbar(View photoCaptureView) {
        Toolbar toolbar = (Toolbar) photoCaptureView.findViewById(R.id.toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn, null));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn));
        }
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText("HOW CAN I HELP YOU?");
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setTitle(null);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.image_cancel_1:
                    imageName_1 = null;
                    image1.setImageBitmap(null);
                    image_cancel_1.setVisibility(View.GONE);
                    imageTag1 = null;
                    break;

                case R.id.image_cancel_2:
                    imageName_2 = null;
                    image2.setImageBitmap(null);
                    image_cancel_2.setVisibility(View.GONE);
                    imageTag2 = null;
                    break;

                case R.id.image_cancel_3:
                    imageName_3 = null;
                    image3.setImageBitmap(null);
                    image_cancel_3.setVisibility(View.GONE);
                    imageTag3 = null;
                    break;

                case R.id.btnUpload:
                    if (!TextUtils.isEmpty(imageTag1) && !TextUtils.isEmpty(imageTag2) && !TextUtils.isEmpty(imageTag3)) {
                        Toast.makeText(getActivity(), "Maximum 3 photos can be uploaded.", Toast.LENGTH_LONG).show();
                        return;
                    }

                    if (TextUtils.isEmpty(imageTag3)) {
                        imageTag = (String) image3.getTag();
                        imageTag3 = (String) image3.getTag();
                        openGallery();
                    } else if (TextUtils.isEmpty(imageTag2)) {
                        imageTag = (String) image2.getTag();
                        imageTag2 = (String) image2.getTag();
                        openGallery();
                    } else if (TextUtils.isEmpty(imageTag1)) {
                        imageTag = (String) image1.getTag();
                        imageTag1 = (String) image1.getTag();
                        openGallery();
                    }

                    break;

                case R.id.btnStyleme:
                    if (TextUtils.isEmpty(txtEnterCharacters.getText().toString())) {
                        Toast.makeText(mContext, "Please enter style text.", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (TextUtils.isEmpty(txtStyleSize.getText().toString())) {
                        Toast.makeText(mContext, "Please enter size text.", Toast.LENGTH_SHORT).show();
                        return;
                    }


                    final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

//                    InputMethodManager mgr = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
//                    mgr.showSoftInput(txtEnterCharacters, InputMethodManager.HIDE_IMPLICIT_ONLY);
//
//                    mgr = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
//                    mgr.showSoftInput(txtStyleSize, InputMethodManager.HIDE_IMPLICIT_ONLY);

                  //  if (!TextUtils.isEmpty(imageName_1) || !TextUtils.isEmpty(imageName_2) || !TextUtils.isEmpty(imageName_3)) {
                        ((AskStylistActivity) getActivity())
                                .switchStylesFragment(UserCapturedStyleFragment.newInstance(occassion,
                                        txtEnterCharacters.getText().toString(),
                                        txtStyleSize.getText().toString(), imageName_1, imageName_2, imageName_3));
                  //  } else {
                  //      Toast.makeText(mContext, "Please select at least one image.", Toast.LENGTH_SHORT).show();
                  //  }
                    break;
            }
        }
    };

    /**
     * text editor watcher
     */
    private final TextWatcher mTextEditorWatcher = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            lblCharsCount.setText(String.valueOf(s.length() + " / 200"));
        }

        public void afterTextChanged(Editable s) {
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PhotoCaptureFragment.RESULT_LOAD_IMAGE && resultCode == Activity.RESULT_OK && null != data) {
            setImage(data);
        }
    }

    private Bitmap getBitmap(Intent data) {
        Uri selectedImage = data.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        picturePath = cursor.getString(columnIndex);
        cursor.close();
        return BitmapFactory.decodeFile(picturePath);
    }

    private void setImage(Intent data) {
        if (imageTag.equals(image1.getTag())) {
            image1.setImageBitmap(getBitmap(data));
            image_cancel_1.setVisibility(View.VISIBLE);
            imageName_1 = picturePath;
        } else if (imageTag.equals(image2.getTag())) {
            image2.setImageBitmap(getBitmap(data));
            image_cancel_2.setVisibility(View.VISIBLE);
            imageName_2 = picturePath;
        } else if (imageTag.equals(image3.getTag())) {
            image3.setImageBitmap(getBitmap(data));
            image_cancel_3.setVisibility(View.VISIBLE);
            imageName_3 = picturePath;
        }
    }

    void setBitmapFromSDCard(String imagePath, ImageView imageView) {
        File image = new File(imagePath);
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);
        imageView.setImageBitmap(bitmap);
    }

    void openGallery() {
        Intent photoIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(photoIntent, RESULT_LOAD_IMAGE);
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        super.onSuccess(response, tag);
        if (!tag.equals(TAG)) {
            return;
        }

        AskStylistResponse askStylistResponse = (AskStylistResponse) response;
        List<AskStylistResponse.DataStylistOccassionEntity> dataStylistOccassionEntities = askStylistResponse.getData();
        occassionArray = new ArrayList<>();
        for (AskStylistResponse.DataStylistOccassionEntity dataStylistOccassionEntity : dataStylistOccassionEntities) {
            occassionArray.add(dataStylistOccassionEntity.getLabel());
            for (int i = 0; i < occassionArray.size(); i++) {
                occassionArray.set(i, occassionArray.get(i).toUpperCase());
            }
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item, occassionArray);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerOccassions.setAdapter(dataAdapter);
        mSpinnerOccassions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                occassion = parent.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}