package com.specimen.askpersonalstylist;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.BaseFragment;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.address.IAddressFacade;
import com.specimen.core.askastylist.IAskAStylistFacade;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.AskStylistImagesData;
import com.specimen.core.response.AskStylistResponse;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 9/9/15.
 */
public class UserCapturedStyleFragment extends BaseFragment {

    final String TAG = "UserCapturedStyleFragment";

    String image_1, image_2, image_3;
    Button btnStyleme;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment PhotoCaptureFragment.
     */
    public static UserCapturedStyleFragment newInstance(String occassion, String userEnteredText,
                                                        String size, String image_1, String image_2, String image_3) {
        UserCapturedStyleFragment fragment = new UserCapturedStyleFragment();
        Bundle args = new Bundle();
        args.putString("occassion", occassion);
        args.putString("userEnteredText", userEnteredText);
        args.putString("size", size);
        args.putString("image_1", image_1);
        args.putString("image_2", image_2);
        args.putString("image_3", image_3);
        fragment.setArguments(args);
        return fragment;
    }

    public UserCapturedStyleFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View captureStyleView = inflater.inflate(R.layout.fragment_ask_stylist_user_captured_style, container, false);
        setToolbar(captureStyleView);
        ((TextView) captureStyleView.findViewById(R.id.lblUserEnteredStyleText))
                .setText(getArguments().getString("userEnteredText").toUpperCase() + " TO WEAR THIS TO "
                        + getArguments().getString("occassion").toUpperCase() + "\n"
                        + "WITH SIZE " + getArguments().getString("size").toUpperCase());

        btnStyleme = (Button) captureStyleView.findViewById(R.id.btnStyleme);
        btnStyleme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

                btnStyleme.setEnabled(false);
                btnStyleme.setClickable(false);
                btnStyleme.setVisibility(View.GONE);

                image_1 = getArguments().getString("image_1");
                image_2 = getArguments().getString("image_2");
                image_3 = getArguments().getString("image_3");

                InputMethodManager mgr = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.showSoftInput(captureStyleView, InputMethodManager.HIDE_IMPLICIT_ONLY);

                ((IAskAStylistFacade) IOCContainer.getInstance().getObject(ServiceName.ASKSTYLIST, TAG))
                        .askastylist(getArguments().getString("occassion"), createStylistImageEntityList(), getArguments().getString("userEnteredText"),
                                getArguments().getString("size"));
            }
        });
        return captureStyleView;
    }

    private void setToolbar(View captureStyleView) {
        Toolbar toolbar = (Toolbar) captureStyleView.findViewById(R.id.toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn, null));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn));
        }
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText("SUMMARY OF YOUR REQUEST");
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setTitle(null);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        super.onSuccess(response, tag);
        if (!tag.equals(TAG)) {
            return;
        }

        ((AskStylistActivity) getActivity()).switchStylesFragment(AskStylistEndFragment.newInstance());
    }

    @Override
    public void onFailure(Exception error) {
        super.onFailure(error);
        btnStyleme.setEnabled(true);
        btnStyleme.setClickable(true);
        btnStyleme.setVisibility(View.VISIBLE);
    }

    private List<AskStylistImagesData.AskStylistImagesEntity> createStylistImageEntityList() {
        AskStylistImagesData stylistImagesData = new AskStylistImagesData();
        AskStylistImagesData.AskStylistImagesEntity stylistImagesEntity;
        List<AskStylistImagesData.AskStylistImagesEntity> stylistImagesEntityList = new ArrayList<>(3);
        if (!TextUtils.isEmpty(image_1)) {
            stylistImagesEntity = stylistImagesData.new AskStylistImagesEntity();
            stylistImagesEntity.setFilename(getSpilttedStringImageName(image_1));
            stylistImagesEntity.setImage_code(getBASE64Image(image_1));
            stylistImagesEntityList.add(stylistImagesEntity);
        }

        if (!TextUtils.isEmpty(image_2)) {
            stylistImagesEntity = stylistImagesData.new AskStylistImagesEntity();
            stylistImagesEntity.setFilename(getSpilttedStringImageName(image_2));
            stylistImagesEntity.setImage_code(getBASE64Image(image_2));
            stylistImagesEntityList.add(stylistImagesEntity);
        }

        if (!TextUtils.isEmpty(image_3)) {
            stylistImagesEntity = stylistImagesData.new AskStylistImagesEntity();
            stylistImagesEntity.setFilename(getSpilttedStringImageName(image_3));
            stylistImagesEntity.setImage_code(getBASE64Image(image_3));
            stylistImagesEntityList.add(stylistImagesEntity);
        }

        return stylistImagesEntityList;
    }

    private String getSpilttedStringImageName(String filePath) {
        String[] splitPicturePath = filePath.split("/");
        return splitPicturePath[splitPicturePath.length - 1];
    }

    private String getBASE64Image(String imageName) {
        Bitmap bm = shrinkBitmap(imageName, 100, 100);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 30, baos); //bm is the bitmap object
        byte[] byteArrayImage = baos.toByteArray();
        return Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
    }

    Bitmap shrinkBitmap(String file, int width, int height) {

        BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
        bmpFactoryOptions.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

        int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight / (float) height);
        int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth / (float) width);

        if (heightRatio > 1 || widthRatio > 1) {
            if (heightRatio > widthRatio) {
                bmpFactoryOptions.inSampleSize = heightRatio;
            } else {
                bmpFactoryOptions.inSampleSize = widthRatio;
            }
        }

        bmpFactoryOptions.inJustDecodeBounds = false;
        bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
        return bitmap;
    }
}
