package com.specimen.authentication;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.specimen.BaseActivity;
import com.specimen.MainActivity;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.widget.CirclePageIndicator;
import com.squareup.picasso.Picasso;

/**
 * Created by root on 11/8/15.
 */
public class AppIntroductionActivity extends BaseActivity {

    final String PREF_KEY_APP_INTRO = "APP_INTRO";
    final int[] mIntroPageArray = {R.drawable.home_bulletin_banners_01, R.drawable.home_bulletin_banners_02, R.drawable.home_bulletin_banners_03, R.drawable.home_bulletin_banners_04, R.drawable.home_bulletin_banners_05, R.drawable.home_bulletin_banners_06};
    final int TOTAL_APP_INTRO_PAGES_COUNT = mIntroPageArray.length;

    Button mIntroPageDone, mBtnIntroPageSkip;
    ImageButton mBtnIntroPageNext;
    ViewPager mPager;

    int mPageCounter;
    private Handler handler;
    private Runnable runnable;
    private final long delayTime = 3000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appintro);

        final IApplicationFacade applicationFacade = ((IApplicationFacade) IOCContainer.getInstance().getObject(ServiceName.APPLICATION_SERVICE, ""));

        if (applicationFacade.getOfflineResponse(PREF_KEY_APP_INTRO) != null) {
            startActivity(new Intent(AppIntroductionActivity.this, SelectAuthenticationProcessActivity.class));
            finish();
        } else {
            if (applicationFacade.getUserResponse() != null) {
                Intent intent = new Intent();
                intent.setClass(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        }


        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.app_intro_pager);
        ImageAdapter adapter = new ImageAdapter(this);
        mPager.setAdapter(adapter);
        CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.circle_pageIndicator);
        indicator.setViewPager(mPager);
        indicator.setSnap(true);

        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mPageCounter = position;
                if ((TOTAL_APP_INTRO_PAGES_COUNT - 1) == mPageCounter) {
                    mBtnIntroPageNext.setVisibility(View.GONE);
                    mBtnIntroPageSkip.setVisibility(View.GONE);
                    mIntroPageDone.setVisibility(View.VISIBLE);
                } else {
                   // mBtnIntroPageNext.setVisibility(View.VISIBLE);
                    //mBtnIntroPageSkip.setVisibility(View.VISIBLE);
                    mIntroPageDone.setVisibility(View.GONE);

                    handler.removeCallbacks(runnable);
                    handler.postDelayed(runnable, delayTime);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mBtnIntroPageSkip = (Button) findViewById(R.id.btnIntroPageSkip);
        mBtnIntroPageSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                applicationFacade.setOfflineResponse(PREF_KEY_APP_INTRO, "done");
                startActivity(new Intent(AppIntroductionActivity.this, SelectAuthenticationProcessActivity.class));
                finish();
            }
        });

        mBtnIntroPageNext = (ImageButton) findViewById(R.id.btnIntroPageNext);
        mBtnIntroPageNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPager.setCurrentItem(++mPageCounter, true);
                if ((TOTAL_APP_INTRO_PAGES_COUNT - 1) == mPageCounter) {
                    mBtnIntroPageNext.setVisibility(View.GONE);
                    mIntroPageDone.setVisibility(View.VISIBLE);
                } else {
                    mBtnIntroPageNext.setVisibility(View.VISIBLE);
                    mIntroPageDone.setVisibility(View.GONE);
                }
            }
        });

        mIntroPageDone = (Button) findViewById(R.id.btnIntroPageDone);
        mIntroPageDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                applicationFacade.setOfflineResponse(PREF_KEY_APP_INTRO, "done");
                startActivity(new Intent(AppIntroductionActivity.this, SelectAuthenticationProcessActivity.class));
                finish();
            }
        });

        handler = new Handler();
        runnable = new Runnable() {
            public void run() {
                if( mPageCounter == 5){
                     handler.removeCallbacks(runnable);
                }else {

                    mPager.setCurrentItem(mPageCounter + 1, true);
                    handler.postDelayed(runnable, delayTime);
                }
            }
        };
       // handler.postDelayed(runnable, 10000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        handler.removeCallbacks(runnable);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(mPageCounter <5)
        {
            handler.postDelayed(runnable, delayTime);
        }
    }

    public class ImageAdapter extends PagerAdapter {

        Context context;

        ImageAdapter(Context context) {
            this.context = context;
        }

        @Override
        public int getCount() {
            return mIntroPageArray.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ImageView imageView = new ImageView(context);
            imageView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT));
            //imageView.setScaleType(ImageView.ScaleType.);
            //imageView.setImageResource(mIntroPageArray[position]);
            imageView.setBackgroundResource(mIntroPageArray[position]);
            //imageView.setBackgroundColor(getResources().getColor(R.color.seventy_percent_transparent));
            container.addView(imageView);
            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((ImageView) object);
        }
    }
}
