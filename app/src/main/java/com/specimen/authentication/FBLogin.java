package com.specimen.authentication;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.application.IApplicationFacade;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;


/**
 * Created by root on 6/9/15.
 */
public class FBLogin {

    Activity mActivity;

    LoginManager fbLoginManager;
    CallbackManager callbackManager;

    public FBLogin(Activity activity) {
        this.mActivity = activity;
    }

    public void initFBLogin() {
        fbLoginManager.logInWithReadPermissions(mActivity, Arrays.asList("public_profile", "user_friends", "email"));
    }

    public CallbackManager getCallbackManager() {
        return callbackManager;
    }

    public void signInWithFB() {
        FacebookSdk.sdkInitialize(mActivity);
        fbLoginManager = LoginManager.getInstance();
        callbackManager = CallbackManager.Factory.create();
        fbLoginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                fbLoginManager = LoginManager.getInstance();
                AccessToken.getCurrentAccessToken();
//                Toast.makeText(mActivity, "Facebook login success...", Toast.LENGTH_SHORT).show();

                if (mActivity instanceof SelectAuthenticationProcessActivity) {
                    ((SelectAuthenticationProcessActivity) mActivity).mProgressView.setAnimation();
                    ((SelectAuthenticationProcessActivity) mActivity).mProgressView.setVisibility(View.VISIBLE);
                } else if (mActivity instanceof UserAuthenticationActivity) {
                    ((UserAuthenticationActivity) mActivity).mProgressView.setAnimation();
                    ((UserAuthenticationActivity) mActivity).mProgressView.setVisibility(View.VISIBLE);
                }
                GraphRequest graphRequest = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
                        try {
                            if (mActivity instanceof SelectAuthenticationProcessActivity) {
                                ((SelectAuthenticationProcessActivity) mActivity).mProgressView.setVisibility(View.GONE);
                            } else if(mActivity instanceof UserAuthenticationActivity) {
                                ((UserAuthenticationActivity) mActivity).mProgressView.setVisibility(View.GONE);
                            }
                            String email = jsonObject.getString("email");
                            String name = jsonObject.getString("name");
                            final String fbid = jsonObject.getString("id");
                            String imageURL = "https://graph.facebook.com/" + fbid + "/picture?type=large";
                            ((IApplicationFacade) IOCContainer.getInstance().getObject(ServiceName.APPLICATION_SERVICE, "FBLogin"))
                                    .setOfflineResponse("profile_img_url", imageURL);
                            String fbAccessToken = AccessToken.getCurrentAccessToken().getToken();

                            if (mActivity instanceof SelectAuthenticationProcessActivity) {
                                ((SelectAuthenticationProcessActivity) mActivity).socialLoginAPI(SelectAuthenticationProcessActivity.FACEBOOK_LOGIN,
                                        email, name, fbAccessToken);
                            } else if (mActivity instanceof UserAuthenticationActivity) {
                                ((UserAuthenticationActivity) mActivity).socialLoginAPI(SelectAuthenticationProcessActivity.FACEBOOK_LOGIN,
                                        email, name, fbAccessToken);
                            }
                        } catch (JSONException e) {
                            AccessToken.setCurrentAccessToken(null);
                            e.printStackTrace();
                        }
                    }
                });
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {
                Toast.makeText(mActivity, "Log in canceled by user.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException e) {
                Toast.makeText(mActivity, "Error occurred while signing in.", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
