package com.specimen.authentication;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.util.StoreAndShareProductBitmapUtil;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by root on 6/9/15.
 */
public class GooglePlusLogin implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    /* RequestCode for resolutions involving sign-in */
    public static final int RC_SIGN_IN = 1;

    /* RequestCode for resolutions to get GET_ACCOUNTS permission on M */
    public static final int RC_PERM_GET_ACCOUNTS = 2;

    /* Keys for persisting instance variables in savedInstanceState */
    public static final String KEY_IS_RESOLVING = "is_resolving";
    public static final String KEY_SHOULD_RESOLVE = "should_resolve";

    // Logcat tag
    private static final String TAG = "GooglePlusLogin";

    // Profile pic image size in pixels
    private static final int PROFILE_PIC_SIZE = 400;

    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;

    /**
     * A flag indicating that a PendingIntent is in progress and prevents us
     * from starting further intents.
     */
    public boolean mIntentInProgress;

    public boolean mSignInClicked;

    private ConnectionResult mConnectionResult;

    private Activity mActivity;

    private String mAccessToken;

    // [START resolution_variables]
    /* Is there a ConnectionResult resolution in progress? */
    public boolean mIsResolving = false;

    /* Should we automatically resolve ConnectionResults when possible? */
    public boolean mShouldResolve = false;
    // [END resolution_variables]

    public GooglePlusLogin(Activity activity) {
        this.mActivity = activity;
    }

    public GoogleApiClient getGoogleApiClient() {
        // [START create_google_api_client]
        // Build GoogleApiClient with access to basic profile
        return mGoogleApiClient = new GoogleApiClient.Builder(mActivity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .addScope(Plus.SCOPE_PLUS_PROFILE)
                .build();
        // [END create_google_api_client]
    }

    /**
     * Sign-in into google
     */
    public void signInWithGplus() {
        // User clicked the sign-in button, so begin the sign-in process and automatically
        // attempt to resolve any errors that occur.
        mShouldResolve = true;
        mGoogleApiClient.connect();
    }

    // [START on_connected]
    @Override
    public void onConnected(Bundle bundle) {
        // onConnected indicates that an account was selected on the device, that the selected
        // account has granted any requested permissions to our app and that we were able to
        // establish a service connection to Google Play services.
        Log.d(TAG, "onConnected:" + bundle);
        mShouldResolve = false;

        // Get user's information
        getProfileInformation();
    }
    // [END on_connected]

    @Override
    public void onConnectionSuspended(int i) {
        // The connection to Google Play services was lost. The GoogleApiClient will automatically
        // attempt to re-connect. Any UI elements that depend on connection to Google APIs should
        // be hidden or disabled until onConnected is called again.
        Log.w(TAG, "onConnectionSuspended:" + i);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // Could not connect to Google Play Services.  The user needs to select an account,
        // grant permissions or resolve an error in order to sign in. Refer to the javadoc for
        // ConnectionResult to see possible error codes.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);

        if (!mIsResolving && mShouldResolve) {
            if (connectionResult.hasResolution()) {
                try {
                    connectionResult.startResolutionForResult(mActivity, RC_SIGN_IN);
                    mIsResolving = true;
                } catch (IntentSender.SendIntentException e) {
                    Log.e(TAG, "Could not resolve ConnectionResult.", e);
                    mIsResolving = false;
                    mGoogleApiClient.connect();
                }
            } else {
                // Could not resolve the connection result, show the user an
                // error dialog.
                showErrorDialog(connectionResult);
            }
        } else {
            // Show the signed-out UI
        }
    }

    private void showErrorDialog(ConnectionResult connectionResult) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(mActivity);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(mActivity, resultCode, RC_SIGN_IN,
                        new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                mShouldResolve = false;
//                                showSignedOutUI();
                            }
                        }).show();
            } else {
                Log.w(TAG, "Google Play Services Error:" + connectionResult);
                String errorString = apiAvailability.getErrorString(resultCode);
                Toast.makeText(mActivity, errorString, Toast.LENGTH_SHORT).show();

                mShouldResolve = false;
            }
        }
    }

    /**
     * Fetching user's information name, email, profile pic
     */
    public void getProfileInformation() {
        try {
            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
                final String personName = currentPerson.getDisplayName();
                String personPhotoUrl = currentPerson.getImage().getUrl();
                String personGooglePlusProfile = currentPerson.getUrl();
                String email = Plus.AccountApi.getAccountName(mGoogleApiClient);

                Log.e(TAG, "Name: " + personName + ", plusProfile: "
                        + personGooglePlusProfile + ", email: " + email
                        + ", Image: " + personPhotoUrl);
                getGooglePlausAccessToken(personName, email);
                // by default the profile url gives 50x50 px image only
                // we can replace the value with whatever dimension we want by
                // replacing sz=X
                personPhotoUrl = personPhotoUrl.substring(0, personPhotoUrl.length() - 2) + PROFILE_PIC_SIZE;
                ((IApplicationFacade) IOCContainer.getInstance().getObject(ServiceName.APPLICATION_SERVICE, TAG))
                        .setOfflineResponse("profile_img_url", personPhotoUrl);
                new LoadProfileImage().execute(personPhotoUrl);
            } else {
                Log.d(TAG, "Person information is null");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to resolve any signin errors
     */
    private void resolveSignInError() {
        if (null != mConnectionResult && mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(mActivity, RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }

    private void getGooglePlausAccessToken(final String personName, final String email) {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... params) {
                final String SCOPES = "https://www.googleapis.com/auth/plus.login ";

                try {
                    mAccessToken = GoogleAuthUtil.getToken(
                            mActivity,
                            Plus.AccountApi.getAccountName(mGoogleApiClient),
                            "oauth2:" + SCOPES);
                } catch (IOException | GoogleAuthException e) {
                    e.printStackTrace();
                }
                return mAccessToken;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if (mActivity instanceof SelectAuthenticationProcessActivity) {
                    ((SelectAuthenticationProcessActivity) mActivity).socialLoginAPI(SelectAuthenticationProcessActivity.GOOGLE_LOGIN,
                            email, personName, mAccessToken);
                } else if (mActivity instanceof UserAuthenticationActivity) {
                    ((UserAuthenticationActivity) mActivity).socialLoginAPI(SelectAuthenticationProcessActivity.GOOGLE_LOGIN,
                            email, personName, mAccessToken);
                }
            }
        }.execute();

    }

    /**
     * Revoking access from google
     */
    private void revokeGplusAccess() {
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            Plus.AccountApi.revokeAccessAndDisconnect(mGoogleApiClient)
                    .setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status arg0) {
                            Log.e(TAG, "User access revoked!");
                            mGoogleApiClient.connect();
                        }

                    });
        }
    }

    /**
     * Background Async task to load user profile picture from url
     */
    private class LoadProfileImage extends AsyncTask<String, Void, Bitmap> {
        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            StoreAndShareProductBitmapUtil bitmapUtil = new StoreAndShareProductBitmapUtil(mActivity);
            bitmapUtil.storeImage(result, false);
            String imageName = bitmapUtil.getStoreImageName();
        }
    }
}
