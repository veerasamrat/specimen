package com.specimen.authentication;

import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.specimen.BaseActivity;
import com.specimen.MainActivity;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.authentication.IAuthenticationFacade;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.LoginBannerResponse;
import com.specimen.core.response.UserResponse;
import com.specimen.home.HomeActivity;
import com.specimen.survey.SurveyActivity;
import com.specimen.ui.fragments.UserSignupSigninFragment;
import com.specimen.widget.ProgressView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Tejas on 6/2/2015.
 * Base Activity containg login, signup, forgot password, t&c, privacy policy fragment.
 */

public class UserAuthenticationActivity extends BaseActivity {

    final String TAG = "UserAuthenticationActivity";
    ProgressView mProgressView;

    // Facebook Login
    CallbackManager callbackManager;
    FBLogin fbLogin;

    // Google plus login
    GoogleApiClient mGoogleApiClient;
    GooglePlusLogin gPlusLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.userauthentication_activity);
        findViewById(R.id.userAuthBackButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mProgressView = (ProgressView) findViewById(R.id.progressView);
        initPreference();
    }

    @Override
    public void onBackPressed() {

        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            Intent intent = new Intent();
            intent.setClass(UserAuthenticationActivity.this, SelectAuthenticationProcessActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
//            finish();
        } else {
            super.onBackPressed();
        }
    }

    void initPreference() {
        IApplicationFacade applicationFacade = (IApplicationFacade) IOCContainer.getInstance().getObject(ServiceName.APPLICATION_SERVICE, "");
        if (applicationFacade.getUserResponse() != null) {
            Intent intent = new Intent();
            intent.setClass(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else {
            int loginProcess = getIntent().getIntExtra("LOGIN", 0);

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.userauthentication_container,
                            new UserSignupSigninFragment(loginProcess)).commit();

            initFB();
            initGPLUS();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    // [START on_save_instance_state]
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(GooglePlusLogin.KEY_IS_RESOLVING, gPlusLogin.mIsResolving);
        outState.putBoolean(GooglePlusLogin.KEY_SHOULD_RESOLVE, gPlusLogin.mShouldResolve);
    }
    // [END on_save_instance_state]

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent data) {
        callbackManager.onActivityResult(requestCode, responseCode, data);

        if (requestCode == GooglePlusLogin.RC_SIGN_IN) {
            // If the error resolution was not successful we should not resolve further.
            if (responseCode != RESULT_OK) {
                gPlusLogin.mShouldResolve = false;
            }

            gPlusLogin.mIsResolving = false;
            mGoogleApiClient.connect();
        }
    }
    // [END on_activity_result]

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult:" + requestCode);
        if (requestCode == GooglePlusLogin.RC_PERM_GET_ACCOUNTS) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                gPlusLogin.getProfileInformation();
            } else {
                Log.d(TAG, "GET_ACCOUNTS Permission Denied.");
            }
        }
    }

    void initFB() {
        fbLogin = new FBLogin(this);
        fbLogin.signInWithFB();
        callbackManager = fbLogin.getCallbackManager();
    }

    void initGPLUS() {
        gPlusLogin = new GooglePlusLogin(this);
        mGoogleApiClient = gPlusLogin.getGoogleApiClient();
    }

    public FBLogin getFbLogin() {
        return fbLogin;
    }

    public GooglePlusLogin getGPlusLogin() {
        return gPlusLogin;
    }

    public void socialLoginAPI(final String loginType, final String email, final String firstName, final String accessToken) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(UserAuthenticationActivity.this);
        String deviceId = sharedPreferences.getString("gcm_device_id", "");
        ((IAuthenticationFacade) IOCContainer.getInstance()
                .getObject(ServiceName.AUTHENTICATION_SERVICE, TAG))
                .socialLogin(loginType, email, firstName, accessToken, deviceId);
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        if (!tag.equals(TAG)) {
            return;
        }

        if (response instanceof UserResponse) {
            UserResponse userResponse = (UserResponse) response;
            if ("Failure".equals(userResponse.getStatus())) {
                Toast.makeText(UserAuthenticationActivity.this, "" + userResponse.getMessage(), Toast.LENGTH_SHORT).show();
            } else {
                Intent intent = new Intent(UserAuthenticationActivity.this, SurveyActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        }
    }

    @Override
    public void onFailure(Exception error) {

    }

}