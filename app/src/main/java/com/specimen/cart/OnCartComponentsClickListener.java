package com.specimen.cart;

/**
 * Created by Swapnil on 8/13/2015.
 */
public interface OnCartComponentsClickListener {

    void onQuantityUpdateListener(String productId, String quantity, String childId);

    void onProductDelete(String productId, String parentProductId);

    void onApplyPromocode(String promocode);

//    void checkout();

    void onRefreshCart();
}
