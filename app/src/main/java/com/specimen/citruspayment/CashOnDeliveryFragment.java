package com.specimen.citruspayment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.BaseFragment;
import com.specimen.MainActivity;
import com.specimen.R;
import com.specimen.captcha.Captcha;
import com.specimen.captcha.TextCaptcha;
import com.specimen.core.IOCContainer;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.ServiceName;
import com.specimen.core.citruspayment.ICitrusFacade;
import com.specimen.core.model.Address;
import com.specimen.core.model.CreateOrderResponse;
import com.specimen.core.response.APIResponse;
import com.specimen.widget.ProgressView;

import org.w3c.dom.Text;

/**
 * Created by DhirajK on 12/4/2015.
 */
public class CashOnDeliveryFragment extends BaseFragment implements View.OnClickListener, IResponseSubscribe {

    private Button btnPayOnDelivery;
    private TextView tvYouPay;
    private String TAG = "CashOnDeliveryFragment";
    private ProgressView progressBar;
    private Bundle bundle;
    private ImageView imgCaptcha;
    private TextView txtChangeImg;
    private EditText edtTxtCaptcha;
    private String captchaAns;
    private TextView txtWrongCaptcha;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_cod, container, false);

        bundle = getArguments();
        SetAmountAddressData setAmountAddressData = new SetAmountAddressData();
        setAmountAddressData.setAmountAndAdreesDataToView(rootView, bundle);

        progressBar = (ProgressView) rootView.findViewById(R.id.progressBar);
        btnPayOnDelivery = (Button) rootView.findViewById(R.id.btnPayOnDelivery);
        tvYouPay = (TextView) rootView.findViewById(R.id.tvYouPay);
        imgCaptcha = (ImageView)rootView.findViewById(R.id.imgCaptcha);
        txtChangeImg = (TextView)rootView.findViewById(R.id.txtChangeImage);
        edtTxtCaptcha = (EditText)rootView.findViewById(R.id.editTxtCaptcha);
        txtWrongCaptcha = (TextView)rootView.findViewById(R.id.txtWrongCaptcha);

        tvYouPay.setText(mContext.getString(R.string.rupee_symbole) + " " + bundle.getString("TOTAL"));
        btnPayOnDelivery.setOnClickListener(this);
//        Address address = bundle.getParcelable("address");
        if (bundle != null) {
            if (bundle.getBoolean("isCashOnDelivery")) {
                btnPayOnDelivery.setEnabled(false);
            } else {
                btnPayOnDelivery.setEnabled(true);
            }
        }
        txtChangeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setCaptcha();
            }
        });

       setCaptcha();
        return rootView;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnPayOnDelivery) {
            if(edtTxtCaptcha.getText().toString().trim().equals(""))
            {
                txtWrongCaptcha.setVisibility(View.VISIBLE);
                txtWrongCaptcha.setText("Please enter the captcha text");
            }
            else if(!edtTxtCaptcha.getText().toString().trim().equals(captchaAns.trim()))
            {
                txtWrongCaptcha.setVisibility(View.VISIBLE);
                txtWrongCaptcha.setText("Wrong code entered, Please try again or change image");
            }
            else {
                Address address = bundle.getParcelable("address");
                ((ICitrusFacade) IOCContainer.getInstance().getObject(ServiceName.PAYMENT_SERVICE, TAG)).createOrder(address.getId(), "cashondelivery");
                progressBar.setVisibility(View.VISIBLE);
                btnPayOnDelivery.setEnabled(false);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        if (TAG.equals(tag)) {
            progressBar.setVisibility(View.GONE);
            btnPayOnDelivery.setEnabled(true);
            if (response instanceof CreateOrderResponse) {
                CreateOrderResponse createOrderResponse = (CreateOrderResponse) response;
                if ((createOrderResponse.getStatus().equalsIgnoreCase("Success"))) {
                    Toast.makeText(getActivity(), "" + response.getMessage(), Toast.LENGTH_SHORT).show();
                    OrderConfirmationDialog orderConfirmationDialog = new OrderConfirmationDialog();
                    Bundle bundle = new Bundle();
                    bundle.putString("txnId", ((CreateOrderResponse) response).getData().getTxnId());
                    orderConfirmationDialog.setArguments(bundle);
                    orderConfirmationDialog.show(getActivity().getSupportFragmentManager(), TAG);
                } else if(createOrderResponse.getStatus().equalsIgnoreCase("0")) {

                    Toast.makeText(getActivity(), "Invalid order.", Toast.LENGTH_SHORT).show();
                    Intent homeIntent = new Intent();
                    homeIntent.setClass(getActivity(), MainActivity.class);
                    homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    getActivity().startActivity(homeIntent);
                }
                else  {
                    Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
                    Intent homeIntent = new Intent();
                    homeIntent.setClass(getActivity(), MainActivity.class);
                    homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    getActivity().startActivity(homeIntent);
                }
            }
        }
    }

    @Override
    public void onFailure(Exception error) {
        Toast.makeText(getActivity(), "" + error.getMessage(), Toast.LENGTH_SHORT).show();
        progressBar.setVisibility(View.GONE);
        btnPayOnDelivery.setEnabled(true);
    }

   public void setCaptcha()
   {
       Captcha c = new TextCaptcha(4, TextCaptcha.TextOptions.NUMBERS_ONLY);
       //Captcha c = new TextCaptcha(300, 100, 5, TextOptions.NUMBERS_AND_LETTERS);
       imgCaptcha.setImageBitmap(c.image);
      // imgCaptcha.setLayoutParams(new LinearLayout.LayoutParams(c.width *2, c.height *2));
       captchaAns = c.answer;
   }
}
