package com.specimen.citruspayment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.MainActivity;
import com.specimen.core.authentication.IAuthenticationFacade;
import com.specimen.core.response.UserResponse;
import com.specimen.widget.ProgressView;
import com.citrus.sdk.Callback;
import com.citrus.sdk.CitrusClient;
import com.citrus.sdk.CitrusUser;
import com.citrus.sdk.TransactionResponse;
import com.citrus.sdk.classes.Amount;
import com.citrus.sdk.classes.CitrusException;
import com.citrus.sdk.classes.Month;
import com.citrus.sdk.classes.Year;
import com.citrus.sdk.payment.CreditCardOption;
import com.citrus.sdk.payment.DebitCardOption;
import com.citrus.sdk.payment.PaymentType;
import com.citrus.sdk.response.CitrusError;
import com.specimen.BaseFragment;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.ServiceName;
import com.specimen.core.model.Address;
import com.specimen.core.response.APIResponse;

/**
 * Created by DhirajK on 12/4/2015.
 */
public class CreditDebitCardFragment extends BaseFragment implements IResponseSubscribe {

    private EditText edtCardNo, edtNameOnCard, edtCVV;
    private TextView txtExpYear;
    private TextView txtExpMonth;
    private Button btnPayNow;
    private CitrusClient citrusClient;
    private DebitCardOption debitCardOption;
    private CreditCardOption creditCardOption;
    private PaymentType.PGPayment pgPayment;
    private Bundle bundle;
    private String TAG = "CreditDebitCardFragment";
    private Address address;
    private ProgressView progressBar;
    Context mContext;
    private TextView tvYouPay;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_debit_credit, container, false);

        bundle = getArguments();
        mContext = rootView.getContext();
        SetAmountAddressData setAmountAddressData = new SetAmountAddressData();
        setAmountAddressData.setAmountAndAdreesDataToView(rootView, bundle);
        address = bundle.getParcelable("address");

        progressBar = (ProgressView) rootView.findViewById(R.id.progressBar);
        edtCardNo = (EditText) rootView.findViewById(R.id.edtCardNo);
        edtNameOnCard = (EditText) rootView.findViewById(R.id.edtNameOnCard);
        txtExpMonth = (TextView) rootView.findViewById(R.id.edtExpMonth);
        txtExpMonth.setTag(0);
        txtExpMonth.setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));
        txtExpYear = (TextView) rootView.findViewById(R.id.edtExpYear);
        txtExpYear.setTag(0);
        txtExpYear.setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));
        edtCVV = (EditText) rootView.findViewById(R.id.edtCVV);
        tvYouPay = (TextView) rootView.findViewById(R.id.tvYouPay);
        btnPayNow = (Button) rootView.findViewById(R.id.btnPayNow);
        tvYouPay.setText(mContext.getString(R.string.rupee_symbole) + " " + bundle.getString("TOTAL"));
        btnPayNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((IAuthenticationFacade) IOCContainer.getInstance()
                        .getObject(ServiceName.AUTHENTICATION_SERVICE, TAG)).getUserId();
            }


        });
        //ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),android.R.layout.simple_spinner_item,getResources().getStringArray(R.array.month_option));
        txtExpMonth.setText("Expiry Month");
        txtExpMonth.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               txtExpYear.setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));
               txtExpMonth.setBackgroundResource(R.drawable.black_border_iiner_white);
               showDialog(txtExpMonth,getResources().getStringArray(R.array.month_option));
           }
       });

        txtExpYear.setText("Expiry Year");
        txtExpYear.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                txtExpMonth.setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));
                txtExpYear.setBackgroundResource(R.drawable.black_border_iiner_white);
                showDialog(txtExpYear,getResources().getStringArray(R.array.year_option));
            }
        });
        return rootView;
    }

    private void showDialog(final TextView txtView, String[] array) {

        final Dialog dialog = new Dialog(getActivity(),android.R.style.Theme_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.custom_dialog_layout);
        ListView listView = (ListView)dialog.findViewById(R.id.listView);
        final CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(getActivity(),array);
        listView.setAdapter(adapter);
        dialog.show();
        adapter.setRemainingFalse((int)txtView.getTag());
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
               txtView.setTag(position);
                dialog.dismiss();
                txtView.setText(adapter.getItem(position));
            }
        });

    }

    private void initiatePayment(String cardType, String billUrl) {
        progressBar.setVisibility(View.GONE);
        citrusClient = CitrusClient.getInstance(mContext); // Activity Context
        citrusClient.enableLog(CitrusConstants.enableLogging);
        citrusClient.init(CitrusConstants.SIGNUP_ID, CitrusConstants.SIGNUP_SECRET, CitrusConstants.SIGNIN_ID, CitrusConstants.SIGNIN_SECRET, CitrusConstants.VANITY, CitrusConstants.environment);

        String tempAmount = bundle.getString("TOTAL");
        String alphaOnly = tempAmount.replaceAll(",", "");
        Amount amount = new Amount(alphaOnly);
        // Init PaymentType
        pgPayment = null;
        // No need to call init on CitrusClient if already done.
        if (cardType.equals("dc")) {
            debitCardOption
                    = new DebitCardOption(edtNameOnCard.getText().toString(), edtCardNo.getText().toString(), edtCVV.getText().toString(), Month.getMonth(txtExpMonth.getText().toString()), Year.getYear(txtExpYear.getText().toString()));
            try {
                pgPayment = new PaymentType.PGPayment(amount, billUrl, debitCardOption,
                        new CitrusUser(CitrusConstants.CITRUS_USER_EMAIL, CitrusConstants.CITRUS_USER_CONTACT));
            } catch (CitrusException e) {
                e.printStackTrace();
            }
        } else {
            // No need to call init on CitrusClient if already done.
            creditCardOption = new CreditCardOption(edtNameOnCard.getText().toString(), edtCardNo.getText().toString(), edtCVV.getText().toString(), Month.getMonth(txtExpMonth.getText().toString()), Year.getYear(txtExpYear.getText().toString()));
            try {
                pgPayment = new PaymentType.PGPayment(amount, billUrl, creditCardOption,
                        new CitrusUser(CitrusConstants.CITRUS_USER_EMAIL, CitrusConstants.CITRUS_USER_CONTACT));
            } catch (CitrusException e) {
                e.printStackTrace();
            }
        }


        citrusClient.pgPayment(pgPayment, new Callback<TransactionResponse>() {
            @Override
            public void success(TransactionResponse transactionResponse) {
                Toast.makeText(mContext, "" + transactionResponse.getMessage(), Toast.LENGTH_SHORT).show();
                redirectToConfirmationFragment("" + transactionResponse.getTransactionDetails().getTransactionId());
            }

            @Override
            public void error(CitrusError error) {
                Toast.makeText(mContext, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
//                Intent homeIntent = new Intent();
//                homeIntent.setClass(getActivity(), MainActivity.class);
//                homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                getActivity().startActivity(homeIntent);
            }
        });

    }


    private void redirectToConfirmationFragment(String s) {

        final OrderConfirmationDialog orderConfirmationDialog = new OrderConfirmationDialog();

        final FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        // transaction.commitAllowingStateLoss();

//        OrderConfirmationDialog orderConfirmationDialog = new OrderConfirmationDialog();
        Bundle bundle1 = new Bundle();
        bundle1.putString("txnId", s);
        orderConfirmationDialog.setArguments(bundle1);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                orderConfirmationDialog.show(transaction, TAG);
            }
        }, 1000);

    }

    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        if (tag.equals(TAG)) {
            if (response instanceof UserResponse) {
//            if (!TextUtils.isEmpty(edtNameOnCard.getText().toString().trim())) {
                String billUrl = CitrusConstants.BILL_URL + "cust_id/" + ((UserResponse) response).getData().getId() + "/addr_id/" + ((Address) bundle.getParcelable("address")).getId() + "/pay_method/" + bundle.getString("cardType");
                initiatePayment(bundle.getString("cardType"), billUrl);
//            }else{
//                    Toast.makeText(mContext, "" + "Please enter valid name.", Toast.LENGTH_SHORT).show();
//                }

            }
        }
    }

    @Override
    public void onFailure(Exception error) {
        progressBar.setVisibility(View.GONE);
    }
}
