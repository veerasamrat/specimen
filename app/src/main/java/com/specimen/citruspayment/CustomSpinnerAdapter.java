package com.specimen.citruspayment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.R;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by Rohan_2 on 2/25/2016.
 */
public class CustomSpinnerAdapter extends BaseAdapter {
  String [] strinArray;
    Context context;
    ArrayList<Boolean>selectedPosition = new ArrayList<>();
    private TextView txtName;
    private RadioButton btn;
    private RelativeLayout spinnerLayout;

    public CustomSpinnerAdapter(Context context,String [] array)
   {
       this.strinArray = array;
       this.context = context;

       for(int i = 0 ; i < strinArray.length ; i++ )
       {
             if(i == 0)
             {
                 selectedPosition.add(i,true);

             }
           else
             {
                 selectedPosition.add(i,false);

             }

       }
   }

    @Override
    public int getCount() {
        return this.strinArray.length;
    }

    @Override
    public String getItem(int position) {
        return strinArray[position];
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }



    @Override
    public View getView(final int position, View view, final ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if(view == null)
        {
            view = inflater.inflate(R.layout.custom_spinner_layout,null,false);
        }

        txtName = (TextView)view.findViewById(R.id.txtName);
        spinnerLayout = (RelativeLayout)view.findViewById(R.id.custom_spinner_layout);
        btn = (RadioButton)view.findViewById(R.id.radioBtn);
        txtName.setText(strinArray[position]);
  /*      spinnerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               if(!selectedPosition.get(position))
                setRemainingFalse(position);
                ((View) parent.getParent().getParent().getParent().getParent()).setVisibility(View.GONE);
            }

        });*/

        if(selectedPosition.get(position))
        {
            btn.setChecked(true);

        }
        else
        {
            btn.setChecked(false);
        }
        return view;
    }

    protected void setRemainingFalse(int position) {

    for (int i =0; i < strinArray.length ; i++)
    {

            selectedPosition.set(i,false);

    }
        selectedPosition.set(position,true);
        notifyDataSetChanged();
    }


}
