package com.specimen.citruspayment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.citrus.sdk.Callback;
import com.citrus.sdk.CitrusClient;
import com.citrus.sdk.CitrusUser;
import com.citrus.sdk.TransactionResponse;
import com.citrus.sdk.classes.Amount;
import com.citrus.sdk.classes.CitrusException;
import com.citrus.sdk.payment.MerchantPaymentOption;
import com.citrus.sdk.payment.NetbankingOption;
import com.citrus.sdk.payment.PaymentType;
import com.citrus.sdk.response.CitrusError;
import com.specimen.BaseFragment;
import com.specimen.MainActivity;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.ServiceName;
import com.specimen.core.authentication.IAuthenticationFacade;
import com.specimen.core.model.Address;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.UserResponse;
import com.specimen.util.ExpandableHeightGridView;
import com.specimen.widget.ProgressView;

import java.util.ArrayList;

/**
 * Created by root on 1/10/15.
 */
public class NetBankingPaymentFragment extends BaseFragment implements IResponseSubscribe {

//    private ArrayList<NetBankingIssuer> netBankingIssuerArrayList;
    private int selectedPosition;
    private Context mContext;
    private Bundle bundle;
    private CitrusClient citrusClient;
    private String TAG = "NetBankingPaymentFragment";
    private ProgressView progressBar;
    private Spinner spinnerBankSelection;
    private ArrayList<NetbankingOption> mNetbankingOptionsList;
    private TextView tvYouPay;
    private ExpandableHeightGridView bankGridView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_netbanking, container, false);

        mContext = rootView.getContext();
        progressBar = (ProgressView) rootView.findViewById(R.id.progressBar);
        tvYouPay = (TextView) rootView.findViewById(R.id.tvYouPay);
        bankGridView = (ExpandableHeightGridView)rootView.findViewById(R.id.gridBank);
        bankGridView.setExpanded(true);
        bundle = getArguments();
        SetAmountAddressData setAmountAddressData = new SetAmountAddressData();
        setAmountAddressData.setAmountAndAdreesDataToView(rootView, bundle);
        BankIconGridAdapter adapter = new BankIconGridAdapter(getActivity());
        bankGridView.setAdapter(adapter);
        tvYouPay.setText(mContext.getString(R.string.rupee_symbole)+" "+bundle.getString("TOTAL"));
//        setAmountData(rootView);
//        NetBankingIssuer netBankingIssuer = new NetBankingIssuer();
//        netBankingIssuerArrayList = netBankingIssuer.getBanks();

        spinnerBankSelection = (Spinner) rootView.findViewById(R.id.spinnerBankSelection);


        spinnerBankSelection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedPosition = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        Button btnPayNow = (Button) rootView.findViewById(R.id.btnNetBankingPayNow);
        btnPayNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((IAuthenticationFacade) IOCContainer.getInstance()
                        .getObject(ServiceName.AUTHENTICATION_SERVICE, TAG)).getUserId();
            }
        });

        initiateCitrusNetBankingsData();

        return rootView;
    }

    public void initiateCitrusNetBankingsData(){
        progressBar.setVisibility(View.VISIBLE);
        citrusClient = CitrusClient.getInstance(mContext); // Activity Context
        citrusClient.enableLog(CitrusConstants.enableLogging);
        citrusClient.init(CitrusConstants.SIGNUP_ID, CitrusConstants.SIGNUP_SECRET, CitrusConstants.SIGNIN_ID, CitrusConstants.SIGNIN_SECRET, CitrusConstants.VANITY, CitrusConstants.environment);

        citrusClient.getMerchantPaymentOptions(new Callback<MerchantPaymentOption>() {
            @Override
            public void success(MerchantPaymentOption merchantPaymentOption) {
                progressBar.setVisibility(View.GONE);
               // mNetbankingOptionsList = excludeMainBanksName(merchantPaymentOption.getNetbankingOptionList());
                mNetbankingOptionsList = merchantPaymentOption.getNetbankingOptionList();

                String[] bankNames = new String[mNetbankingOptionsList.size()];

                for (int index = 0; index < mNetbankingOptionsList.size(); index++) {
                    bankNames[index] = mNetbankingOptionsList.get(index).getBankName();
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                        android.R.layout.simple_spinner_dropdown_item, bankNames);

                spinnerBankSelection.setAdapter(adapter);
            }

            @Override
            public void error(CitrusError error) {
                Toast.makeText(mContext, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
    }

/*    private ArrayList<NetbankingOption> excludeMainBanksName(ArrayList<NetbankingOption> netbankingOptionList) {

        for(int i = 0; i < netbankingOptionList.size(); i++)
        {
            String bankName = mNetbankingOptionsList.get(i).getBankName();

            if(bankName.toLowerCase().contains("axis"))
            {

            }
        }

    return null;
    }*/

    private void redirectToConfirmationFragment(String s) {

        final OrderConfirmationDialog orderConfirmationDialog = new OrderConfirmationDialog();

        final FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        // transaction.commitAllowingStateLoss();

//        OrderConfirmationDialog orderConfirmationDialog = new OrderConfirmationDialog();
        Bundle bundle1 = new Bundle();
        bundle1.putString("txnId", s);
        orderConfirmationDialog.setArguments(bundle1);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                orderConfirmationDialog.show(transaction, TAG);
            }
        }, 1000);

    }

    public void initiatePayment(String cardType, String billUrl) {
        progressBar.setVisibility(View.GONE);

        // No need to call init on CitrusClient if already done.
        NetbankingOption netbankingOption = new NetbankingOption(mNetbankingOptionsList.get(selectedPosition).getBankName(), mNetbankingOptionsList.get(selectedPosition).getBankCID());
        String tempAmount = bundle.getString("TOTAL");
        String alphaOnly = tempAmount.replaceAll(",", "");
        Amount amount = new Amount(alphaOnly);
        // Init PaymentType
        PaymentType.PGPayment pgPayment = null;
        try {
            pgPayment = new PaymentType.PGPayment(amount, billUrl, netbankingOption, new CitrusUser(CitrusConstants.CITRUS_USER_EMAIL, CitrusConstants.CITRUS_USER_CONTACT));
        } catch (CitrusException e) {
            e.printStackTrace();
        }

        citrusClient.pgPayment(pgPayment, new Callback<TransactionResponse>() {
            @Override
            public void success(TransactionResponse transactionResponse) {
//                Toast.makeText(getActivity(), "Transaction Successful : RS." + transactionResponse.getTransactionAmount(), Toast.LENGTH_SHORT).show();
                redirectToConfirmationFragment(transactionResponse.getTransactionDetails().getPgTxnNo());
            }

            @Override
            public void error(CitrusError error) {
                Toast.makeText(mContext, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                Intent homeIntent = new Intent();
                homeIntent.setClass(getActivity(), MainActivity.class);
                homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                getActivity().startActivity(homeIntent);
            }
        });
    }

//    public void setAmountData(View view){
//        TextView itemCountSummary, orderTotalPriceSummary, totalPriceSummary;
//        TextView shippingTo, shippingAddress1, shippingAddress2, shippingAddress3, shippingAddressCity, shippingAddressState, shippingContact;
//
//
//        itemCountSummary = (TextView) view.findViewById(R.id.itemCountSummary);
//        orderTotalPriceSummary = (TextView) view.findViewById(R.id.orderTotalPriceSummary);
//        totalPriceSummary = (TextView) view.findViewById(R.id.totalPriceSummary);
//
//        shippingTo = (TextView) view.findViewById(R.id.shippingTo);
//        shippingAddress1 = (TextView) view.findViewById(R.id.shippingAddress1);
//        shippingAddress2 = (TextView) view.findViewById(R.id.shippingAddress2);
//        shippingAddress3 = (TextView) view.findViewById(R.id.shippingAddress3);
//        shippingAddressCity = (TextView) view.findViewById(R.id.shippingAddressCity);
//        shippingAddressState = (TextView) view.findViewById(R.id.shippingAddressState);
//        shippingContact = (TextView) view.findViewById(R.id.shippingContact);
//
//        String itemCount = bundle.getString("CART_PRODUCT_COUNT");
//        if(1 < Integer.parseInt(itemCount)){
//            itemCount = itemCount + " Item";
//        }else{
//            itemCount = itemCount + " Items";
//        }
//        itemCountSummary.setText(itemCount);
//        orderTotalPriceSummary.setText(mContext.getResources().getString(R.string.rupee_symbole)+ " " +bundle.getString("TOTAL"));
//        totalPriceSummary.setText(mContext.getResources().getString(R.string.rupee_symbole)+ " " +bundle.getString("TOTAL"));
//        Address address = bundle.getParcelable("address");
//        if (address.getIsPrimary()) {
//
//            shippingTo.setText("" + address.getName());
//
//            if (!TextUtils.isEmpty(address.getAddress()))
//                shippingAddress1.setText("" + address.getAddress());
//            else
//                shippingAddress1.setVisibility(View.GONE);
//
//            if (!TextUtils.isEmpty(address.getAddress1()))
//                shippingAddress2.setText("" + address.getAddress1());
//            else
//                shippingAddress2.setVisibility(View.GONE);
//
//            shippingAddress3.setText("" + address.getAddress2() + "\n" + address.getLocality() + " " + address.getLandmark());
//
//            shippingAddressCity.setText("" + address.getDistrict() + " - " + address.getPincode());
//            shippingAddressState.setVisibility(View.GONE);
//            shippingContact.setText("MOBILE: " + address.getMobile_no());
//
//        }
//    }

    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        if (tag.equals(TAG)) {
            if (response instanceof UserResponse) {
//            if (!TextUtils.isEmpty(edtNameOnCard.getText().toString().trim())) {
                String billUrl = CitrusConstants.BILL_URL + "cust_id/" + ((UserResponse) response).getData().getId() + "/addr_id/" + ((Address) bundle.getParcelable("address")).getId() + "/pay_method/" + bundle.getString("cardType");
                initiatePayment(bundle.getString("cardType"), billUrl);
//            }else{
//                    Toast.makeText(mContext, "" + "Please enter valid name.", Toast.LENGTH_SHORT).show();
//                }
            }
        }
    }

    @Override
    public void onFailure(Exception error) {
        progressBar.setVisibility(View.GONE);
    }
}
