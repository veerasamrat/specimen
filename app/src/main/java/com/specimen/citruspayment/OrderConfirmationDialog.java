package com.specimen.citruspayment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.specimen.MainActivity;
import com.specimen.R;
import com.specimen.home.HomeActivity;

/**
 * Created by root on 1/11/15.
 */
public class OrderConfirmationDialog extends DialogFragment {

    AppCompatActivity mActivity;
    private Bundle bundle;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);
    }

    void setToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText("ORDER CONFIRMATION");
        mActivity = (AppCompatActivity) getActivity();
        mActivity.setSupportActionBar(toolbar);
        mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mActivity.getSupportActionBar().setTitle(null);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent homeIntent = new Intent();
                homeIntent.setClass(mActivity, MainActivity.class);
                homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                mActivity.startActivity(homeIntent);
                dismiss();
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.payment_successful_layout, container, false);

        bundle = getArguments();
        ((TextView) view.findViewById(R.id.textView27)).setText(bundle.getString("txnId"));
        setToolbar(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getDialog().setOnKeyListener(new DialogInterface.OnKeyListener()
        {
            @Override
            public boolean onKey(android.content.DialogInterface dialog, int keyCode,
                                 android.view.KeyEvent event) {

                if ((keyCode ==  android.view.KeyEvent.KEYCODE_BACK))
                {

                    Intent homeIntent = new Intent();
                    homeIntent.setClass(mActivity, MainActivity.class);
                    homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    mActivity.startActivity(homeIntent);
                    dismiss();
                    return true;
//                    //This is the filter
//                    if (event.getAction()!= KeyEvent.ACTION_DOWN)
//                        return true;
//                    else
//                    {
//                        //Hide your keyboard here!!!!!!
//                        return true; // pretend we've processed it
//                    }
                }
                else
                    return false; // pass on to be processed as normal
            }
        });
    }
}
