package com.specimen.citruspayment;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.specimen.BaseActivity;
import com.specimen.R;
import com.specimen.core.model.Address;

import org.w3c.dom.Text;

/**
 * Created by Swapnil on 4/14/2015.
 * <p/>
 * This activity has Different payment options and payment process
 */
public class PaymentActivity extends BaseActivity implements View.OnClickListener {

    private PaymentActivityTabAdapter paymentActivityTabAdapter;
    private ViewPager mPaymentPager;
    private LinearLayout llDebit, llCod, llNetbanking, llCredit;
    private Intent paymentType;
    private Bundle bundle;
    private TextView txtCreditCardTab,txtdebitCardTab,txtNetBankingTab,txtCodTab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        setToolbar();

        bundle = getIntent().getExtras();

        if (null == bundle) {
            return;
        }

        mPaymentPager = (ViewPager) findViewById(R.id.pagerPayment);
        llDebit = (LinearLayout) findViewById(R.id.llDebit);
        llCod = (LinearLayout) findViewById(R.id.llCod);
        llNetbanking = (LinearLayout) findViewById(R.id.llNetbanking);
        llCredit = (LinearLayout) findViewById(R.id.llCredit);

        txtCreditCardTab = (TextView)findViewById(R.id.creditCardTab);
        txtCreditCardTab.setTypeface(null,Typeface.BOLD);
        txtdebitCardTab = (TextView)findViewById(R.id.debitCardTab);
        txtNetBankingTab = (TextView)findViewById(R.id.netBankingTab);
        txtCodTab = (TextView)findViewById(R.id.codTab);

        llDebit.setOnClickListener(this);
        llCod.setOnClickListener(this);
        llNetbanking.setOnClickListener(this);
        llCredit.setOnClickListener(this);


        paymentActivityTabAdapter = new PaymentActivityTabAdapter(getSupportFragmentManager(), bundle);
        if(bundle.getString("cod") != null) {
            if (bundle.getString("cod").equalsIgnoreCase("false")) {
                llCod.setVisibility(View.GONE);
                paymentActivityTabAdapter.setSize(3);
            }else{
                paymentActivityTabAdapter.setSize(4);
            }
        }

        mPaymentPager.setAdapter(paymentActivityTabAdapter);

        int paymentType = getIntent().getIntExtra("paymentType", 1);


        mPaymentPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        llDebit.setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));
                        llCod.setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));
                        llNetbanking.setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));
                        llCredit.setBackgroundColor(getResources().getColor(R.color.specimen_grey));

                        txtCreditCardTab.setTypeface(null, Typeface.BOLD);
                        txtdebitCardTab.setTypeface(null, Typeface.NORMAL);
                        txtNetBankingTab.setTypeface(null, Typeface.NORMAL);
                        txtCodTab.setTypeface(null, Typeface.NORMAL);
                        break;
                    case 1:
                        llCod.setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));
                        llDebit.setBackgroundColor(getResources().getColor(R.color.specimen_grey));
                        llNetbanking.setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));
                        llCredit.setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));

                        txtCreditCardTab.setTypeface(null, Typeface.NORMAL);
                        txtdebitCardTab.setTypeface(null, Typeface.BOLD);
                        txtNetBankingTab.setTypeface(null, Typeface.NORMAL);
                        txtCodTab.setTypeface(null, Typeface.NORMAL);
                        break;
                    case 2:
                        llDebit.setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));
                        llCod.setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));
                        llNetbanking.setBackgroundColor(getResources().getColor(R.color.specimen_grey));
                        llCredit.setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));

                        txtCreditCardTab.setTypeface(null, Typeface.NORMAL);
                        txtdebitCardTab.setTypeface(null, Typeface.NORMAL);
                        txtNetBankingTab.setTypeface(null, Typeface.BOLD);
                        txtCodTab.setTypeface(null, Typeface.NORMAL);
                        break;
                    case 3:
                        llCod.setBackgroundColor(getResources().getColor(R.color.specimen_grey));
                        llDebit.setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));
                        llNetbanking.setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));
                        llCredit.setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));

                        txtCreditCardTab.setTypeface(null, Typeface.NORMAL);
                        txtdebitCardTab.setTypeface(null, Typeface.NORMAL);
                        txtNetBankingTab.setTypeface(null, Typeface.NORMAL);
                        txtCodTab.setTypeface(null, Typeface.BOLD);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        selectPaymentTypeFragment(paymentType);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llCod:
                selectPaymentTypeFragment(3);
                break;

            case R.id.llDebit:
                selectPaymentTypeFragment(1);
                break;

            case R.id.llNetbanking:
                selectPaymentTypeFragment(2);
                break;

            case R.id.llCredit:
                selectPaymentTypeFragment(0);
                break;

        }
    }

    void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("SELECT PAYMENT TYPE");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn, null));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn));
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void selectPaymentTypeFragment(int type) {
        switch (type) {
            case 0:
                mPaymentPager.setCurrentItem(0, true);
                setTabBackground(llCredit);
                break;

            case 1:
                mPaymentPager.setCurrentItem(1, true);
                setTabBackground(llDebit);
                break;

            case 2:
                mPaymentPager.setCurrentItem(2, true);
                setTabBackground(llNetbanking);
                break;

            case 3:
                mPaymentPager.setCurrentItem(3, true);
                setTabBackground(llCod);
                break;

        }
    }

    public void refreshPaymentTypes(boolean isCashOnDelivery) {
        llCod.setVisibility(View.GONE);
    }


    private void setTabBackground(LinearLayout selectedTab) {

        llCod.setBackgroundColor(getResources().getColor(R.color.specimen_grey));
        llCredit.setBackgroundColor(getResources().getColor(R.color.specimen_grey));
        llDebit.setBackgroundColor(getResources().getColor(R.color.specimen_grey));
        llNetbanking.setBackgroundColor(getResources().getColor(R.color.specimen_grey));

        selectedTab.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
    }

    @Override
    public void onBackPressed() {
        this.finish();
    }


}
