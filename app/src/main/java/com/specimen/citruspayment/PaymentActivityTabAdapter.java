package com.specimen.citruspayment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by DhirajK on 12/3/2015.
 */
public class PaymentActivityTabAdapter extends FragmentStatePagerAdapter {
    Bundle bundle;
    int size = 4;

    public PaymentActivityTabAdapter(FragmentManager fm, Bundle bundle) {
        super(fm);
        this.bundle = bundle;

    }

    public void setSize(int size){
        this.size = size;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                CreditDebitCardFragment creditCardFragment1 = new CreditDebitCardFragment();
                bundle.putString("cardType", "cc");
                creditCardFragment1.setArguments(bundle);
                return creditCardFragment1;

            case 1:
                CreditDebitCardFragment debitCardFragment = new CreditDebitCardFragment();
                bundle.putString("cardType", "dc");
                debitCardFragment.setArguments(bundle);
                return debitCardFragment;

            case 2:
                NetBankingPaymentFragment netBankingPaymentFragment = new NetBankingPaymentFragment();
                netBankingPaymentFragment.setArguments(bundle);
                return netBankingPaymentFragment;

            case 3:
                CashOnDeliveryFragment cashOnDeliveryFragment = new CashOnDeliveryFragment();
                cashOnDeliveryFragment.setArguments(bundle);
                return cashOnDeliveryFragment;

            default:
                return null;
        }
//        if (position == 0) {
//            CashOnDeliveryFragment cashOnDeliveryFragment = new CashOnDeliveryFragment();
//            return cashOnDeliveryFragment;
//        }

    }

    @Override
    public int getCount() {
        return size;
    }
}
