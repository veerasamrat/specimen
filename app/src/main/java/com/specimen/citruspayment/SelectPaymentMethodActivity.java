package com.specimen.citruspayment;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.specimen.BaseActivity;
import com.specimen.MainActivity;
import com.specimen.R;
import com.specimen.core.model.Address;

import twitter4j.Query;

/**
 * Created by root on 1/10/15.
 */
public class SelectPaymentMethodActivity extends BaseActivity implements View.OnClickListener {

    private Bundle bundle;
    private Address address;
    private TextView totalAmtToPay, itemCountSummary, orderTotalPriceSummary, totalPriceSummary;
    private LinearLayout payByCardLayout, payByCashLayout, payByWalletLayout, payByGiftCardLayout;
    private TextView shippingTo, shippingAddress1, shippingAddress2, shippingAddress3, shippingAddressCity, shippingAddressState, shippingContact;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_step_three);
        setToolbar();
        bundle = getIntent().getExtras();

        if (null == bundle) {
            return;
        }
        getComponents();

    }

    @Override
    public void onBackPressed() {
        Intent trunkIntent = new Intent();
        trunkIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        trunkIntent.setClass(SelectPaymentMethodActivity.this, MainActivity.class);
        trunkIntent.putExtra("tag", MainActivity.TRUNK);
        startActivity(trunkIntent);
        finish();
    }

    void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.lblStepsHeader)).setText("PAYMENT");
        ((TextView) toolbar.findViewById(R.id.lblStepsHeader)).setTextSize(TypedValue.COMPLEX_UNIT_SP,16);
        ((TextView) toolbar.findViewById(R.id.lblStepsCount)).setText("STEP 3/3");

        Toolbar.LayoutParams layoutParams = new Toolbar.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.CENTER;
        toolbar.findViewById(R.id.lblStepsHeader).setLayoutParams(layoutParams);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn, null));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn));
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void getComponents() {
        totalAmtToPay = (TextView) findViewById(R.id.totalAmtToPay);
        itemCountSummary = (TextView) findViewById(R.id.itemCountSummary);
        orderTotalPriceSummary = (TextView) findViewById(R.id.orderTotalPriceSummary);
        totalPriceSummary = (TextView) findViewById(R.id.totalPriceSummary);

        shippingTo = (TextView) findViewById(R.id.shippingTo);
        shippingAddress1 = (TextView) findViewById(R.id.shippingAddress1);
        shippingAddress2 = (TextView) findViewById(R.id.shippingAddress2);
        shippingAddress3 = (TextView) findViewById(R.id.shippingAddress3);
        shippingAddressCity = (TextView) findViewById(R.id.shippingAddressCity);
        shippingAddressState = (TextView) findViewById(R.id.shippingAddressState);
        shippingContact = (TextView) findViewById(R.id.shippingContact);

        payByCardLayout = (LinearLayout) findViewById(R.id.payByCardLayout);
        payByCashLayout = (LinearLayout) findViewById(R.id.payByCashLayout);
        payByWalletLayout = (LinearLayout) findViewById(R.id.payByWalletLayout);
        payByGiftCardLayout = (LinearLayout) findViewById(R.id.payByGiftCardLayout);
        payByCardLayout.setOnClickListener(this);
        payByCashLayout.setOnClickListener(this);
        payByWalletLayout.setOnClickListener(this);
        payByGiftCardLayout.setOnClickListener(this);

        totalAmtToPay.setText(getResources().getString(R.string.rupee_symbole)+ " " +bundle.getString("TOTAL"));
        String itemCount = bundle.getString("CART_PRODUCT_COUNT");
        if(1 < Integer.parseInt(itemCount)){
            itemCount = itemCount + " Item";
        }else{
            itemCount = itemCount + " Items";
        }
        itemCountSummary.setText(itemCount);
        orderTotalPriceSummary.setText(getResources().getString(R.string.rupee_symbole)+ " " +bundle.getString("TOTAL"));
        totalPriceSummary.setText(getResources().getString(R.string.rupee_symbole)+ " " +bundle.getString("TOTAL"));
        address = bundle.getParcelable("address");
        if (address.getIsPrimary()) {

            shippingTo.setText("" + address.getName());

            if (!TextUtils.isEmpty(address.getAddress()))
                shippingAddress1.setText("" + address.getAddress());
            else
                shippingAddress1.setVisibility(View.GONE);

//            if (!TextUtils.isEmpty(address.getAddress1()))
//                shippingAddress2.setText("" + address.getAddress1());
//            else
                shippingAddress2.setVisibility(View.GONE);
//
////                if (!TextUtils.isEmpty(address.getAddress2()))
//            shippingAddress3.setText("" + address.getAddress2() + "\n" + address.getLocality() + " " + address.getLandmark());
//                else
                    shippingAddress3.setVisibility(View.GONE);

            shippingAddressCity.setText("" + address.getCity() + ": " + address.getPincode());
            shippingAddressState.setText("" + address.getState());
            shippingContact.setText("\nMobile: " + address.getMobile_no());

            if(bundle.getString("cod") != null) {
                if (bundle.getString("cod").equalsIgnoreCase("false")) {
                    payByCashLayout.setVisibility(View.GONE);
                }
            }

//                txtName.setText("" + address.getName());
//                String addressString = address.getAddress()
//                        + (TextUtils.isEmpty(address.getAddress1()) ? "" : "\n" + address.getAddress1())
//                        + (TextUtils.isEmpty(address.getAddress2()) ? "" : "\n" + address.getAddress2())
//                        + "\n" + address.getDistrict()
//                        + "\n" + address.getLocality()
//                        + "\n" + address.getLandmark()
//                        + "\n" + address.getPincode();
//                txtAddress.setText(addressString + "\nMOBILE: " + address.getMobile_no());
        }
    }

    @Override
    public void onClick(View view) {
        Intent  intent = new Intent(this, PaymentActivity.class);

        switch (view.getId()) {
            case R.id.payByCardLayout:
                intent.putExtra("paymentType",1);
                intent.putExtras(bundle);
                startActivity(intent);
                break;

            case R.id.payByCashLayout:
                intent.putExtra("paymentType",3);
                intent.putExtras(bundle);
                startActivity(intent);
                break;

            case R.id.payByWalletLayout:
                intent.putExtra("paymentType",2);
                intent.putExtras(bundle);
                startActivity(intent);
                break;

            case R.id.payByGiftCardLayout:
                intent.putExtra("paymentType",0);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
        }
    }
}
