//package com.specimen.citruspayment.paymentaddress;
//
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//
//import com.specimen.BaseFragment;
//import com.specimen.R;
//import com.specimen.core.model.Address;
//import com.specimen.core.response.AddressResponse;
//import com.specimen.me.address.IAddressEditor;
//
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.List;
//
///**
// * Created by root on 19/11/15.
// */
//public class ChangeShippingAddressFragment extends BaseFragment implements IAddressEditor{
//
//    RecyclerView storedAddressList;
//    AddressResponse addressResponse;
//    List<Address> addressList;
//    INavigateChageAddressFragment iChangeAddress;
//
//    private int selectedAddressId = -1;
//
//    @Override
//    public void editAddress(int id) {
//        iChangeAddress.navigateEditAddressFragment(id);
//    }
//
//    @Override
//    public void deleteAddress(int id) {
//        ((ShippingAddressActivity) getActivity()).deleteAddressAPICall(id);
//    }
//
//    @Override
//    public void selectedAddressId(int id) {
//        selectedAddressId = id;
//    }
//
//    public interface INavigateChageAddressFragment {
//        void getSelectedConfirmAddress(int id);
//        void navigateEditAddressFragment(int id);
//    }
//
//    public ChangeShippingAddressFragment (INavigateChageAddressFragment iChangeAddress) {
//        this.iChangeAddress = iChangeAddress;
//    }
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        addressResponse = getArguments().getParcelable("address");
//        if (null != addressResponse) {
//            addressList = addressResponse.getData();
//        }
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View storedAddressView = inflater.inflate(R.layout.fragment_store_address, container, false);
//
//        ((ShippingAddressActivity) getActivity()).setToolbarTitle(R.string.address_hint);
//
//        storedAddressList = (RecyclerView) storedAddressView.findViewById(R.id.list);
//        Collections.sort(addressList, new Comparator<Address>() {
//            @Override
//            public int compare(Address address, Address t1) {
//                if (address.getIsPrimary() || t1.getIsPrimary()) {
//                    return -1;
//                } else {
//                    return 0;
//                }
//            }
//        });
//
//        storedAddressView.findViewById(R.id.imgAddNewAddress).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                iChangeAddress.getSelectedConfirmAddress(selectedAddressId);
//            }
//        });
//
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
//        storedAddressList.setLayoutManager(linearLayoutManager);
//        final ShippingAddressAdapter shippingAddressAdapter = new ShippingAddressAdapter(R.layout.address_store_view, addressList, this);
//        storedAddressList.setAdapter(shippingAddressAdapter);
////        storedAddressView.findViewById(R.id.imgAddNewAddress).setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                iChangeAddress.getSelectedConfirmAddress(shippingAddressAdapter.);
////            }
////        });
//
//        return storedAddressView;
//    }
//}