package com.specimen.citruspayment.paymentaddress;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.BaseActivity;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.address.IAddressFacade;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.LocalityResponse;

/**
 * Created by root on 3/1/16.
 */
public class SelectLocalityActivity extends BaseActivity {

    private final String TAG = "SelectLocalityActivity";

    private String pinCode, city, state, country, locality;
    //private Spinner spinnerLocality;
    private boolean isLocalitySelected;
    private ListView listViewLocality;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_locality);

       // spinnerLocality = (Spinner) findViewById(R.id.spinnerLocality);
        listViewLocality = (ListView)findViewById(R.id.listViewLocality);

        if (!TextUtils.isEmpty(locality = getIntent().getStringExtra("LOCALITY"))) {
            ((TextView) findViewById(R.id.txtLocality)).setText(locality);
            isLocalitySelected = true;
        }

        pinCode = getIntent().getStringExtra("PINCODE");
        if (!TextUtils.isEmpty(pinCode)) {
            ((IAddressFacade) IOCContainer.getInstance().getObject(ServiceName.ADDRESS_SERVICE, TAG)).zipcodeAutoArea(pinCode);
        }

        findViewById(R.id.btnSaveLocality).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locality = ((TextView) findViewById(R.id.txtLocality)).getText().toString();
                if (null != locality && 0 < locality.trim().length()) {
                    Intent intent = new Intent();
                    intent.putExtra("CITY", city);
                    intent.putExtra("STATE", state);
                    intent.putExtra("COUNTRY", country);
                    intent.putExtra("LOCALITY", locality);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    Toast.makeText(SelectLocalityActivity.this, "Please enter valid locality.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    public void onSuccess(final APIResponse response, String tag) {
        super.onSuccess(response, tag);

        if (!tag.equals(TAG)) {
            return;
        }

        if (response instanceof LocalityResponse) {
            if (null != ((LocalityResponse) response).getData() && 0 < ((LocalityResponse) response).getData().getLocality().size()) {
                city = ((LocalityResponse) response).getData().getCity();
                state = ((LocalityResponse) response).getData().getState();
                country = ((LocalityResponse) response).getData().getCountry();
                LocalityAdapter localityAdapter = new LocalityAdapter(this, R.layout.locality_adapter_text, ((LocalityResponse) response).getData().getLocality(),
                        city, state, country);
                listViewLocality.setAdapter(localityAdapter);
                locality = ((LocalityResponse) response).getData().getLocality().get(0).getName();
                listViewLocality.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                      //  if (!isLocalitySelected) {
                            locality = ((LocalityResponse) response).getData().getLocality().get(position).getName();
                            ((TextView) findViewById(R.id.txtLocality)).setText(locality);
                     //   }
                       // isLocalitySelected = false;
                    }
                });

            }
        }
    }

    @Override
    public void onFailure(Exception error) {
        super.onFailure(error);
    }
}
