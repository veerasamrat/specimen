package com.specimen.filter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.Objects;

/**
* Created by Rohit on 10/19/2015.
*/
public abstract class BaseFilterHolder extends RecyclerView.ViewHolder {

    public BaseFilterHolder(View itemView) {
        super(itemView);
    }

    public abstract void getView(Object objects);
}
