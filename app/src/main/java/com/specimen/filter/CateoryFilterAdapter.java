package com.specimen.filter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.specimen.R;
import com.specimen.core.model.TopCategory;
import com.specimen.core.response.CategoryListResponse;
import com.specimen.util.BusProvider;
import com.specimen.widget.AutoAdjustLayout;

import java.util.List;

/**
 * Created by Rohit on 10/19/2015.
 */
public class CateoryFilterAdapter extends BaseFilterAdapter {

    private CategoryListResponse filters;

    public CateoryFilterAdapter(Context context, CategoryListResponse filters) {
        super(context);
        this.filters = filters;
    }


    @Override
    public BaseFilterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_filter_row, null);

        return new FilterHolder(view);
    }


    @Override
    public void onBindViewHolder(BaseFilterHolder holder, int position) {
//        FilterType filter = filters.get(position);
        holder.getView(filters);
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public class FilterHolder extends BaseFilterHolder implements View.OnClickListener {
        TextView lable;
        AutoAdjustLayout adjustLayout;
        CategoryListResponse filterType;

        public FilterHolder(View itemView) {
            super(itemView);
            lable = (TextView) itemView.findViewById(R.id.lable);
            adjustLayout = (AutoAdjustLayout) itemView.findViewById(R.id.autoAdjustLayout);
        }

        public void getView(Object filterType) {

            if (filterType == null) {
                return;
            }

            this.filterType = (CategoryListResponse) filterType;
            lable.setText("Category");

            adjustLayout.removeAllViews();

            LayoutInflater layoutInflater = LayoutInflater.from(context);

            List<TopCategory> topCategoryList = ((CategoryListResponse) filterType).getData();


//            ArrayList<String> values = Query.getInstance().getValues(this.filterType.getCode());
            for (TopCategory topCategory : topCategoryList) {
                TextView btnFilterView = (TextView) layoutInflater.inflate(R.layout.filter_options_view, null);

                btnFilterView.setTag(topCategory);

                Context context = btnFilterView.getContext();
                int textColor;
                Drawable backGroundSelector = null;

                textColor = context.getResources().getColor(R.color.ninety_percent_transparent);
//                backGroundSelector = context.getResources().getDrawable(R.drawable.filter_option);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    backGroundSelector = context.getResources().getDrawable(R.drawable.filter_option, null);
                } else {
                    backGroundSelector = context.getResources().getDrawable(R.drawable.filter_option);
                }

                btnFilterView.setBackground(backGroundSelector);
                btnFilterView.setTextColor(textColor);
                btnFilterView.setText(topCategory.getName());

                btnFilterView.setOnClickListener(this);
                adjustLayout.addView(btnFilterView);
            }

        }

        @Override
        public void onClick(View v) {
            TopCategory filterOptionState = (TopCategory) v.getTag();
            BusProvider.getInstance().post(filterOptionState);
        }
    }
}
