package com.specimen.home;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.specimen.home.bulletin.BulletinFragment;
import com.specimen.home.handpicked.HandpickedFragment;
import com.specimen.home.deal.DealFragment;

/**
 * Created by root on 21/7/15.
 */
public class HomeActivityDealPagerAdapter extends FragmentStatePagerAdapter {


    public HomeActivityDealPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }


    @Override
    public Fragment getItem(int position) {

        if (position == 0)
            return DealFragment.newInstance();
        else if (position == 1)
            return new BulletinFragment();
        else
            return new HandpickedFragment();

    }

    @Override
    public int getCount() {
        return 3;
    }
}

