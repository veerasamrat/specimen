package com.specimen.home.bulletin;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.specimen.R;
import com.specimen.core.model.BulletinData;
import com.specimen.core.model.BulletinEntity;
import com.specimen.core.model.ImageEntity;
import com.specimen.core.model.Look;
import com.specimen.look.LookDetailActivity;
import com.specimen.product.productdetails.ProductDetailsActivity;
import com.specimen.util.ImageUtils;
import com.specimen.widget.CirclePageIndicator;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 20/10/15.
 */
public class BannerBulletinHolder extends BulletinHolder {

    ViewPager mBannerBulletin;
    CirclePageIndicator indicator;

    public BannerBulletinHolder(View itemView) {
        super(itemView);
        this.mBannerBulletin = (ViewPager) itemView.findViewById(R.id.viewPager);
        this.indicator = (CirclePageIndicator) itemView.findViewById(R.id.circle_pageIndicator);

    }

    @Override
    public void setView(BulletinEntity bulletinEntity) {
        //TODO : add ViewPager

        List<BulletinData> imageEntityList = bulletinEntity.getData();
        if (null != imageEntityList && 0 < imageEntityList.size()) {
            mBannerBulletin.setAdapter(new ImageAdapter(mBannerBulletin.getContext(), imageEntityList));
            indicator.setViewPager(mBannerBulletin);
            indicator.setSnap(true);
        } else {
            Toast.makeText(mBannerBulletin.getContext(), "Server encountered an error while retrieving images", Toast.LENGTH_SHORT).show();
        }
    }

    public class ImageAdapter extends PagerAdapter {
        private final DisplayMetrics matrix;
        Context context;
        List<BulletinData> imageEntityList;

        ImageAdapter(Context context, List<BulletinData> imageEntities) {
            this.context = context;
            this.imageEntityList = imageEntities;
            matrix = context.getResources().getDisplayMetrics();
        }

        @Override
        public int getCount() {
            return imageEntityList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            final ImageView imageView = new ImageView(context);
//            imageView.setBackgroundColor(Color.BLUE);
//            Picasso.with(context)
//                    .load(imageEntityList.get(position).image.toString())
//                    .placeholder(R.drawable.pdp_banner_placeholder)
//                    .resize(matrix.widthPixels, 406)
//                    .centerInside()
//                    .into(imageView);
            if (imageEntityList.get(position).images != null) {
                Picasso.with(context)
                        .load(ImageUtils.getProductImageUrl(imageEntityList.get(position).images, matrix))
                        .placeholder(R.drawable.product_list_placeholder)
                        .resize(matrix.widthPixels, (matrix.widthPixels / 4) * 3)
                        .centerInside()
                        .into(imageView);
            } else {
                Picasso.with(context).load(R.drawable.product_list_placeholder).resize(matrix.widthPixels, (matrix.widthPixels / 4) * 3).into(imageView);
            }

            container.addView(imageView, 0);
            imageView.setTag("specimen" + position);
//
//            imageView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    if (imageEntityList.get(position).banner_type.equals("product")) {
//                        Intent intent = new Intent(context, ProductDetailsActivity.class);
//                        intent.putExtra("PRODUCT_ID", String.valueOf(imageEntityList.get(position).id));
//                        context.startActivity(intent);
//                    } else if (imageEntityList.get(position).banner_type.equals("looks")) {
//                        Look lookEntity = new Look();
//                        lookEntity.setName(imageEntityList.get(position).name);
//                        List<ImageEntity> imageEntities = new ArrayList<ImageEntity>();
//                        imageEntities.add(imageEntityList.get(position).images);
//                        lookEntity.setImage(imageEntities);
//                        lookEntity.setId(String.valueOf(imageEntityList.get(position).id));
//                        Intent intent = new Intent(context, LookDetailActivity.class);
////                        Bundle bundle = new Bundle();
////                        bundle.putParcelable("look", lookEntity);
//                        intent.putExtra("look", lookEntity);
//                        context.startActivity(intent);
//                    }
//                }
//            });
            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((ImageView) object);
        }
    }
}
