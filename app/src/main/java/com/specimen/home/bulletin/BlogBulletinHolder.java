package com.specimen.home.bulletin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.specimen.R;
import com.specimen.core.model.BulletinEntity;
import com.squareup.picasso.Picasso;

/**
 * Created by Swapnil on 20/10/15.
 */
public class BlogBulletinHolder extends BulletinHolder {

    //    RecyclerView mRecyclerView;
    TextView tvBlogTitle, tvBlogText;
    DisplayMetrics matrix;
    ImageView ivBlogImage;
    Context mContext;

    public BlogBulletinHolder(View itemView) {
        super(itemView);
//        this.mRecyclerView = (RecyclerView) itemView.findViewById(R.id.list);
        this.ivBlogImage = (ImageView) itemView.findViewById(R.id.ivBlog);
        this.tvBlogText = (TextView) itemView.findViewById(R.id.tvBlogText);
        this.tvBlogTitle = (TextView) itemView.findViewById(R.id.tvTBlogTitle);
        this.mContext = itemView.getContext();
        this.matrix = itemView.getContext().getResources().getDisplayMetrics();
        ;
    }

    @Override
    public void setView(BulletinEntity bulletinEntity) {
//        Picasso.with(mRecyclerView.getContext())
//                .load(imageEntityList.get(position).image.toString())
//                .placeholder(R.drawable.pdp_banner_placeholder)
//                .resize(matrix.widthPixels, 406)
//                .centerInside()
//                .into(imageView);
        if (bulletinEntity.getData().get(0).image != null) {
            Picasso.with(mContext)
                    .load(bulletinEntity.getData().get(0).image)
                    .placeholder(R.drawable.banner_explore_placeholder)
                    .resize(matrix.widthPixels, (matrix.widthPixels / 4) * 3)
                    .centerInside()
                    .into(ivBlogImage);
        } else {
            Picasso.with(mContext).load(R.drawable.banner_explore_placeholder).resize(matrix.widthPixels, (matrix.widthPixels / 4) * 3).into(ivBlogImage);
        }

        ivBlogImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(mContext, BulletinWebActivity.class);
                Bundle bundle = new Bundle();
                // TODO : Add url getting from the server
                bundle.putString("url", "http://www.intelliswift.co.in/");
                intent.putExtras(bundle);
                mContext.startActivity(intent);
            }
        });
    }
}
