package com.specimen.home.bulletin;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.specimen.BaseFragment;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.bulletin.IBulletinFacade;
import com.specimen.core.model.BulletinEntity;
import com.specimen.core.model.Collection;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.BulletinResponseData;
import com.specimen.util.YouTubeWebView;
import com.specimen.widget.ProgressView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by root on 21/7/15.
 */
@SuppressWarnings("ALL")
public class BulletinFragment extends BaseFragment {

    RecyclerView mRecyclerView;
    String TAG = "BULLETIN_FRGMENT";

    ProgressView progressView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bulletin, container, false);
        progressView = (ProgressView) view.findViewById(R.id.progressView);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.list);
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(layoutManager);

        progressView.setAnimation();
        progressView.setVisibility(View.VISIBLE);
        fetch();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
//        fetch();
    }

    private void fetch() {
        ((IBulletinFacade) IOCContainer.getInstance().getObject(ServiceName.BULLETIN_SERVICE, TAG)).bulletin();
    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        super.onSuccess(response, tag);
        Log.d("BUlletin", response.toString());
        progressView.setVisibility(View.GONE);
        if (TAG.equals(tag)) {
            if (response instanceof BulletinResponseData) {

                List<BulletinEntity> entities = ((BulletinResponseData) response).getData();

                if (entities != null) {

                    Collections.sort(entities, new Comparator<BulletinEntity>() {
                        @Override
                        public int compare(BulletinEntity lhs, BulletinEntity rhs) {
                            if (lhs.getPosition() > rhs.getPosition())
                                return 1;
                            return 0;
                        }
                    });

                    BulletinAdapter bulletinAdapter = new BulletinAdapter(mContext, entities);
                    mRecyclerView.setAdapter(bulletinAdapter);
                    bulletinAdapter.notifyDataSetChanged();
                }

                BulletinAdapter bulletinAdapter = new BulletinAdapter(mContext, entities);
                mRecyclerView.setAdapter(bulletinAdapter);
                bulletinAdapter.notifyDataSetChanged();
            }
        }
    }
}

