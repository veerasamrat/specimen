package com.specimen.home.bulletin;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.specimen.R;
import com.specimen.core.model.BulletinEntity;

/**
 * Created by root on 20/10/15.
 */
public abstract class BulletinHolder extends RecyclerView.ViewHolder {

//    RecyclerView mRecyclerView;

    public BulletinHolder(View itemView) {
        super(itemView);
    }

    public abstract void setView(BulletinEntity bulletinEntity);

}
