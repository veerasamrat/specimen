package com.specimen.home.bulletin;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import com.specimen.BaseActivity;
import com.specimen.R;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.response.APIResponse;

/**
 * Created by Swapnil on 11/20/2015.
 *
 *  This Activity displays product listing by category
 */


public class BulletinProductListingActivity extends BaseActivity implements IResponseSubscribe{

    RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bulletin_prod_listing);
        mRecyclerView = (RecyclerView) findViewById(R.id.rvProductListing);
        mRecyclerView.setHasFixedSize(true);
        MyLinearLayoutManager myLinearLayoutManager = new MyLinearLayoutManager(BulletinProductListingActivity.this, LinearLayoutManager.VERTICAL, false);
//        mRecyclerView.setLayoutManager(new GridLayoutManager(mContext,2,LinearLayoutManager.HORIZONTAL,false));
        mRecyclerView.setLayoutManager(myLinearLayoutManager);
//        BulletinProductsListingAdapter bestSEllerAdpater = new BulletinProductsListingAdapter(BulletinProductListingActivity.this, R.layout.bulletin_productgrid_item, bulletinEntity.getData());
//        mRecyclerView.setAdapter(bestSEllerAdpater);
        setToolbar();
    }

    void setToolbar () {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText(getResources().getString(R.string.title_activity_product_list));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn, null));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn));
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
    }

    @Override
    public void onFailure(Exception error) {
    }
}
