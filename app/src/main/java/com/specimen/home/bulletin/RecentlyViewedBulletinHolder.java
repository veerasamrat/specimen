package com.specimen.home.bulletin;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.specimen.R;
import com.specimen.core.model.BulletinEntity;

/**
 * Created by root on 20/10/15.
 */
public class RecentlyViewedBulletinHolder extends BulletinHolder {

    RecyclerView mRecyclerView;

    public RecentlyViewedBulletinHolder(View itemView) {
        super(itemView);
        this.mRecyclerView = (RecyclerView) itemView.findViewById(R.id.list);
    }

    @Override
    public void setView(BulletinEntity bulletinEntity) {

    }
}
