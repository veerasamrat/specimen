package com.specimen.home.deal;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.specimen.R;
import com.specimen.util.GaugeView;

/**
 * Created by root on 6/10/15.
 */
public class CloseDealHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    TextView tickerProductTitle_closed_deal, text_closed_deal_Spying,
            lbl_closed_deal_TotalViews, lblRemainingStock_closed_deal,
            tickerProductPrice_closed_deal, text_closed_deal_OriginalPrice,
            text_closed_deal_TodaysLow, text_closed_deal_TodayHigh,
            hrtxt_closed_deal, mintxt_closed_deal, sectxt_closed_deal, text_closed_deal_Friends;
    Button btn_closed_deal_buy, btn_closed_deal_spy;
    ImageView tickerBannerImg_closed_deal;

    GaugeView mGaugeView;

    //Just for testing
    private float degree = -225;
    private float sweepAngleControl = 0;
    private float sweepAngleFirstChart = 1;
    private float sweepAngleSecondChart = 1;
    private float sweepAngleThirdChart = 1;

    public CloseDealHolder(View itemView) {
        super(itemView);
        tickerProductTitle_closed_deal = (TextView) itemView.findViewById(R.id.tickerProductTitle_closed_deal);
        text_closed_deal_Spying = (TextView) itemView.findViewById(R.id.text_closed_deal_Spying);
        lbl_closed_deal_TotalViews = (TextView) itemView.findViewById(R.id.lbl_closed_deal_TotalViews);
        lblRemainingStock_closed_deal = (TextView) itemView.findViewById(R.id.lblRemainingStock_closed_deal);
        tickerProductPrice_closed_deal = (TextView) itemView.findViewById(R.id.tickerProductPrice_closed_deal);
        text_closed_deal_OriginalPrice = (TextView) itemView.findViewById(R.id.text_closed_deal_OriginalPrice);
        text_closed_deal_TodaysLow = (TextView) itemView.findViewById(R.id.text_closed_deal_TodaysLow);
        text_closed_deal_TodayHigh = (TextView) itemView.findViewById(R.id.text_closed_deal_TodayHigh);
        hrtxt_closed_deal = (TextView) itemView.findViewById(R.id.hrtxt_closed_deal);
        mintxt_closed_deal = (TextView) itemView.findViewById(R.id.mintxt_closed_deal);
        sectxt_closed_deal = (TextView) itemView.findViewById(R.id.sectxt_closed_deal);
        text_closed_deal_Friends = (TextView) itemView.findViewById(R.id.text_closed_deal_Friends);
        btn_closed_deal_buy = (Button) itemView.findViewById(R.id.btn_closed_deal_buy);
        btn_closed_deal_spy = (Button) itemView.findViewById(R.id.btn_closed_deal_spy);
        tickerBannerImg_closed_deal = (ImageView) itemView.findViewById(R.id.tickerBannerImg_closed_deal);
        mGaugeView = (GaugeView) itemView.findViewById(R.id.tickerGaugeNiddle_closed_deal);
        mGaugeView.setRotateDegree(degree);
    }

    public void setClickListener() {
        btn_closed_deal_buy.setOnClickListener(this);
        btn_closed_deal_spy.setOnClickListener(this);
        super.itemView.setOnClickListener(this);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_closed_deal_buy:
                break;

            case R.id.btn_closed_deal_spy:
                break;
        }
    }

    public void startRunningGauge(final Activity activity, final int totalStock) {
        new Thread() {
            public void run() {

                for (int i = 0; i < totalStock; i++) {
                    try {
                            activity.runOnUiThread(new Runnable() {

                               @Override
                               public void run() {
                                   degree++;
                                   sweepAngleControl++;
                                   if (degree < 45) {
                                       mGaugeView.setRotateDegree(degree);
                                   }

                                   if (sweepAngleControl <= 25) {
                                       sweepAngleFirstChart++;
                                       mGaugeView.setSweepAngleFirstChart(sweepAngleFirstChart);
                                   } else if (sweepAngleControl <= 50) {
                                       sweepAngleSecondChart++;
                                       mGaugeView.setSweepAngleSecondChart(sweepAngleSecondChart);
                                   } else if (sweepAngleControl <= 100) {
                                       sweepAngleThirdChart++;
                                       mGaugeView.setSweepAngleSecondChart(sweepAngleThirdChart);
                                   }
    //                                else {
    //                                        sweepAngleSecondChart++;
    //                                        mGaugeView.setSweepAngleSecondChart(sweepAngleSecondChart);
    //                                    }

    //                                if (sweepAngleControl <= 90) {
    //                                    sweepAngleFirstChart++;
    //                                    mGaugeView.setSweepAngleFirstChart(sweepAngleFirstChart);
    //                                } else if (sweepAngleControl <= 180) {
    //                                    sweepAngleSecondChart++;
    //                                    mGaugeView.setSweepAngleSecondChart(sweepAngleSecondChart);
    //                                } else if (sweepAngleControl <= 270) {
    //                                    sweepAngleThirdChart++;
    //                                    mGaugeView.setSweepAngleThirdChart(sweepAngleThirdChart);
    //                                }
                               }
                           }
                        );
                        Thread.sleep(15);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }
}