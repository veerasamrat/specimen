package com.specimen.home.deal;

/**
 * Created by root on 7/10/15.
 */
public interface IUpdateTimer {
    void updateTimer(long updatedTimerValue);
}
