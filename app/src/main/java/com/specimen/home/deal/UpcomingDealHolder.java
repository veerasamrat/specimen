package com.specimen.home.deal;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.R;
import com.specimen.core.model.DealsEntity;
import com.specimen.core.util.SharedPreferenceHelper;
import com.specimen.util.NumberFormatsUtil;

/**
 * Created by root on 6/10/15.
 */
public class UpcomingDealHolder extends RecyclerView.ViewHolder implements View.OnClickListener, IUpdateTimer, IDealCloseTimerUpdate {

    Context mContext;
    TextView hrtxt_upcoming_deal, mintxt_upcoming_deal, sectxt_upcoming_deal;
    Button btnNotifyMe;
    DealAdapter dealAdapter;
    private DealsEntity dealsEntity;
    private IDealCloseTimerUpdate mIDealCloseTimerUpdate;

    public UpcomingDealHolder(View itemView, Context context, DealAdapter dealAdapter) {
        super(itemView);
        this.mContext = context;
        this.dealAdapter = dealAdapter;
        hrtxt_upcoming_deal = (TextView) itemView.findViewById(R.id.hrtxt_upcoming_deal);
        mintxt_upcoming_deal = (TextView) itemView.findViewById(R.id.mintxt_upcoming_deal);
        sectxt_upcoming_deal = (TextView) itemView.findViewById(R.id.sectxt_upcoming_deal);
        btnNotifyMe = (Button) itemView.findViewById(R.id.btnNotifyMe);

    }

    public void setDealsEntity(DealsEntity dealsEntity) {
        this.dealsEntity = dealsEntity;
        String notifiedDealId = SharedPreferenceHelper.get(SharedPreferenceHelper.UPCOMING_DELAS, ("DEAL_ID_" + dealsEntity.getId()), "");
        if (! TextUtils.isEmpty(notifiedDealId)) {
            btnNotifyMe.setBackgroundColor(btnNotifyMe.getContext().getResources().getColor(R.color.specimen_background_dark_grey));
            ((Button) btnNotifyMe).setText("NOTIFIED");
        }
    }

    public void setClickListener() {
        btnNotifyMe.setOnClickListener(this);
        super.itemView.setOnClickListener(this);
    }

    public void setDealCloseTimerInterface(IDealCloseTimerUpdate iDealCloseTimerUpdate) {
        this.mIDealCloseTimerUpdate = iDealCloseTimerUpdate;
    }

    @Override
    public void updateTimer(long updatedTimerValue) {
        final String constantTimeString = "00:00:00";
        final String[] remainingTime = (!TextUtils.isEmpty(dealsEntity.getRemaining_time()) ?
                dealsEntity.getRemaining_time().split(":") : constantTimeString.split(":"));
        long hoursInmillis = Long.parseLong(remainingTime[0]) * (60 * 60 * 1000);
        long minsInMillis = Long.parseLong(remainingTime[1]) * (60 * 1000);
        long secsInMillis = Long.parseLong(remainingTime[2]) * 1000;

        final long dealTimeInMillis = hoursInmillis + minsInMillis + secsInMillis;
        final long updatedTimeInMillis = dealTimeInMillis - (updatedTimerValue * 1000);

        if (NumberFormatsUtil.isNegative(updatedTimeInMillis)) {
            hrtxt_upcoming_deal.setText("00");
            mintxt_upcoming_deal.setText("00");
            sectxt_upcoming_deal.setText("00");
            mIDealCloseTimerUpdate.dealCloseTimerUpdate("upcoming");
        } else {
            setTimerAnimation(updatedTimeInMillis);
        }
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnNotifyMe) {
            String notifiedDealId = SharedPreferenceHelper.get(SharedPreferenceHelper.UPCOMING_DELAS, ("DEAL_ID_" + dealsEntity.getId()), "");
            if (TextUtils.isEmpty(notifiedDealId)) {
                SharedPreferenceHelper.put(SharedPreferenceHelper.UPCOMING_DELAS,  ("DEAL_ID_" + dealsEntity.getId()), dealsEntity.getId());
                Toast.makeText(mContext, "We will notify when deal starts.", Toast.LENGTH_LONG).show();
//                dealAdapter.updateDeals();
                ((Button) btnNotifyMe).setText("NOTIFIED");
                v.setBackgroundColor(btnNotifyMe.getContext().getResources().getColor(R.color.specimen_background_dark_grey));
            }
        }
    }

    @Override
    public void dealCloseTimerUpdate(String status) {

    }

    void setTimerAnimation(long updatedTimeInMillis) {

        final long sec = (updatedTimeInMillis / 1000) % 60;
        final long hr = (updatedTimeInMillis / (1000 * 60 * 60));
        final long min = (updatedTimeInMillis / (1000 * 60) % 60);

        sectxt_upcoming_deal.setText("" + String.format("%02d", sec));
        mintxt_upcoming_deal.setText("" + String.format("%02d", min));
        hrtxt_upcoming_deal.setText("" + String.format("%02d", hr));

        ObjectAnimator secObjectAnimator = ObjectAnimator.ofFloat(sectxt_upcoming_deal, "rotationX", 90f, -90.0f);
        secObjectAnimator.setDuration(1000);
        secObjectAnimator.setInterpolator(new LinearInterpolator());
        secObjectAnimator.start();

        if (sec == 59) {

            ObjectAnimator minObjectAnimator = ObjectAnimator.ofFloat(mintxt_upcoming_deal, "rotationX", 90f, 0.0f);
            minObjectAnimator.setDuration(800);
            minObjectAnimator.setInterpolator(new LinearInterpolator());
            minObjectAnimator.start();

        }
        if (min == 59) {

            ObjectAnimator hrObjectAnimator = ObjectAnimator.ofFloat(hrtxt_upcoming_deal, "rotationX", 90f, 0.0f);
            hrObjectAnimator.setDuration(800);
            hrObjectAnimator.setInterpolator(new LinearInterpolator());
            hrObjectAnimator.start();


        }


    }

}
