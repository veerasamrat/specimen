package com.specimen.look;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.BaseActivity;
import com.specimen.MainActivity;
import com.specimen.R;
import com.specimen.askpersonalstylist.AskStylistActivity;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.look.ILookFacade;
import com.specimen.core.model.Look;
import com.specimen.core.model.Product;
import com.specimen.core.product.IProductFacade;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.AddOrRemoveFromWishListResponse;
import com.specimen.core.response.CartDetailsResponse;
import com.specimen.core.response.LookDetailsResponse;
import com.specimen.core.response.SpyAndCartItemCountResponse;
import com.specimen.core.spyandcartitemcount.ISpyAndCartItemCountFacade;
import com.specimen.product.ProductHolder;
import com.specimen.trustcircle.TrustCircleActivity;
import com.specimen.util.ImageUtils;
import com.specimen.util.StoreAndShareProductBitmapUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Rohit on 8/24/2015.
 */
public class LookDetailActivity extends BaseActivity {

    private View progressView;
    private RecyclerView productRecyclerView;
    private ArrayList<Product> productsList;
    private String TAG = "LookDetailActivity";
    private Look looks;
    private LookDetailProductListingAdapter lookDetailProductListingAdapter;
    private ImageView share;
    private TextView lblCartCount;
    private Animation animatorSet;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_look_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn, null));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn));
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        animatorSet = AnimationUtils.loadAnimation(this, R.anim.shake);

        lblCartCount = (TextView) findViewById(R.id.lblCartCount);
//        trunk = (ImageView) findViewById(R.id.imgTrunk);

        share = (ImageView) findViewById(R.id.imgShare);
        looks = getIntent().getParcelableExtra("look");
        productsList = new ArrayList<>();
        lookDetailProductListingAdapter = new LookDetailProductListingAdapter(this, productsList);
        progressView = findViewById(R.id.progressView);
        productRecyclerView = (RecyclerView) findViewById(R.id.product_recycler_view);
        productRecyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        productRecyclerView.setLayoutManager(layoutManager);
        productRecyclerView.setAdapter(lookDetailProductListingAdapter);

        progressView.setVisibility(View.VISIBLE);
        findViewById(R.id.imbAskStylist).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LookDetailActivity.this, AskStylistActivity.class);
                startActivity(intent);
            }
        });

        buttonAskMate();
        navigateToTrunk();

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != looks) {
                    StoreAndShareProductBitmapUtil bitmapUtil = new StoreAndShareProductBitmapUtil(LookDetailActivity.this);
                    if (null != lookDetailProductListingAdapter.getLookHolder()) {
                        Bitmap bitmap = ((BitmapDrawable) lookDetailProductListingAdapter.getLookHolder().lookImage.getDrawable()).getBitmap();
                        bitmapUtil.storeImage(bitmap, true);
                    }
                    ((IProductFacade) IOCContainer.getInstance().getObject(ServiceName.PRODUCT_SERVICE, "productsharecount")).productsharecount(looks.getId());
                } else {
                    Toast.makeText(LookDetailActivity.this, "Server encountered an error while loading data.", Toast.LENGTH_LONG).show();
                }
            }
        });

        fetchDetail();
        ((ISpyAndCartItemCountFacade) IOCContainer.getInstance().getObject(ServiceName.SPYANDCART_ITEM_COUNT_SERVICE, TAG)).getTotalSpyCartCount();
    }

    void navigateToTrunk() {
        findViewById(R.id.imgTrunk).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent trunkIntent = new Intent();
                trunkIntent.setClass(LookDetailActivity.this, MainActivity.class);
                trunkIntent.putExtra("tag", MainActivity.TRUNK);
                startActivity(trunkIntent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
//        productsList.clear();
//        fetchDetail();
    }

    @Override
    protected void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    void buttonAskMate() {
        ImageView askMateButton = (ImageView) findViewById(R.id.imgAskaMate);
        askMateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent askMateIntent = new Intent(LookDetailActivity.this, TrustCircleActivity.class);
                askMateIntent.putExtra("productId", looks.getId());
                startActivity(askMateIntent);
            }
        });
    }

    private void fetchDetail() {
//        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
        ((ILookFacade) IOCContainer.getInstance().getObject(ServiceName.LOOK_SERVICE, TAG)).fetchLookDetail(looks.getId());
        if (progressView != null) {
            progressView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {

        if (progressView != null) {
            progressView.setVisibility(View.GONE);
        }

        if (response instanceof LookDetailsResponse) {
            LookDetailsResponse productListResponse = (LookDetailsResponse) response;
            productsList.addAll(productListResponse.getData().getProducts());
            lookDetailProductListingAdapter.setHeader(looks);
            lookDetailProductListingAdapter.notifyDataSetChanged();
        }

        if (response instanceof CartDetailsResponse) {
//            Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
            findViewById(R.id.imgTrunk).startAnimation(animatorSet);
            lblCartCount.setVisibility(View.VISIBLE);
            lblCartCount.setText("" + ((CartDetailsResponse) response).getData().getshoppingcart_count());
            MainActivity.setCartCount(((CartDetailsResponse) response).getData().getshoppingcart_count());

            View addView = LayoutInflater.from(LookDetailActivity.this).inflate(R.layout.animated_add_cart_alert_view, null);
            DisplayMetrics matrix = getResources().getDisplayMetrics();

            final PopupWindow popupWindow = new PopupWindow(
                    addView,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            Button btnDismiss = (Button) addView.findViewById(R.id.imgCancelPopup);
            btnDismiss.setOnClickListener(new Button.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (null != popupWindow)
                        popupWindow.dismiss();
                }
            });
            popupWindow.showAsDropDown(findViewById(R.id.imgTrunk), (int) findViewById(R.id.imgTrunk).getPivotX(), (int) findViewById(R.id.imgTrunk).getY());

            ((TextView) addView.findViewById(R.id.lblAnimatedCartProductName)).setText("" + ProductHolder.tempProduct.getName());

            Picasso.with(LookDetailActivity.this)
                    .load(ImageUtils.getFullSizeImage(ProductHolder.tempProduct.getImage().get(0), matrix))
                    .placeholder(R.drawable.pdp_banner_placeholder)
                    .resize(matrix.widthPixels, matrix.heightPixels)
                    .centerInside()
                    .into((ImageView) addView.findViewById(R.id.imgAnimatedCart));

            if (null != ProductHolder.tempProduct.getSpecialprice())
                ((TextView) addView.findViewById(R.id.lblAnimatedCartProductPrice)).setText("Price: " + getResources().getString(R.string.rupee_symbole) + " " + ProductHolder.tempProduct.getSpecialprice());
            else
                ((TextView) addView.findViewById(R.id.lblAnimatedCartProductPrice)).setText("Price: " + getResources().getString(R.string.rupee_symbole) + " " + ProductHolder.tempProduct.getPrice());


            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (null != popupWindow)
                        popupWindow.dismiss();
                }
            }, 5000);
        }

        if (response instanceof AddOrRemoveFromWishListResponse) {
            Toast.makeText(getApplicationContext(), response.getMessage(), Toast.LENGTH_SHORT).show();
            MainActivity.setSpyCount(((AddOrRemoveFromWishListResponse) response).getData().getSpy_count());
        }

        if (response instanceof SpyAndCartItemCountResponse) {
            SpyAndCartItemCountResponse spyAndCartItemCountResponse = (SpyAndCartItemCountResponse) response;
            Log.i(TAG, "spy count : " + spyAndCartItemCountResponse.getData().getSpy_count());
            Log.i(TAG, "cart count : " + spyAndCartItemCountResponse.getData().getShoppingcart_count());

            if (!TextUtils.isEmpty(spyAndCartItemCountResponse.getData().getShoppingcart_count())) {
                if (!"0".equals(spyAndCartItemCountResponse.getData().getShoppingcart_count())) {
                    lblCartCount.setVisibility(View.VISIBLE);
                    lblCartCount.setText("" + spyAndCartItemCountResponse.getData().getShoppingcart_count());
                }
            }
            MainActivity.setSpyCount(spyAndCartItemCountResponse.getData().getSpy_count());
        }
    }
}
