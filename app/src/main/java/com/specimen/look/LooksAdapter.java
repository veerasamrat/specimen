package com.specimen.look;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.specimen.core.model.Look;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jitendra Khetle on 03/07/15.
 */
public class LooksAdapter extends RecyclerView.Adapter<LookHolder> {
    private final DisplayMetrics matrix;
    Context context;
    LayoutInflater inflater;
    List<Look> lookses;
    int resourceLayout;

    public LooksAdapter(Context context, int resource, ArrayList<Look> objects) {
        this.context = context;
        this.resourceLayout = resource;
        this.lookses = objects;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        matrix = context.getResources().getDisplayMetrics();
    }

    public void setResourceLayout(int resourceLayout) {
        this.resourceLayout = resourceLayout;
    }

    @Override
    public LookHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(resourceLayout, null);

        return new LookHolder(view, matrix);
    }

    @Override
    public void onBindViewHolder(LookHolder holder, int position) {
        Look looks = lookses.get(position);
        holder.getView(looks);
    }

    @Override
    public int getItemCount() {
        return (null != lookses ? lookses.size() : 0);
    }


}
