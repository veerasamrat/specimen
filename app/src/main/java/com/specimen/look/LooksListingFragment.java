package com.specimen.look;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.specimen.BaseFragment;
import com.specimen.R;
import com.specimen.askpersonalstylist.AskStylistActivity;
import com.specimen.core.model.Look;
import com.specimen.widget.ProgressView;

import java.util.ArrayList;

public class LooksListingFragment extends BaseFragment {

    ProgressView progressView;
    LooksAdapter looksAdapter;
    protected RecyclerView productRecyclerView;
    protected RecyclerView headerRecyclerView;
    protected ArrayList<Look> looksesList;

    public LooksListingFragment() {
        super();
    }

    public static LooksListingFragment newInstance(ArrayList<Look> looksArrayList) {

        LooksListingFragment fragment = new LooksListingFragment();

        if (looksArrayList != null) {
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("looks", looksArrayList);
            fragment.setArguments(bundle);
        }

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            looksesList = (ArrayList<Look>) getArguments().get("looks");
        } else {
            looksesList = new ArrayList<>();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_look_home, container, false);

        progressView = (ProgressView) view.findViewById(R.id.progressBar);

        headerRecyclerView = (RecyclerView) view.findViewById(R.id.horizontal_recycler_view);
        headerRecyclerView.setVisibility(View.GONE);

        productRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        productRecyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        productRecyclerView.setLayoutManager(linearLayoutManager);

        looksAdapter = new LooksAdapter(getActivity(), R.layout.looksitem, looksesList);
        productRecyclerView.setAdapter(looksAdapter);

        if (null == looksesList || 0 == looksesList.size()) {
            view.findViewById(R.id.recycler_view).setVisibility(View.GONE);
            view.findViewById(R.id.linearNoProductFoundView).setVisibility(View.VISIBLE);
        }

        view.findViewById(R.id.imbAskStylist).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AskStylistActivity.class);
                startActivity(intent);
            }
        });


        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (null == looksesList || 0 == looksesList.size()) {
//                Toast.makeText(getActivity(), "Ahhhh.. apologies, no look product found.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
