package com.specimen.me;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.specimen.R;

/**
 * Created by root on 30/10/15.
 */
public class DialogPasswordChnageSuccess extends DialogFragment {

    private IPasswordChangedCallback mIPasswordChangedCallback;

    public interface IPasswordChangedCallback {
        void changedPasswordCallback();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
    }

    public DialogPasswordChnageSuccess(IPasswordChangedCallback iPasswordChangedCallback) {
        mIPasswordChangedCallback = iPasswordChangedCallback;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(false);

        View view = inflater.inflate(R.layout.dialog_success_change_password, container, false);
        view.setMinimumWidth(view.getWidth());
        view.findViewById(R.id.textViewOkGotIt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIPasswordChangedCallback.changedPasswordCallback();
            }
        });
        return view;
    }
}
