package com.specimen.me;

import android.app.Fragment;

/**
 * Created by Tejas on 6/1/2015.
 * This will contain the order details in cluding tracking information in another tab.
 */
public class OrderDetailFragment {
    /**
     * Created by Tejas on 6/1/2015.
     * Base Fragment for Signup screen.
     */
    public static class SignUpBaseFragment extends Fragment {
    }

    /**
     * Created by Tejas on 6/1/2015.
     * Base Fragment for order screen.
     */
    public static class OrderBaseFragment {
    }

    /**
     * Created by Tejas on 6/1/2015.
     * The Base Fragment for Survey.
     */
    public static class SurveyBaseFragment {
    }

    /**
     * Created by Tejas on 6/1/2015.
     * Base Fragment for Trust Circle screen.
     */
    public static class TrustCircleBaseFragment {
    }
}
