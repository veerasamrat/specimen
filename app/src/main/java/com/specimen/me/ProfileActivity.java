package com.specimen.me;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.specimen.BaseFragment;
import com.specimen.R;
import com.specimen.me.address.AddressActivity;
import com.specimen.authentication.PrivacyAndTermsConditionsFragment;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.UserResponse;
import com.specimen.orders.OrdersActivity;
import com.specimen.studio.StudioActivity;
import com.specimen.survey.SurveyActivity;
import com.specimen.trustcircle.TrustCircleActivity;
import com.specimen.util.CircleTransform;
import com.specimen.widget.ProgressView;
import com.squareup.picasso.Picasso;

public class ProfileActivity extends BaseFragment {

    final String SHARE_SPECIMEN_LINK = "https://play.google.com/store/apps/details?id=";
    final String TAG = "ProfileActivity";
    ProgressView mProgressView;
    ImageView profileImage;

    String getSpecimenPlayStoreLink() {
        return SHARE_SPECIMEN_LINK + mContext.getPackageName();
    }

    protected void callAfterInitialization(View view) {
        mProgressView = (ProgressView) view.findViewById(R.id.progressView);
        profileImage = (ImageView) view.findViewById(R.id.profile_image);
        String profileImgURL = ((IApplicationFacade) IOCContainer.getInstance().getObject(ServiceName.APPLICATION_SERVICE, TAG))
                .getOfflineResponse("profile_img_url");
        if (TextUtils.isEmpty(profileImgURL)) {
            /*Picasso.with(mContext).load(R.drawable.profile_image).resize(76, 76)
                    .transform(new CircleTransform()).placeholder(R.drawable.profile_image).into(profileImage);*/
        } else {
            Picasso.with(mContext).load(profileImgURL).resize(76, 76)
                    .transform(new CircleTransform()).into(profileImage);
        }


        // contact us
        view.findViewById(R.id.linear_contactus).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment contactUsFragment = new ContactUsFragment();
                contactUsFragment.show(getChildFragmentManager(), "");
            }
        });

        // terms & condition
        view.findViewById(R.id.linear_tnc).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //DialogFragment dialogFragment = MeTermsAndConditions.newInstance();
                DialogFragment dialogFragment = PrivacyAndTermsConditionsFragment.newInstance();
                dialogFragment.show(getChildFragmentManager(), "TnC");
            }
        });

        // customer address
        view.findViewById(R.id.linearAddress).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addressIntent = new Intent();
                addressIntent.setClass(mContext, AddressActivity.class);
                startActivity(addressIntent);
            }
        });

        // trust circle
        view.findViewById(R.id.trustCircleProfileLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, TrustCircleActivity.class));

            }
        });

        // customer order details & staus
        view.findViewById(R.id.llMyOrders).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, OrdersActivity.class));
            }
        });

        // experience studio / talk-shop / book a studio visit
        view.findViewById(R.id.linear_book_studio).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, StudioActivity.class));
            }
        });

        view.findViewById(R.id.linear_social).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext, SocialConnectActivity.class));
            }
        });

        // style profile / survey
        view.findViewById(R.id.linear_style_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ((ISurveyFacade) IOCContainer.getInstance().getObject(ServiceName.SURVEY_SERVICE, TAG)).isSurveyCompleted();
                Intent surveyIntent = new Intent(mContext, SurveyActivity.class);
                surveyIntent.putExtra("TAG", TAG);
                startActivity(surveyIntent);
            }
        });

        // share specimen application google play store link
        view.findViewById(R.id.linear_share_specimen).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Specimen App");
                shareIntent.putExtra(Intent.EXTRA_TEXT, getSpecimenPlayStoreLink());
                startActivity(Intent.createChooser(shareIntent, "Share link!"));
            }
        });

        view.findViewById(R.id.linear_settings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent settingsIntent = new Intent();
                settingsIntent.setClass(getActivity(), SettingsActivity.class);
                startActivity(settingsIntent);
            }
        });

        UserResponse userResponse = ((IApplicationFacade) IOCContainer.getInstance()
                .getObject(ServiceName.APPLICATION_SERVICE, TAG)).getUserResponse();
        if (userResponse != null && userResponse.getData() != null) {
            TextView userName = (TextView) view.findViewById(R.id.username);
            StringBuffer res = new StringBuffer();

            String[] strArr = userResponse.getData().getCustomerFname().split(" ");
            for (String str : strArr) {
                char[] stringArray = str.trim().toCharArray();
                stringArray[0] = Character.toUpperCase(stringArray[0]);
                str = new String(stringArray);

                res.append(str).append(" ");
            }
            userName.setText("" + res.toString().trim());
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_profile, container, false);
        callAfterInitialization(view);
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        super.onSuccess(response, tag);
        mProgressView.setVisibility(View.GONE);
        if (!tag.equals(TAG)) {
            return;
        }

//        if (response instanceof IsSurveyCompleteResponse) {
//            Intent surveyIntent = new Intent(mContext, SurveyActivity.class);
//            surveyIntent.putExtra("TAG", TAG);
//            IsSurveyCompleteResponse surveyCompleteResponse = (IsSurveyCompleteResponse) response;
//
//            if (null != surveyCompleteResponse && surveyCompleteResponse.getIs_survey_completed().equals("true")) {
//                surveyIntent.putExtra("SURVEY_COMPLETE", true);
//                startActivity(surveyIntent);
//            } else {
//                startActivity(surveyIntent);
//            }
//        }
    }

    @Override
    public void onFailure(Exception error) {
        super.onFailure(error);
        mProgressView.setVisibility(View.GONE);
    }
}