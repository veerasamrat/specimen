package com.specimen.me;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.pinterest.android.pdk.PDKBoard;
import com.pinterest.android.pdk.PDKCallback;
import com.pinterest.android.pdk.PDKClient;
import com.pinterest.android.pdk.PDKException;
import com.pinterest.android.pdk.PDKRequest;
import com.pinterest.android.pdk.PDKResponse;
import com.specimen.BaseActivity;
import com.specimen.R;
import com.specimen.util.ShareToPinterest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Tejas on 9/1/2015.
 */
public class SocialConnectActivity extends BaseActivity implements View.OnClickListener {

    private ImageView fbConnect, pinConnect, tweetConnect;
    private ShareDialog shareSpecimenDialog;
    private CallbackManager callbackManager;
    private ShareToPinterest shareToPinterest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social_connect);
        setToolbar();
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        fbConnect = (ImageView) findViewById(R.id.facebook_social_btn);
        pinConnect = (ImageView) findViewById(R.id.pintrest_social_btn);
        tweetConnect = (ImageView) findViewById(R.id.twitter_social_btn);
        fbConnect.setOnClickListener(this);
        pinConnect.setOnClickListener(this);
        tweetConnect.setOnClickListener(this);

        shareSpecimenDialog = new ShareDialog(this);

    }

    void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn, null));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn));
        }
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText(getResources().getString(R.string.social_connect_title));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.facebook_social_btn:
                postOnFacebook();
                break;
            case R.id.pintrest_social_btn:
                shareToPinterest = new ShareToPinterest(SocialConnectActivity.this);
                shareToPinterest.logInToPinterest();
                break;
            case R.id.twitter_social_btn:
                startActivity(new Intent(SocialConnectActivity.this, TwitterActivity.class));
                break;
        }

        shareSpecimenDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {

            @Override
            public void onSuccess(Sharer.Result result) {
                Log.d("Facebook", "success");
            }

            @Override
            public void onCancel() {
                Log.d("Facebook", "cancelled");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d("Facebook", "error:" + " " + error.getMessage());
            }
        });
    }

    private void postOnFacebook() {
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent content = new ShareLinkContent.Builder()
                    .setContentUrl(Uri.parse("https://www.intelliswift.com"))
                    .setImageUrl(Uri.parse("http://specimen.intelliswift.in/media/myimage/logo.png"))
                    .setContentTitle("You must have this app")
                    .setContentDescription("Try this application for Mens clothing")
                    .build();

            ShareDialog.show(this, content);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (data != null && shareToPinterest != null) {
            shareToPinterest.getPDKClient().onOauthResponse(requestCode, resultCode, data);
            String accessToken = data.getStringExtra("PDKCLIENT_EXTRA_RESULT");
            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(SocialConnectActivity.this).edit();
            edit.putString("pinterestaccesstoken", accessToken);
            edit.commit();
        }
    }
}
