package com.specimen.me.address;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.specimen.BaseActivity;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.ServiceName;
import com.specimen.core.address.IAddressFacade;
import com.specimen.core.model.Address;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.AddressResponse;
import com.specimen.widget.ProgressView;

/**
 * Created by mohanish on 15/8/15.
 */
public class AddressActivity extends BaseActivity implements IResponseSubscribe, AddressFragment.INavigateNewAddressFragment, AddAddressFragment.IAddAddress {

    final String TAG = "AddressActivity";
    AddressResponse addressResponseParceable;
    Toolbar toolbar;
    TextView toolbarTitle;
    AddressFragment addressFragment;
    AddAddressFragment addAddressFragment;
    ProgressView mProgressView;
    boolean isUpdatedFragmentVisible;

    @Override
    public void onBackPressed() {
        if(isUpdatedFragmentVisible){
            if(null != addressResponseParceable && null != addressResponseParceable.getData() && 0 < addressResponseParceable.getData().size()) {
                setAddressList();
            } else {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
    }

    public boolean setUpadatedFragmentVisiblility(boolean flag) {
        return isUpdatedFragmentVisible = flag;
    }

    void setToolbar () {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn, null));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn));
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void setToolbarTitle (int toolbarTitleResouceId) {
        toolbarTitle.setText(toolbarTitleResouceId);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        setToolbar();
        mProgressView = (ProgressView) findViewById(R.id.progressView);
        fetchAddressListAPICall();
    }

    @Override
    protected void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    public void replaceFragment(Fragment fragment, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment, tag);
        transaction.commit();
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        mProgressView.setVisibility(View.GONE);
        if (!tag.equals(TAG)) {
            return;
        }

        if (response instanceof AddressResponse) {
            int size = (((AddressResponse) response).getData() != null) ? ((AddressResponse) response).getData().size() : 0;

            if (0 != size) {
                addressResponseParceable = new AddressResponse(((AddressResponse) response).getData());
                setAddressList();
            } else {
                addressResponseParceable = null;
                navigateNewAddressFragment();
            }
        }
    }

    @Override
    public void onFailure(Exception error) {
        mProgressView.setVisibility(View.GONE);
    }

    void fetchAddressListAPICall() {
        mProgressView.setVisibility(View.VISIBLE);
        ((IAddressFacade) IOCContainer.getInstance().getObject(ServiceName.ADDRESS_SERVICE, TAG)).getStoredAddress();
    }

    void deleteAddressAPICall(int deleteAddressId) {
        mProgressView.setVisibility(View.VISIBLE);
        ((IAddressFacade) IOCContainer.getInstance().getObject(ServiceName.ADDRESS_SERVICE, TAG))
                .deleteStoreAddress(addressResponseParceable.getData().get(deleteAddressId));
    }

    void addNewAddressAPICAll(Address addressObj) {
        mProgressView.setVisibility(View.VISIBLE);
        ((IAddressFacade) IOCContainer.getInstance().getObject(ServiceName.ADDRESS_SERVICE, TAG)).addNewAddress(addressObj);
    }

    void editAddressAPICAll(Address addressObj) {
        mProgressView.setVisibility(View.VISIBLE);
        ((IAddressFacade) IOCContainer.getInstance().getObject(ServiceName.ADDRESS_SERVICE, TAG)).changeStoreAddress(addressObj);
    }

    @Override
    public void navigateNewAddressFragment() {
        addAddressFragment = new AddAddressFragment(this, null);
        replaceFragment(addAddressFragment, "NewAddress");
    }

    @Override
    public void navigateEditAddressFragment(int selectedAddressId) {
        Bundle bundle = new Bundle();
        bundle.putString("addressToBe", "EditAddress");

        addAddressFragment = new AddAddressFragment(this, addressResponseParceable.getData().get(selectedAddressId));
        addAddressFragment.setArguments(bundle);
        replaceFragment(addAddressFragment, "EditAddress");
    }

    void setAddressList() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("address", addressResponseParceable);

        addressFragment = new AddressFragment(this);
        addressFragment.setArguments(bundle);
        replaceFragment(addressFragment, "AddressFragment");
    }

    @Override
    public void addNewAddressCallback() {
        fetchAddressListAPICall();
    }

}
