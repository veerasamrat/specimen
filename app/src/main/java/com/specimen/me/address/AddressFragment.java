package com.specimen.me.address;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.specimen.BaseFragment;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.address.IAddressFacade;
import com.specimen.core.model.Address;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.AddressResponse;

import java.util.List;

/**
 * Created by Tejas on 6/1/2015.
 * Address fragment enlisting all addresses that user added previously.
 */
public class AddressFragment extends BaseFragment implements IAddressEditor {

    RecyclerView storedAddressList;
    AddressResponse addressResponse;
    List<Address> addressList;
    INavigateNewAddressFragment iAddNewAddress;

    @Override
    public void editAddress(int id) {
        iAddNewAddress.navigateEditAddressFragment(id);
    }

    @Override
    public void deleteAddress(int id) {
        ((AddressActivity) getActivity()).deleteAddressAPICall(id);
    }

    @Override
    public void selectedAddressId(int id) {
        if (id != (-1)) {
            Address address = addressList.get(id);
            address.setIsPrimary(true);
            ((IAddressFacade) IOCContainer.getInstance().getObject(ServiceName.ADDRESS_SERVICE, "setPrimary")).changeStoreAddress(address);
        } else {
            getActivity().finish();
        }
    }

    public interface INavigateNewAddressFragment {
        void navigateNewAddressFragment();

        void navigateEditAddressFragment(int id);
    }

    public AddressFragment(INavigateNewAddressFragment iAddNewAddress) {
        this.iAddNewAddress = iAddNewAddress;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addressResponse = getArguments().getParcelable("address");
        if (null != addressResponse) {
            addressList = addressResponse.getData();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View storedAddressView = inflater.inflate(R.layout.fragment_store_address, container, false);
//        ((AddressActivity) getActivity()).setToolbarTitle(R.string.address_hint);
        ((AddressActivity) getActivity()).setToolbarTitle(R.string.address_text_title);
//        storedAddressView.findViewById(R.id.toolbar).setVisibility(View.GONE);
        storedAddressList = (RecyclerView) storedAddressView.findViewById(R.id.list);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        storedAddressList.setLayoutManager(linearLayoutManager);
        storedAddressList.setAdapter(new AddressAdapter(R.layout.address_store_view, addressList, this, getActivity()));
        storedAddressView.findViewById(R.id.imgAddNewAddress).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iAddNewAddress.navigateNewAddressFragment();
            }
        });

        return storedAddressView;
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        super.onSuccess(response, tag);

        if (tag.equals("setPrimary")) {
            if (response instanceof AddressResponse) {
                getActivity().onBackPressed();
            }
        }

    }

    @Override
    public void onFailure(Exception error) {
        super.onFailure(error);
        Toast.makeText(mContext, "Server has encountered an error. Please try again.", Toast.LENGTH_SHORT).show();
    }
}
