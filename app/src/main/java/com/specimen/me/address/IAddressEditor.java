package com.specimen.me.address;

/**
 * Created by root on 30/8/15.
 */
public interface IAddressEditor {

    void editAddress(int id);
    void deleteAddress(int id);
    void selectedAddressId(int id);
}
