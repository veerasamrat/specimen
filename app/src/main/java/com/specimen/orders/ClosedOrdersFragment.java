package com.specimen.orders;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.specimen.R;
import com.specimen.core.model.OrdersEntity;

import java.util.ArrayList;

public class ClosedOrdersFragment extends Fragment {


    OrderAdapter mOrderAdapter;
    RecyclerView rvClosedOrders;

    public ClosedOrdersFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_on_going_orders, container, false);
        Bundle bundle = getArguments();
        final ArrayList<OrdersEntity> closedOrders = bundle.getParcelableArrayList("closed");
        rvClosedOrders = (RecyclerView) rootView.findViewById(R.id.rvOngoing);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvClosedOrders.setLayoutManager(layoutManager);
        mOrderAdapter = new OrderAdapter(getActivity(),closedOrders);
        rvClosedOrders.setAdapter(mOrderAdapter);

        mOrderAdapter.SetOnOrderClickListener(new OnOrderClickListner() {
            @Override
            public void onItemClickListner(View v, int position) {
                Intent intent = new Intent(getActivity(), OrderDetailsActivity.class);
                intent.putExtra("orderid", closedOrders != null ? closedOrders.get(position).getOrderId() : "");
                intent.putExtra("orderNo",closedOrders != null ? closedOrders.get(position).getOrderNo() : "");

                startActivity(intent);
            }
        });
        return rootView;
    }



}
