package com.specimen.orders;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.specimen.R;
import com.specimen.core.model.OrderProduct;
import com.specimen.core.model.Product;
import com.specimen.feedback.FeedbackActivity;
import com.specimen.product.productdetails.ProductDetailsActivity;

import java.util.List;

/**
 * Created by Swapnil on 8/24/2015.
 */
public class ItemsViewHolder extends RecyclerView.ViewHolder {

    public TextView tvProductName;
    public TextView tvProductPrice;
    public TextView tvQuantity;
    public Button btnFeedback;
    public ImageView ivProductImage;

    public ItemsViewHolder(final View itemView,final List<Product> mProduct) {
        super(itemView);
        tvProductName = (TextView)itemView.findViewById(R.id.tvProductName);
        tvProductPrice= (TextView)itemView.findViewById(R.id.tvPrice);
        tvQuantity= (TextView)itemView.findViewById(R.id.tvQuantity);
        btnFeedback= (Button)itemView.findViewById(R.id.btnFeedback);
        ivProductImage= (ImageView)itemView.findViewById(R.id.ivProductImage);

        btnFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                itemView.getContext().startActivity(new Intent(mContext, FeedbackActivity.class));
                Intent feedbackIntent = new Intent(itemView.getContext(), FeedbackActivity.class);
                feedbackIntent.putExtra("productId", mProduct.get(getAdapterPosition()).getId());
                feedbackIntent.putExtra("imageUrl", mProduct.get(getAdapterPosition()).getImage().get(0).getMedium().getUrl());
                feedbackIntent.putExtra("productName", mProduct.get(getAdapterPosition()).getName());
                feedbackIntent.putExtra("productPrice", mProduct.get(getAdapterPosition()).getPrice());
                feedbackIntent.putExtra("productDPrice", mProduct.get(getAdapterPosition()).getSpecialprice());
                itemView.getContext().startActivity(feedbackIntent);
            }
        });

        ivProductImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent productDetailIntent = new Intent(itemView.getContext(), ProductDetailsActivity.class);
                productDetailIntent.putExtra("PRODUCT_ID", mProduct.get(getAdapterPosition()).getId());
                itemView.getContext().startActivity(productDetailIntent);
            }
        });
    }
}
