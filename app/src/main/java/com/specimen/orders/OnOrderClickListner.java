package com.specimen.orders;

import android.view.View;

/**
 * Created by Swapnil on 8/25/2015.
 */
public interface OnOrderClickListner {

    void onItemClickListner(View v, int position);
}
