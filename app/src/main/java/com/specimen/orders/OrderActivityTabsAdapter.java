package com.specimen.orders;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.specimen.core.model.OrderHistoryResponseData;

import java.util.ArrayList;

/**
 * Created by Swapnil on 8/21/2015.
 */
public class OrderActivityTabsAdapter extends FragmentStatePagerAdapter {
    private OrderHistoryResponseData orderHistoryResponseData;

    public OrderActivityTabsAdapter(FragmentManager fm, OrderHistoryResponseData data) {
        super(fm);
        this.orderHistoryResponseData = data;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            OnGoingOrdersFragment onGoingOrdersFragment = new OnGoingOrdersFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("ongoing", (ArrayList) orderHistoryResponseData.getOnGoingOrders());
            onGoingOrdersFragment.setArguments(bundle);
            return onGoingOrdersFragment;
        } else {
            ClosedOrdersFragment closedOrdersFragment = new ClosedOrdersFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("closed", (ArrayList) orderHistoryResponseData.getClosedOrders());
            closedOrdersFragment.setArguments(bundle);
            return closedOrdersFragment;
        }
//        else {
//            ReturnedOrdersFragment returnedOrdersFragment = new ReturnedOrdersFragment();
//            Bundle bundle = new Bundle();
//            bundle.putParcelableArrayList("returned", (ArrayList) orderHistoryResponseData.getReturnOrders());
//            returnedOrdersFragment.setArguments(bundle);
//            return returnedOrdersFragment;
//        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
