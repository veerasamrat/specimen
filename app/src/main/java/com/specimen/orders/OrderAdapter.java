package com.specimen.orders;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.specimen.R;
import com.specimen.core.model.OrdersEntity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Swapnil on 8/21/2015.
 */
public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.OrdersViewHolder> {

    private List<OrdersEntity> mOrdersList;
    private Activity mContext;
    private ItemsAdapter mItemAdapter;
    private OnOrderClickListner onOrderClickListner;

    public OrderAdapter(Activity context, List<OrdersEntity> orders) {
        this.mOrdersList = orders;
        this.mContext = context;
    }

    @Override
    public OrdersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_row_header, null);

        return new OrdersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(OrdersViewHolder holder, int position) {
        holder.tvOrderNo.setText("ORDER NO : " + mOrdersList.get(position).getOrderNo());
        holder.tvOrderStatus.setText(mOrdersList.get(position).getOrderStatus());
//        holder.tvOrderPlacedOn.setText("PLACED " + mOrdersList.get(position).getOrderDate());
        holder.tvOrderPlacedOn.setText("PLACED " + convertDate(mOrdersList.get(position).getOrderDate()));
        holder.tvTotal.setText("TOTAL " + mContext.getResources().getString(R.string.rupee_symbole)+mOrdersList.get(position).getOrderTotalValue());

        MyLinearLayoutManager layoutManager = new MyLinearLayoutManager(mContext, 1, false);
        holder.rvItems.setLayoutManager(layoutManager);
        mItemAdapter = new ItemsAdapter(mContext, mOrdersList.get(position).getOrderItems());
        holder.rvItems.setAdapter(mItemAdapter);
    }

    @Override
    public int getItemCount() {
        return mOrdersList.size();
    }

    public class OrdersViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tvOrderNo;
        public TextView tvOrderPlacedOn;
        public TextView tvTotal;
        public TextView tvOrderStatus;
        public RecyclerView rvItems;

        public OrdersViewHolder(View view) {
            super(view);
            tvOrderNo = (TextView) view.findViewById(R.id.tvOrderNo);
            tvOrderPlacedOn = (TextView) view.findViewById(R.id.tvOdrderPlacedOn);
            tvOrderStatus = (TextView) view.findViewById(R.id.btnOrderStatus);
            tvTotal = (TextView) view.findViewById(R.id.tvTotal);
            rvItems = (RecyclerView) view.findViewById(R.id.rvItems);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (onOrderClickListner != null)
                onOrderClickListner.onItemClickListner(v, getAdapterPosition());
        }
    }

    public void SetOnOrderClickListener(final OnOrderClickListner mItemClickListener) {
        this.onOrderClickListner = mItemClickListener;
    }

    public String convertDate(String date){
        String dateString[] = date.split(" ");
        System.out.println(dateString);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        DateFormat targetFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        String formattedDate = null;
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateString[0]);
            System.out.println(dateString[0]);
            formattedDate = targetFormat.format(convertedDate);
        } catch (ParseException e) {
// TODO Auto-generated catch block
            e.printStackTrace();
        }
        return formattedDate;
    }
}
