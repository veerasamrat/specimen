package com.specimen.orders;

import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.specimen.BaseActivity;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.model.OrdersEntity;
import com.specimen.core.order.IOrdersFacade;
import com.specimen.widget.ProgressView;

public class OrderDetailsActivity extends BaseActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {

    private ViewPager mOrderDetailsPager;
    private OrderDetailsTabAdapter orderDetailsTabAdapter;
    private TextView tvDetailsTab;
    private TextView tvTrackingTab;
    private String orderId, orderNo;
    private ProgressView progressView;
    private OrdersEntity ordersEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        setToolbar();
        orderId = getIntent().getStringExtra("orderid");
        orderNo = getIntent().getStringExtra("orderNo");
        ordersEntity = getIntent().getParcelableExtra("order");

        mOrderDetailsPager = (ViewPager) findViewById(R.id.pagerDetails);
        orderDetailsTabAdapter = new OrderDetailsTabAdapter(getSupportFragmentManager());
        mOrderDetailsPager.setAdapter(orderDetailsTabAdapter);

        tvDetailsTab = (TextView) findViewById(R.id.tvDetailsTab);
        tvTrackingTab = (TextView) findViewById(R.id.tvTrackingTab);
        progressView = (ProgressView)findViewById(R.id.progressBar);

        tvDetailsTab.setOnClickListener(this);
        tvTrackingTab.setOnClickListener(this);
        mOrderDetailsPager.addOnPageChangeListener(this);

        setTabBackground(tvDetailsTab);
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn, null));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn));
        }
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText("ORDERS");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void setProgressbarVisibility(int visibility){
            progressView.setVisibility(visibility);
    }

    public String getOrderId() {
        return orderId;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvDetailsTab:
                mOrderDetailsPager.setCurrentItem(0, true);
                setTabBackground(tvDetailsTab);
                break;

            case R.id.tvTrackingTab:
                mOrderDetailsPager.setCurrentItem(1, true);
                setTabBackground(tvTrackingTab);
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

        switch (position) {
            case 0:
                setTabBackground(tvDetailsTab);
                break;
            case 1:
                setTabBackground(tvTrackingTab);
                break;

        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(OrderDetailsActivity.this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(OrderDetailsActivity.this);
    }

    void setTabBackground(View selectedTab) {

        tvDetailsTab.setBackgroundColor(getResources().getColor(R.color.specimen_grey));
        tvTrackingTab.setBackgroundColor(getResources().getColor(R.color.specimen_grey));
        tvTrackingTab.setTypeface(null, Typeface.NORMAL);
        tvDetailsTab.setTypeface(null, Typeface.NORMAL);

        selectedTab.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
        ((TextView)selectedTab).setTypeface(null, Typeface.BOLD);
    }

    public OrdersEntity getOrder(){
        return ordersEntity;
    }
}
