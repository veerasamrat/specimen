package com.specimen.orders;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.specimen.BaseFragment;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.model.OrderDetailsResponseData;
import com.specimen.core.order.IOrdersFacade;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.OrderedDetailsResponse;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Swapnil on 8/28/2015.
 */
public class OrderDetailsFragment extends BaseFragment {


    private TextView orderNumber, orderPlacedDate, totalOrderAmt;//, totalTaxAmount, totalAmt, cardNumber, addresseName, addressLine1, addressLine2;
    private ImageView imgCancelOrder;
    RecyclerView orderedItemsList;
    private String orderId;
    private String TAG = "ORDER_DETAILS";
    private Context mContext;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_order_details, container, false);
        orderId = ((OrderDetailsActivity) getActivity()).getOrderId();
        orderedItemsList = (RecyclerView) rootView.findViewById(R.id.orderedItemsList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        orderedItemsList.setLayoutManager(linearLayoutManager);

        orderNumber = (TextView) rootView.findViewById(R.id.orderNumber);
        orderPlacedDate = (TextView) rootView.findViewById(R.id.orderPlacedDate);
        totalOrderAmt = (TextView) rootView.findViewById(R.id.totalOrderAmt);
//        totalTaxAmount = (TextView) rootView.findViewById(R.id.totalTaxAmt);
//        totalAmt = (TextView) rootView.findViewById(R.id.totalAmt);
//        cardNumber = (TextView) rootView.findViewById(R.id.textCardNumber);
//        addresseName = (TextView) rootView.findViewById(R.id.addresseName);
//        addressLine1 = (TextView) rootView.findViewById(R.id.addressLine1);
//        addressLine2 = (TextView) rootView.findViewById(R.id.addressLine2);
//        imgCancelOrder = (ImageView) rootView.findViewById(R.id.imgCancelOrder);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        orderId = ((OrderDetailsActivity) getActivity()).getOrderId();
        // Register the service to get API request and response
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
        ((IOrdersFacade) IOCContainer.getInstance().getObject(ServiceName.ORDER_SERVICE, TAG)).getOrderDetail(orderId);
        ((OrderDetailsActivity)getActivity()).setProgressbarVisibility(View.VISIBLE);
    }

    @Override
    public void onPause() {
        super.onPause();
        // unregister the service to get disable API request and response
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        if(response != null) {
            Log.d(TAG, response.getMessage());
            if (!tag.equals(TAG)) {
                return;
            }
            ((OrderDetailsActivity) getActivity()).setProgressbarVisibility(View.GONE);
            OrderDetailsResponseData orderedDetailsResponse = ((OrderedDetailsResponse) response).getData();
            orderNumber.setText(orderedDetailsResponse.getOrderNo());

            orderPlacedDate.setText(convertDate((orderedDetailsResponse.getOrderDate().split(" "))[0]));
            totalOrderAmt.setText(mContext.getResources().getString(R.string.rupee_symbole)+" "+orderedDetailsResponse.getOrderTotalValue());
//        totalTaxAmount.setText(orderedDetailsResponse.getTotalTax());
//        totalAmt.setText(orderedDetailsResponse.getOrderTotalValue());
//        cardNumber.setText(orderedDetailsResponse.getPaymentDetails());
//        addresseName.setText(orderedDetailsResponse.getAddress());
//        addressLine1.setText(orderedDetailsResponse.getAddress());

            OrderDetailsAdapter orderDetailsAdapter = new OrderDetailsAdapter(getActivity(),
                    R.layout.order_details_adapter_view, orderedDetailsResponse);
            orderedItemsList.setAdapter(orderDetailsAdapter);
        }
    }

    public String convertDate(String date){
        String dateString[] = date.split(" ");
        System.out.println(dateString);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        DateFormat targetFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        String formattedDate = null;
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateString[0]);
            System.out.println(dateString[0]);
            formattedDate = targetFormat.format(convertedDate);
        } catch (ParseException e) {
// TODO Auto-generated catch block
            e.printStackTrace();
        }
        return formattedDate;
    }

    @Override
    public void onFailure(Exception error) {
        ((OrderDetailsActivity)getActivity()).setProgressbarVisibility(View.GONE);
    }
}
