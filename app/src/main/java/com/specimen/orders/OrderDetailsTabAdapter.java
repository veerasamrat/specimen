package com.specimen.orders;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;


/**
 * Created by Swapnil on 8/25/2015.
 */
public class OrderDetailsTabAdapter extends FragmentStatePagerAdapter {

    public OrderDetailsTabAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            OrderDetailsFragment orderDetailsFragment = new OrderDetailsFragment();
            Bundle bundle = new Bundle();
//            bundle.putParcelableArrayList("ongoing", (ArrayList) orderHistoryResponseData.getOnGoingOrders());
            orderDetailsFragment.setArguments(bundle);
            return orderDetailsFragment;
        }
        if (position == 1) {
            OrderTrackingFragment orderTrackingFragment = new OrderTrackingFragment();
            Bundle bundle = new Bundle();
//            bundle.putParcelableArrayList("closed", (ArrayList) orderHistoryResponseData.getClosedOrders());
            orderTrackingFragment.setArguments(bundle);
            return orderTrackingFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
