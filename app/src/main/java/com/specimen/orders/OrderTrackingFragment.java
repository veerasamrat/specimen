package com.specimen.orders;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.specimen.BaseFragment;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.ServiceName;
import com.specimen.core.model.OrdersEntity;
import com.specimen.core.order.IOrdersFacade;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.OrderTrackingResponse;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Swapnil on 8/28/2015.
 */
public class OrderTrackingFragment extends BaseFragment implements IResponseSubscribe {
    private String TAG = "OrderTracking";
    private ImageView ivIndicator1, ivIndicator2, ivIndicator3, ivIndicator4, ivIndicator5;
    private TextView tvDeliveryState;
    private String tansitionStatus;
    private OrdersEntity ordersEntity;
    private TextView txtAddress, tvOrderNo, tvOrderDate;
    private TextView tvName;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_order_tracking, container, false);
        ordersEntity = ((OrderDetailsActivity) getActivity()).getOrder();
        ivIndicator1 = (ImageView) rootView.findViewById(R.id.ivIndicator1);
        ivIndicator2 = (ImageView) rootView.findViewById(R.id.ivIndicator2);
        ivIndicator3 = (ImageView) rootView.findViewById(R.id.ivIndicator3);
        ivIndicator4 = (ImageView) rootView.findViewById(R.id.ivIndicator4);
        ivIndicator5 = (ImageView) rootView.findViewById(R.id.ivIndicator5);

        tvDeliveryState = (TextView) rootView.findViewById(R.id.tvDeliveryState);
        txtAddress = (TextView) rootView.findViewById(R.id.txtAddress);
        tvOrderNo  = (TextView) rootView.findViewById(R.id.tvOrderNo);
        tvOrderDate= (TextView) rootView.findViewById(R.id.tvOrderDate);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
        if (ordersEntity != null)
            ((IOrdersFacade) IOCContainer.getInstance().getObject(ServiceName.ORDER_SERVICE, TAG)).trackShipment(ordersEntity.getOrderNo());
    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        if (!tag.equals(TAG)) {
            return;
        }
        if(response instanceof OrderTrackingResponse) {
            OrderTrackingResponse orderTrackingResponse = (OrderTrackingResponse) response;
            String status = orderTrackingResponse.getData().getStatus();
            switch (status) {
                case "0":
                    ivIndicator1.setImageResource(R.drawable.traking_point_indicator_sucess);
                    tansitionStatus = "Placed";
                    break;

                case "1":
                    ivIndicator1.setImageResource(R.drawable.traking_point_indicator_sucess);
                    ivIndicator2.setImageResource(R.drawable.traking_point_indicator_sucess);
                    tansitionStatus = "Processed";
                    break;

                case "2":
                    ivIndicator1.setImageResource(R.drawable.traking_point_indicator_sucess);
                    ivIndicator2.setImageResource(R.drawable.traking_point_indicator_sucess);
                    ivIndicator3.setImageResource(R.drawable.traking_point_indicator_sucess);
                    tansitionStatus = "Dispatched";
                    break;

                case "3":
                    ivIndicator1.setImageResource(R.drawable.traking_point_indicator_sucess);
                    ivIndicator2.setImageResource(R.drawable.traking_point_indicator_sucess);
                    ivIndicator3.setImageResource(R.drawable.traking_point_indicator_sucess);
                    ivIndicator4.setImageResource(R.drawable.traking_point_indicator_sucess);
                    tansitionStatus = "Final Transit";
                    break;

                case "4":
                    ivIndicator1.setImageResource(R.drawable.traking_point_indicator_sucess);
                    ivIndicator2.setImageResource(R.drawable.traking_point_indicator_sucess);
                    ivIndicator3.setImageResource(R.drawable.traking_point_indicator_sucess);
                    ivIndicator4.setImageResource(R.drawable.traking_point_indicator_sucess);
                    ivIndicator5.setImageResource(R.drawable.traking_point_indicator_sucess);
                    tansitionStatus = "Delivered";
                    break;

                case "5":
                    tansitionStatus = orderTrackingResponse.getMessage();
                    break;
            }


            tvDeliveryState.setText(tansitionStatus.toUpperCase());

            tvOrderNo.setText("ORDER NO : " + ordersEntity.getOrderNo());
            tvOrderDate.setText("PLACED " + convertDate(ordersEntity.getOrderDate().split(" ")[0]));
//        String addressString = ordersEntity.get
//                + (TextUtils.isEmpty(address.getAddress1()) ? "" : "\n" + address.getAddress1())
//                + (TextUtils.isEmpty(address.getAddress2()) ? "" : "\n" + address.getAddress2())
//                + "\n" + address.getDistrict()
//                + "\n" + address.getLocality()
//                + "\n" + address.getLandmark()
//                + "\n" + address.getPincode();
//        txtAddress.setText(addressString + "\nMOBILE: " + address.getMobile_no());
        }
    }

    public String convertDate(String date){
        String dateString[] = date.split(" ");
        System.out.println(dateString);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        DateFormat targetFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        String formattedDate = null;
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateString[0]);
            System.out.println(dateString[0]);
            formattedDate = targetFormat.format(convertedDate);
        } catch (ParseException e) {
// TODO Auto-generated catch block
            e.printStackTrace();
        }
        return formattedDate;
    }

    @Override
    public void onFailure(Exception error) {
    }
}
