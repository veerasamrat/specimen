package com.specimen.product;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.BaseFragment;
import com.specimen.MainActivity;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.filter.IFilterFacade;
import com.specimen.core.model.Query;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.CartDetailsResponse;
import com.specimen.search.FooterPanel;
import com.specimen.search.footerEvent.ProductFooterEvent;
import com.specimen.shop.CategoryFragment;
import com.specimen.util.BusProvider;
import com.specimen.util.ImageUtils;
import com.squareup.picasso.Picasso;

public class ProductListActivity extends BaseFragment implements View.OnClickListener {

    protected View fragmentContainer;
    protected View fragmentProductList;
    private FooterPanel footerPanel;
    private ProductFooterEvent productFooterEventState;
    private PopupWindow popupWindow;
    private Handler handler;

    protected void callAfterInitialization(View view) {

        fragmentContainer = view.findViewById(R.id.product_listing_container);
        fragmentProductList = view.findViewById(R.id.products_pager_container);

        productFooterEventState = new ProductFooterEvent();
        footerPanel = new FooterPanel(mContext, productFooterEventState, Query.getInstance().getTopCategoryId());
        footerPanel.init(view);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(getContentView(), container, false);
        callAfterInitialization(view);

        return view;
    }

    protected int getContentView() {
        return R.layout.activity_product_list;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.backButton) {
            mContext.onBackPressed();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
        footerPanel.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
        footerPanel.onPause();
        if (null != popupWindow && popupWindow.isShowing()) {
            popupWindow.dismiss();
        }

        if (null != popupWindow) popupWindow = null;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        ((IFilterFacade) IOCContainer.getInstance().getObject(ServiceName.FILTER_SERVICE, "")).clearFilterResponse();
        CategoryFragment.isPopupWindowDisplayed = false;
        if (null != popupWindow) popupWindow.dismiss();
        if (null != handler) handler = null;

    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        if (response instanceof CartDetailsResponse) {
//            Toast.makeText(mContext, response.getMessage(), Toast.LENGTH_SHORT).show();


            if (tag.equals("MainActivity") || tag.equals("CART_ACTIVITY")) return;

            if (!CategoryFragment.isPopupWindowDisplayed) {

                CategoryFragment.isPopupWindowDisplayed = true;

                View addView = LayoutInflater.from(mContext).inflate(R.layout.animated_add_cart_alert_view, null);
                DisplayMetrics matrix = mContext.getResources().getDisplayMetrics();

                popupWindow = new PopupWindow(
                        addView,
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                Button btnDismiss = (Button) addView.findViewById(R.id.imgCancelPopup);
                btnDismiss.setOnClickListener(new Button.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (null != popupWindow)
                            popupWindow.dismiss();

                        CategoryFragment.isPopupWindowDisplayed = false;
                    }
                });
                popupWindow.showAsDropDown(MainActivity.trunk_tab, (int) MainActivity.trunk_tab.getPivotX(), (int) MainActivity.trunk_tab.getY());

                ((TextView) addView.findViewById(R.id.lblAnimatedCartProductName)).setText("" + ProductHolder.tempProduct.getName());

                Picasso.with(mContext)
                        .load(ImageUtils.getFullSizeImage(ProductHolder.tempProduct.getImage().get(0), matrix))
                        .placeholder(R.drawable.pdp_banner_placeholder)
                        .resize(matrix.widthPixels, matrix.heightPixels)
                        .centerInside()
                        .into((ImageView) addView.findViewById(R.id.imgAnimatedCart));

                if (null != ProductHolder.tempProduct.getSpecialprice())
                    ((TextView) addView.findViewById(R.id.lblAnimatedCartProductPrice)).setText("Price: " + mContext.getResources().getString(R.string.rupee_symbole) + " " + ProductHolder.tempProduct.getSpecialprice());
                else
                    ((TextView) addView.findViewById(R.id.lblAnimatedCartProductPrice)).setText("Price: " + mContext.getResources().getString(R.string.rupee_symbole) + " " + ProductHolder.tempProduct.getPrice());
//            Toast.makeText(mContext, response.getMessage(), Toast.LENGTH_SHORT).show();
                MainActivity.setCartCount(((CartDetailsResponse) response).getData().getshoppingcart_count());

                handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (null != popupWindow)
                            popupWindow.dismiss();

                        CategoryFragment.isPopupWindowDisplayed = false;
                    }
                }, 5000);
            }
        }
    }
}
