package com.specimen.product;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.model.PageRequest;
import com.specimen.core.model.Product;
import com.specimen.core.model.Query;
import com.specimen.core.product.IProductFacade;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.CartDetailsResponse;
import com.specimen.core.response.ProductListResponse;
import com.specimen.search.footerEvent.IToggleState;
import com.specimen.widget.DividerItemDecoration;
import com.specimen.widget.SpacesItemDecoration;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

/**
 * Created by Intelliswift on 8/13/2015.
 */
public class ProductListingActivity extends ProductListActivity {

    protected ProductAdapter productAdapter;
    protected GridLayoutManager gridLayoutManager;

    protected View progressView;

    protected RecyclerView productRecyclerView;
    protected ArrayList<Product> productsList;
    private String TAG = "SubCategoryFragment";

    protected DividerItemDecoration listItemDecor;
    private SpacesItemDecoration gridDecor;

    @Override
    protected void callAfterInitialization(View view) {
        super.callAfterInitialization(view);
        fragmentProductList.setVisibility(View.GONE);
        startWithTag(view);
    }

    private void startWithTag(View view) {

        listItemDecor = new DividerItemDecoration(getActivity());
        gridDecor = new SpacesItemDecoration(2);

        productRecyclerView = (RecyclerView) view.findViewById(R.id.product_recycler_view);
        productRecyclerView.setHasFixedSize(true);

        productsList = new ArrayList<>();

        productAdapter = new ProductAdapter(mContext, R.layout.product_item_layout, productsList);
        productRecyclerView.setAdapter(productAdapter);

        gridLayoutManager = new GridLayoutManager(mContext, 1);
        productAdapter.setResourceLayout(R.layout.product_item_layout);

        productRecyclerView.addItemDecoration(listItemDecor);
        productRecyclerView.setLayoutManager(gridLayoutManager);

        progressView = view.findViewById(R.id.progressView);
        progressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        super.onSuccess(response, tag);
        progressView.setVisibility(View.GONE);

        if (!tag.equals(TAG)) {
            return;
        }

        if (response != null) {
            ProductListResponse productListResponse = (ProductListResponse) response;
            if (0 < productListResponse.getProductResponseData().getProducts().size()) {
                productsList.addAll(productListResponse.getProductResponseData().getProducts());
                productAdapter.notifyDataSetChanged();
            } else {
                Toast.makeText(mContext, "Ahhhh.. apologies, no product found for you.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    protected void fetchProduct() {

        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
        ((IProductFacade) IOCContainer.getInstance().getObject(ServiceName.PRODUCT_SERVICE, TAG))
                .getProduct(new PageRequest().setLimit(36), Query.getInstance(), TAG);
        if (progressView != null) {
            progressView.setVisibility(View.VISIBLE);
        }

    }


    @Subscribe
    public void updateProduct(IToggleState integer) {
        productsList.clear();
        productAdapter.notifyDataSetChanged();
        fetchProduct();
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchProduct();
    }


    @Subscribe
    public void toggleProductView(Boolean isGrid) {

        if (!isGrid) {

            gridLayoutManager.setSpanCount(1);
            productAdapter.setResourceLayout(R.layout.product_item_layout);
            productRecyclerView.removeItemDecoration(gridDecor);
            productRecyclerView.addItemDecoration(listItemDecor);

        } else {

            gridLayoutManager.setSpanCount(2);
            productAdapter.setResourceLayout(R.layout.product_item_grid_layout);
            productRecyclerView.removeItemDecoration(listItemDecor);
            productRecyclerView.addItemDecoration(gridDecor);

        }
        productRecyclerView.setAdapter(productAdapter);

    }
}
