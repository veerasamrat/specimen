package com.specimen.product;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.astuetz.PagerSlidingTabStrip;
import com.specimen.R;
import com.specimen.core.model.Subcategory;
import com.specimen.product.callback.IPageChangeDelegate;
import com.specimen.util.BusProvider;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

/**
 * Created by Intelliswift on 8/13/2015.
 */
public class ProductSubCategoryListingActivity extends ProductListActivity {
    protected ViewPager viewPager;
    private ImageView back;
    private ArrayList<Subcategory> subcategories;
    private String initialSelectedSubcategory;
    private int initialSelectedSubcategoryPosition;

    private ArrayList<IPageChangeDelegate> pageChangeDelegates;
    private ViewPager.OnPageChangeListener pageChangeListenerDelegate = new ViewPager.OnPageChangeListener() {

        private boolean isWorkForScrolled = true;

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            if (isWorkForScrolled)
                callPageVisible(position);
            isWorkForScrolled = false;
        }

        public void callPageVisible(int position) {
            if (pageChangeDelegates == null) {
                return;
            }

            for (int i = 0; i < pageChangeDelegates.size(); i++) {
                if (i == position) {
                    pageChangeDelegates.get(position).pageVisible(true);
                } else {
                    pageChangeDelegates.get(i).pageVisible(false);
                }
            }
//            viewPager.removeOnPageChangeListener(pageChangeListenerDelegate);
        }

        @Override
        public void onPageSelected(int position) {
            Log.d("onPageSelected", "onPageSelected : " + position);
            if (!isWorkForScrolled)
                callPageVisible(position);

        }

        @SuppressLint("LongLogTag")
        @Override
        public void onPageScrollStateChanged(int state) {
            Log.d("onPageScrollStateChanged", "onPageScrollStateChanged : " + state);
        }
    };

    private void startWithSubCategories(final View view) {
        pageChangeDelegates = new ArrayList<>();
        SubCategoryProductListingPagerAdapter subCategoryProductListingPagerAdapter = new SubCategoryProductListingPagerAdapter(mContext, getChildFragmentManager(), subcategories, pageChangeDelegates);
        viewPager.addOnPageChangeListener(pageChangeListenerDelegate);

        viewPager.setOffscreenPageLimit(2);
        viewPager.setAdapter(subCategoryProductListingPagerAdapter);

        viewPager.post(new Runnable() {
            @Override
            public void run() {
                viewPager.setCurrentItem(initialSelectedSubcategoryPosition);
                // Bind the tabs to the ViewPager
                PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) view.findViewById(R.id.subCategoryTabs);
                tabs.setViewPager(viewPager);
            }
        });


//        pageChangeListenerDelegate.onPageSelected(0);
//        viewPager.setCurrentItem(initialSelectedSubcategoryPosition);

        back.setOnClickListener(this);
    }


    @Override
    protected void callAfterInitialization(View view) {
        super.callAfterInitialization(view);

        Bundle bundle = getArguments();
        fragmentContainer.setVisibility(View.GONE);
        initialiseForSubCategory(view, bundle);
        startWithSubCategories(view);
    }

    private void initialiseForSubCategory(View view, Bundle bundle) {
        back = (ImageView) view.findViewById(R.id.backButton);
        viewPager = (ViewPager) view.findViewById(R.id.subCategoryViewPager);


        subcategories = bundle.getParcelableArrayList("subcategories");
        initialSelectedSubcategory = bundle.getString("selectedSubCategory");
        initialSelectedSubcategoryPosition = bundle.getInt("selectedSubCategoryPosition");
    }


}
