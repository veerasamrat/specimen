package com.specimen.product;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.BaseFragment;
import com.specimen.MainActivity;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.category.ICategoryFacade;
import com.specimen.core.filter.IFilterFacade;
import com.specimen.core.model.ImageEntity;
import com.specimen.core.model.PageRequest;
import com.specimen.core.model.Product;
import com.specimen.core.model.Subcategory;
import com.specimen.core.product.IProductFacade;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.AddOrRemoveFromWishListResponse;
import com.specimen.core.response.CartDetailsResponse;
import com.specimen.core.response.ProductListResponse;
import com.specimen.product.callback.IPageChangeDelegate;
import com.specimen.search.FooterPanel;
import com.specimen.search.footerEvent.IToggleState;
import com.specimen.shop.CategoryFragment;
import com.specimen.util.BusProvider;
import com.specimen.util.ImageUtils;
import com.specimen.widget.DividerItemDecoration;
import com.specimen.widget.SpacesItemDecoration;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SubCategoryProductListingFragment extends BaseFragment implements IPageChangeDelegate {

    boolean isPageVisibleToUser;
    private ProductAdapter productAdapter;
    private GridLayoutManager gridLayoutManager;
    private View progressView;
    private RecyclerView productRecyclerView;
    private PopupWindow popupWindow;
    private ArrayList<Product> productsList;
    private View mTempView;
    private String categoryId;
    private DividerItemDecoration listItemDecor;
    private SpacesItemDecoration gridDecor;
    private Handler handler;
    private String TAG = "SubCategoryFragment";

    public SubCategoryProductListingFragment() {
        super();
    }

    public static SubCategoryProductListingFragment newInstance(Subcategory subcategory) {

        SubCategoryProductListingFragment fragment = new SubCategoryProductListingFragment();

        if (subcategory != null) {
            Bundle bundle = new Bundle();
            bundle.putParcelable("subcategory", subcategory);
            fragment.setArguments(bundle);
        }

        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        ((IFilterFacade) IOCContainer.getInstance().getObject(ServiceName.FILTER_SERVICE, "")).clearFilterResponse();
        CategoryFragment.isPopupWindowDisplayed = false;
        if (null != popupWindow) popupWindow.dismiss();
        if (null != handler) handler = null;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            Subcategory subcategory = (Subcategory) getArguments().get("subcategory");
            categoryId = "" + (subcategory != null ? subcategory.getId() : 0);
        } else {
            categoryId = "";
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_sub_category, container, false);
        mTempView = view;
        productRecyclerView = (RecyclerView) view.findViewById(R.id.product_recycler_view);

        listItemDecor = new DividerItemDecoration(getActivity());
        gridDecor = new SpacesItemDecoration(2);

        productsList = new ArrayList<>();
        productAdapter = new ProductAdapter(getActivity(), R.layout.product_item_layout, productsList);

        productRecyclerView.setAdapter(productAdapter);

        gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        productAdapter.setResourceLayout(R.layout.product_item_layout);
        productRecyclerView.addItemDecoration(listItemDecor);
        productRecyclerView.setLayoutManager(gridLayoutManager);

        progressView = view.findViewById(R.id.progressView);
        toggleProductView(FooterPanel.lastToggleState);

        return view;
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {

        progressView.setVisibility(View.GONE);
        if ("MainActivity".equals(tag)) {
            return;
        }

        if (response instanceof CartDetailsResponse) {
            MainActivity.setCartCount(((CartDetailsResponse) response).getData().getshoppingcart_count());

            if (!CategoryFragment.isPopupWindowDisplayed) {

                CategoryFragment.isPopupWindowDisplayed = true;

                View addView = LayoutInflater.from(mContext).inflate(R.layout.animated_add_cart_alert_view, null);
                DisplayMetrics matrix = mContext.getResources().getDisplayMetrics();

                if (null != ProductHolder.tempProduct) {

                    popupWindow = new PopupWindow(
                            addView,
                            ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT);
                    Button btnDismiss = (Button) addView.findViewById(R.id.imgCancelPopup);
                    btnDismiss.setOnClickListener(new Button.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            if (null != popupWindow)
                                popupWindow.dismiss();

                            CategoryFragment.isPopupWindowDisplayed = false;
                        }
                    });
                    popupWindow.showAsDropDown(MainActivity.trunk_tab, (int) MainActivity.trunk_tab.getPivotX(), (int) MainActivity.trunk_tab.getY());

                    ((TextView) addView.findViewById(R.id.lblAnimatedCartProductName)).setText("" + ProductHolder.tempProduct.getName());
                    if(ProductHolder.tempProduct.getImage() != null) {

                        ImageEntity image = null;
                        if(!ProductHolder.tempProduct.getImage().isEmpty())
                        {
                            image = ProductHolder.tempProduct.getImage().get(0);
                        }


                            Picasso.with(mContext)
                                    .load(ImageUtils.getFullSizeImage(image, matrix))
                                    .placeholder(R.drawable.pdp_banner_placeholder)
                                    .resize(matrix.widthPixels, matrix.heightPixels)
                                    .centerInside()
                                    .error(R.drawable.pdp_banner_placeholder)
                                    .into((ImageView) addView.findViewById(R.id.imgAnimatedCart));

                    }

                    if (null != ProductHolder.tempProduct.getSpecialprice())
                        ((TextView) addView.findViewById(R.id.lblAnimatedCartProductPrice)).setText("Price: " + mContext.getResources().getString(R.string.rupee_symbole) + " " + ProductHolder.tempProduct.getSpecialprice());
                    else
                        ((TextView) addView.findViewById(R.id.lblAnimatedCartProductPrice)).setText("Price: " + mContext.getResources().getString(R.string.rupee_symbole) + " " + ProductHolder.tempProduct.getPrice());

                    MainActivity.setCartCount(((CartDetailsResponse) response).getData().getshoppingcart_count());

                    handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (null != popupWindow)
                                popupWindow.dismiss();

                            CategoryFragment.isPopupWindowDisplayed = false;
                        }
                    }, 5000);
                }
            }
        }

        if (response instanceof AddOrRemoveFromWishListResponse) {
//            Toast.makeText(mContext, response.getMessage(), Toast.LENGTH_SHORT).show();
            MainActivity.setSpyCount(((AddOrRemoveFromWishListResponse) response).getData().getSpy_count());
        }

        if (!tag.equals(categoryId)) {
            return;
        }
        Log.d(TAG, "HTTP Id -->" + categoryId);

        if (response != null) {
            productsList.clear();
            ProductListResponse productListResponse = (ProductListResponse) response;

            if ("Failure".equals(productListResponse.getStatus())) {
                Toast.makeText(mContext, "No Products Found", Toast.LENGTH_LONG).show();
                return;
            }
            productsList.addAll(productListResponse.getProductResponseData().getProducts());
            productAdapter.setHeader(productListResponse.getProductResponseData().getCurated_format());
            productAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onFailure(Exception error) {
        super.onFailure(error);
        progressView.setVisibility(View.GONE);
    }

    @Override
    public void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
        updateProduct(null);
    }

    private void fetchProduct() {
        if (categoryId != null) {
            if (progressView != null) {
                progressView.setVisibility(View.VISIBLE);
                ((IProductFacade) IOCContainer.getInstance().getObject(ServiceName.PRODUCT_SERVICE, TAG)).getProduct(new PageRequest().setLimit(36), categoryId, categoryId);
            }
        }
    }


    @Subscribe
    public void toggleProductView(Boolean isGrid) {

        if (!isGrid) {
            gridLayoutManager.setSpanCount(1);
            productAdapter.setResourceLayout(R.layout.product_item_layout);
            productRecyclerView.removeItemDecoration(gridDecor);
            productRecyclerView.addItemDecoration(listItemDecor);
            gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.DefaultSpanSizeLookup());
        } else {
            gridLayoutManager.setSpanCount(2);
            productAdapter.setResourceLayout(R.layout.product_item_grid_layout);
            productRecyclerView.removeItemDecoration(listItemDecor);
            productRecyclerView.addItemDecoration(gridDecor);

            gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {

//                    adapter.isHeader(position) ? manager.getSpanCount() : 1;

                    return productAdapter.getItemViewType(position) == ProductAdapter.HEADER ? gridLayoutManager.getSpanCount() : 1;
                }
            });

        }

        productRecyclerView.setAdapter(productAdapter);

    }

    @Override
    public void pageVisible(boolean isVisibleToUser) {
        this.isPageVisibleToUser = isVisibleToUser;
        Log.d(":--> pageVisible ", this.toString() + "--->" + isPageVisibleToUser);
        if (isPageVisibleToUser) {
            if (productsList != null && productsList.size() == 0) {
                fetchProduct();
            }
        }
    }

//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        this.isPageVisibleToUser = isVisibleToUser;
//        if (isPageVisibleToUser) {
//            if (productsList != null && productsList.size() == 0) {
//                fetchProduct();
//            }
//        }
//    }


    @Subscribe
    public void updateProduct(IToggleState integer) {
        productsList.clear();
//        productAdapter.setHeader(null);
        productAdapter.clear();
        productAdapter.notifyDataSetChanged();
        if (isPageVisibleToUser) {
            fetchProduct();
        }
    }

}
