package com.specimen.product;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.specimen.core.model.Subcategory;
import com.specimen.product.callback.IPageChangeDelegate;

import java.util.ArrayList;

/**
 * Created by santoshbangera on 26/06/15.
 */
public class SubCategoryProductListingPagerAdapter extends FragmentPagerAdapter {
    private final ArrayList<IPageChangeDelegate> mPageChangeDelegates;
    //    int PAGE_COUNT;
    Context context;
    private ArrayList<Subcategory> subCategoryTitles;

    public SubCategoryProductListingPagerAdapter(Context context, FragmentManager fm,
                                                 ArrayList<Subcategory> tabTitles, ArrayList<IPageChangeDelegate> pageChangeDelegates) {
        super(fm);
        this.context = context;
        this.subCategoryTitles = tabTitles;
        this.mPageChangeDelegates = pageChangeDelegates;
    }

    @Override
    public Fragment getItem(int position) {
        Log.d("PagerAdapter", "" + subCategoryTitles.get(position));
        SubCategoryProductListingFragment subCategoryProductListingFragment
                = SubCategoryProductListingFragment.newInstance(subCategoryTitles.get(position));
        mPageChangeDelegates.add(subCategoryProductListingFragment);
        return subCategoryProductListingFragment;
    }


    @Override
    public int getCount() {
        return subCategoryTitles.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return subCategoryTitles.get(position).getName();
    }
}
