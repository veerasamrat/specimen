package com.specimen.product.productdetails;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.specimen.R;
import com.specimen.core.model.ImageEntity;
import com.specimen.widget.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 5/8/15.
 */
public class PDPImageDialogFragment extends DialogFragment {

    ProductDetailsPagerAdapter adapter;
    ViewPager viewPager;
    List<ImageEntity> bannerImageArray;

    public PDPImageDialogFragment (List<ImageEntity> bannerImageArray) {
        this.bannerImageArray = bannerImageArray;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Light);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_fragment_product_details, container, false);
        viewPager = (ViewPager) view.findViewById(R.id.pager);
        List<String> imageArray = new ArrayList<>(bannerImageArray.size());

        for (int bannerImageCount = 0; bannerImageCount < bannerImageArray.size(); bannerImageCount++) {
            imageArray.add(bannerImageArray.get(bannerImageCount).getBest().getUrl());
        }

        adapter = new ProductDetailsPagerAdapter(getActivity(), imageArray);
        viewPager.setAdapter(adapter);
        CirclePageIndicator indicator = (CirclePageIndicator) view.findViewById(R.id.circle_pageIndicator);
       if(imageArray.size() == 1)
       {
           indicator.setVisibility(View.INVISIBLE);
       }
        else
       {
           indicator.setViewPager(viewPager);
           indicator.setSnap(true);
       }


        return view;
    }
}
