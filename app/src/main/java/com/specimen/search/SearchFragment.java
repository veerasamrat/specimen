package com.specimen.search;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.specimen.BaseFragment;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.model.Query;
import com.specimen.core.model.SuggestionEntity;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.AutoCompleteResponse;
import com.specimen.core.search.ISearchFacade;
import com.specimen.product.productdetails.ProductDetailsActivity;
import com.specimen.util.BusProvider;
import com.specimen.widget.DividerItemDecoration;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by RohitL on 6/15/2015.
 * For generc search.
 */
public class SearchFragment extends BaseFragment implements AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener {


    private AutoCompleteTextView searchAutoCompleteTextView;
    private RecyclerView historyList;
    private List<String> suggestion;
    private ArrayAdapter suggestionAdapter;

    private String TAG = "SearchFragment";
    private List<SuggestionEntity> suggestionEntityList;
    private ISearchFacade searchFacade;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        /**
         * Start with blank query parameter.
         */
        Query.getInstance().totalResetQuery();

        suggestion = new ArrayList();
        suggestionAdapter = new ArrayAdapter(mContext, android.R.layout.simple_list_item_1, suggestion);


        searchFacade = (ISearchFacade) IOCContainer.getInstance().getObject(ServiceName.SEARCH_SERVICE, TAG);
        searchAutoCompleteTextView = (AutoCompleteTextView) view.findViewById(R.id.autoCompleteTextView1);

        ((InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE))
                .showSoftInput(searchAutoCompleteTextView, InputMethodManager.SHOW_FORCED);
        searchAutoCompleteTextView.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        searchAutoCompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                getAutoSearchCompleteText(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        searchAutoCompleteTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch(v.getText().toString());
                    return true;
                }
                return false;
            }
        });

        searchAutoCompleteTextView.setOnItemSelectedListener(this);
        searchAutoCompleteTextView.setOnItemClickListener(this);

        historyList = (RecyclerView) view.findViewById(R.id.history);
        historyList.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        historyList.setLayoutManager(layoutManager);

        List<SuggestionEntity> entities = searchFacade.getHistory();

        if (entities != null) {
            SearchHistoryRowAdapter searchHistoryRowAdapter = new SearchHistoryRowAdapter(mContext, entities);
            historyList.addItemDecoration(new DividerItemDecoration(mContext));
            historyList.setAdapter(searchHistoryRowAdapter);

        }
    }

    private void performSearch(String s) {
/**
 *  Remove all last query parameter.
 */
        Query.getInstance().totalResetQuery();

        Query.getInstance().setSearchString(s);
        ArrayList<SuggestionEntity> entities = new ArrayList<>();

//        entities = (ArrayList<SuggestionEntity>) searchFacade.getHistory();

        SuggestionEntity entity = new SuggestionEntity();
        entity.setString(s);
        entities.add(entity);
//
//        //TODO : Move to search Result
//        moveToSearchProductListing(entities);

//        if (null != entities && 0 < entities.size()) {
//            Iterator<SuggestionEntity> entityIterator = entities.iterator();
//            while (entityIterator.hasNext()) {
//                String searchHistoryString = entityIterator.next().getString();
//                if (!searchHistoryString.equals(entity.getString())) {
//                    searchFacade.addInHistory(entity);
//                }
//            }
//        } else {
//            entities = new ArrayList<>();
//            entities.add(entity);
//            searchFacade.addInHistory(entity);
//        }

        //TODO : Move to search Result
        moveToSearchProductListing(entities);

        searchFacade.addInHistory(entity);
    }

    private void getAutoSearchCompleteText(CharSequence s) {

        searchFacade.getAutoSearchCompleteText(s.toString().trim());
    }

    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
        BusProvider.getInstance().register(this);
        InputMethodManager mgr = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.showSoftInput(searchAutoCompleteTextView, InputMethodManager.SHOW_IMPLICIT);
    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
        BusProvider.getInstance().unregister(this);
        InputMethodManager mgr = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.showSoftInput(searchAutoCompleteTextView, InputMethodManager.HIDE_IMPLICIT_ONLY);
        setKeyboardVisible(false);
    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
                               long arg3) {
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        setKeyboardVisible(false);

    }

    private void setKeyboardVisible(boolean b) {

        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(mContext.INPUT_METHOD_SERVICE);
        View currentFocus = mContext.getCurrentFocus();
        if (currentFocus != null) {
            if (!b)
                imm.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
            else
                imm.showSoftInputFromInputMethod(currentFocus.getWindowToken(), 0);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

        SuggestionEntity entity = suggestionEntityList.get(arg2);

        Context context = arg1.getContext();

        processEntity(entity, context);

        searchFacade.addInHistory(entity);

    }

    private boolean processEntity(SuggestionEntity entity, Context context) {

        Query.getInstance().totalResetQuery();

        if (entity != null) {

            //TODO : Category
            if (entity.getType() != null && entity.getType().equalsIgnoreCase("category")) {


                ArrayList<SuggestionEntity> entities = new ArrayList<>();

                if (entity.getParent_id() == 0) {

                    /**
                     * Assume suggestion is Top category.
                     */

                    Query.getInstance().setTopCategoryId(entity.getSug_id());

                    if (entity.getSug_id() != null && !entity.getSug_id().equalsIgnoreCase("0") && !entity.getSug_id().equalsIgnoreCase("")) {
                        SuggestionEntity subEntity = getEntity(entity.getString(), String.valueOf(entity.getSug_id()));
                        entities.add(subEntity);
                    }

                    moveToSearchProductListing(entities);

                    return true;
                }
                /**
                 * Suggestion is Sub+Top category ID
                 * */
                else {
                    //Expected :"formal IN Top"
                    String[] suggestions = entity.getString().split(" IN ");
                    Query.getInstance().setTopCategoryId(String.valueOf(entity.getParent_id()));

                    SuggestionEntity topEntity = getEntity(suggestions[1], String.valueOf(entity.getParent_id()));
                    entities.add(topEntity);


                    if (entity.getSug_id() != null && !entity.getSug_id().equalsIgnoreCase("0") && !entity.getSug_id().equalsIgnoreCase("")) {

                        Query.getInstance().setSubCategoryId(entity.getSug_id());
                        SuggestionEntity subEntity = getEntity(suggestions[0], String.valueOf(entity.getSug_id()));

                        entities.add(subEntity);
                    }

                    moveToSearchProductListing(entities);
                }
            } else if (entity.getType() != null && entity.getType().equalsIgnoreCase("product")) {
                Intent intent = new Intent(context, ProductDetailsActivity.class);
                intent.putExtra("PRODUCT_ID", "" + entity.getSug_id());
                context.startActivity(intent);
//            } else if (entity.getType() == null) {
                /**
                 * It works for other than above cases
                 */
            } else {
                Query.getInstance().setSearchString(entity.getString());
                ArrayList<SuggestionEntity> entities = new ArrayList<>();
                entities.add(entity);

                moveToSearchProductListing(entities);
            }

        }
        return false;
    }

    @NonNull
    private SuggestionEntity getEntity(String string, String sug_id) {
        SuggestionEntity subEntity = new SuggestionEntity();
        subEntity.setString(string);
        subEntity.setSug_id(sug_id);
        return subEntity;
    }

    private void moveToSearchProductListing(ArrayList<SuggestionEntity> entities) {

        setKeyboardVisible(false);

        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("search_lable", entities);

        SearchProductList fragment = new SearchProductList();
        fragment.setArguments(bundle);

        mContext.switchFragment(fragment, true, R.id.fragmentContainer);
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        super.onSuccess(response, tag);

        if (!tag.equals(TAG)) {
            return;
        }
        if (response instanceof AutoCompleteResponse) {

            AutoCompleteResponse autoCompleteResponse = (AutoCompleteResponse) response;

            if (autoCompleteResponse.getData() != null) {
                suggestion.clear();
                suggestionAdapter.clear();
                suggestionEntityList = autoCompleteResponse.getData();
                for (SuggestionEntity entity : suggestionEntityList) {

                    if (entity != null) {
                        suggestion.add(entity.getString());
                    }

                }

                suggestionAdapter = new ArrayAdapter(mContext, android.R.layout.simple_list_item_1, suggestion);
                searchAutoCompleteTextView.setAdapter(suggestionAdapter);
                suggestionAdapter.getFilter().filter(searchAutoCompleteTextView.getText(), null);
            }
        }
    }

    @Subscribe
    public void onSuggestionHistoryItemClick(SuggestionEntity entity) {
        processEntity(entity, mContext);
    }


}
