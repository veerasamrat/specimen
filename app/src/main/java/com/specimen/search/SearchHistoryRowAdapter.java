package com.specimen.search;

import java.util.List;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.specimen.R;
import com.specimen.core.model.SuggestionEntity;
import com.specimen.util.BusProvider;


public class SearchHistoryRowAdapter extends RecyclerView.Adapter<SearchHistoryRowAdapter.SearchHistoryRowViewHolder> {

    private List<SuggestionEntity> mDataSet;

    private Context context;
    private LayoutInflater layoutInflater;

    public SearchHistoryRowAdapter(Context context, List<SuggestionEntity> input) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.mDataSet = input;
    }

    @Override
    public SearchHistoryRowViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.search_history_row, null);
        SearchHistoryRowViewHolder holder = new SearchHistoryRowViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(SearchHistoryRowViewHolder holder, int position) {

        holder.setView(mDataSet.get(position));
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public class SearchHistoryRowViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView txtHistroy;
        private SuggestionEntity entity;

        public SearchHistoryRowViewHolder(View view) {
            super(view);
            txtHistroy = (TextView) view.findViewById(R.id.txt_histroy);
        }

        public void setView(SuggestionEntity object) {
            entity = object;
            txtHistroy.setText(object.getString());
            txtHistroy.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            BusProvider.getInstance().post(entity);
        }
    }

}
