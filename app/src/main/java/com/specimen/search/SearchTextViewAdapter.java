package com.specimen.search;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.specimen.R;
import com.specimen.core.model.Category;
import com.specimen.core.model.SuggestionEntity;

import java.util.List;


public class SearchTextViewAdapter extends RecyclerView.Adapter<SearchTextViewAdapter.SearchtextViewViewHolder> implements View.OnClickListener {

    private List<SuggestionEntity> mDataSet;

    private Context context;
    private LayoutInflater layoutInflater;

    private View.OnClickListener onClickListener;

    public SearchTextViewAdapter(Context context, List<SuggestionEntity> input) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.mDataSet = input;
        this.onClickListener = this;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public SearchtextViewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.search_header_category_view, null);
        SearchtextViewViewHolder holder = new SearchtextViewViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(SearchtextViewViewHolder holder, int position) {

        holder.setView(mDataSet.get(position));
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    @Override
    public void onClick(View v) {

    }


    public class SearchtextViewViewHolder extends RecyclerView.ViewHolder {

        private Button text1;

        public SearchtextViewViewHolder(View view) {
            super(view);
            text1 = (Button) view.findViewById(android.R.id.text1);
        }

        public void setView(final SuggestionEntity object) {
            text1.setText(object.getString());
            text1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setTag(object);
                    onClickListener.onClick(v);
                }
            });
        }


    }

}
