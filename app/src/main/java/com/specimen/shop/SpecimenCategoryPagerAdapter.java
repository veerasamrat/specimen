package com.specimen.shop;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.specimen.core.model.TopCategory;
import com.specimen.look.LookCategoryListingFragment;

import java.util.ArrayList;

/**
 * Created by santoshbangera on 26/06/15.
 */
public class SpecimenCategoryPagerAdapter extends FragmentPagerAdapter {
    int lookId = 32;
    int PAGE_COUNT;
    Context context;
    private ArrayList<TopCategory> topCategories;

    public SpecimenCategoryPagerAdapter(Context context, FragmentManager fm, ArrayList<TopCategory> topCategories) {
        super(fm);
        this.context = context;
        this.topCategories = topCategories;
        PAGE_COUNT = topCategories.size();
    }

    @Override
    public Fragment getItem(int position) {
        if (topCategories.get(position).getId() == lookId) {
            return LookCategoryListingFragment.newInstance(topCategories.get(position));
        }

        return CategoryFragment.newInstance(topCategories.get(position));
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        String topCategoryName = topCategories.get(position).getName().toUpperCase();
//        Typeface face= Typeface.createFromAsset(context.getAssets(), "fonts/GILB____.TTF");
//        tv1.setTypeface(face);
        return topCategoryName;
    }
}
