package com.specimen.spy;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.specimen.BaseFragment;
import com.specimen.R;
import com.specimen.core.model.Product;
import com.specimen.product.ProductAdapter;
import com.specimen.widget.DividerItemDecoration;

import java.util.ArrayList;

/**
 * Created by Intelliswift on 8/7/2015.
 */
public class SpiedProductFragment extends BaseFragment {

    RecyclerView recyclerView;
    private ArrayList<Product> spiedList;
    private ProductAdapter mAdapter;


    public static SpiedProductFragment newInstance(ArrayList<Product> products) {


        SpiedProductFragment fragment = new SpiedProductFragment();

        if (products != null) {
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("productList", products);
            fragment.setArguments(bundle);
        }

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            spiedList = (ArrayList<Product>) getArguments().get("productList");

        } else {
            spiedList = new ArrayList<>();
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_spy, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        mAdapter = new ProductAdapter(getActivity(), R.layout.product_item_layout, spiedList);
        recyclerView.setAdapter(mAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));

        if (null == spiedList || 0 == spiedList.size()) {
            view.findViewById(R.id.recycler_view).setVisibility(View.GONE);
            view.findViewById(R.id.linearNoProductFoundView).setVisibility(View.VISIBLE);
        }

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (null == spiedList || 0 == spiedList.size()) {
//                Toast.makeText(getActivity(), "Ahhhh.. no product found in your spy list.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
