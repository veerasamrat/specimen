package com.specimen.spy;

import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.specimen.BaseFragment;
import com.specimen.MainActivity;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.model.Look;
import com.specimen.core.model.Product;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.AddOrRemoveFromWishListResponse;
import com.specimen.core.response.CartDetailsResponse;
import com.specimen.core.response.FavoriteListResponse;
import com.specimen.core.response.SpyAndCartItemCountResponse;
import com.specimen.core.spy.ISpyFacade;
import com.specimen.core.spyandcartitemcount.ISpyAndCartItemCountFacade;
import com.specimen.look.LooksListingFragment;
import com.specimen.product.ProductHolder;
import com.specimen.util.ImageUtils;
import com.specimen.product.ProductAdapter;
import com.specimen.widget.ProgressView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Tejas on 6/25/2015.
 */
public class SpyActivity extends BaseFragment {

    public String TAG = "SpyActivity";
    private ViewPager viewPager;
    private PagerSlidingTabStrip tabs;
    private ProgressView progressView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.spy_activity, container, false);
        viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        tabs = (PagerSlidingTabStrip) view.findViewById(R.id.tabs);
        progressView = (ProgressView) view.findViewById(R.id.progressView);

        ((ISpyAndCartItemCountFacade) IOCContainer.getInstance().getObject(ServiceName.SPYANDCART_ITEM_COUNT_SERVICE, TAG)).getTotalSpyCartCount();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
        spyListAPICall();

    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    public void spyListAPICall() {
        progressView.setAnimation();
        progressView.setVisibility(View.VISIBLE);
        ((ISpyFacade) IOCContainer.getInstance().getObject(ServiceName.SPY_SERVICE, TAG)).spiedProductList();

    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        try {
            progressView.setVisibility(View.GONE);

            if (tag.equals("MainActivity")) return;

            if (response instanceof CartDetailsResponse) {
//            Toast.makeText(mContext, response.getMessage(), Toast.LENGTH_SHORT).show();

                View addView = LayoutInflater.from(mContext).inflate(R.layout.animated_add_cart_alert_view, null);
                DisplayMetrics matrix = mContext.getResources().getDisplayMetrics();

                final PopupWindow popupWindow = new PopupWindow(
                        addView,
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                Button btnDismiss = (Button) addView.findViewById(R.id.imgCancelPopup);
                btnDismiss.setOnClickListener(new Button.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (null != popupWindow)
                            popupWindow.dismiss();
                    }
                });
                popupWindow.showAsDropDown(MainActivity.trunk_tab, 50, (int) MainActivity.trunk_tab.getY());
                // Closes the popup window when touch outside.
                popupWindow.setOutsideTouchable(true);
                popupWindow.setTouchable(true);
                popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                popupWindow.setTouchInterceptor(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        popupWindow.dismiss();
                        return true;
                    }
                });

                ((TextView) addView.findViewById(R.id.lblAnimatedCartProductName)).setText("" + ProductHolder.tempProduct.getName());

                Picasso.with(mContext)
                        .load(ImageUtils.getFullSizeImage(ProductHolder.tempProduct.getImage().get(0), matrix))
                        .placeholder(R.drawable.pdp_banner_placeholder)
                        .resize(matrix.widthPixels, matrix.heightPixels)
                        .centerInside()
                        .into((ImageView) addView.findViewById(R.id.imgAnimatedCart));

                if (null != ProductHolder.tempProduct.getSpecialprice())
                    ((TextView) addView.findViewById(R.id.lblAnimatedCartProductPrice)).setText("Price: " + getResources().getString(R.string.rupee_symbole) + " " + ProductHolder.tempProduct.getSpecialprice());
                else
                    ((TextView) addView.findViewById(R.id.lblAnimatedCartProductPrice)).setText("Price: " + getResources().getString(R.string.rupee_symbole) + " " + ProductHolder.tempProduct.getPrice());

                MainActivity.setCartCount(((CartDetailsResponse) response).getData().getshoppingcart_count());

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (null != popupWindow)
                            popupWindow.dismiss();
                    }
                }, 5000);
            }

            if (response instanceof AddOrRemoveFromWishListResponse) {
                MainActivity.setSpyCount(((AddOrRemoveFromWishListResponse) response).getData().getSpy_count());
            }

            if (response instanceof SpyAndCartItemCountResponse) {
                SpyAndCartItemCountResponse spyAndCartItemCountResponse = (SpyAndCartItemCountResponse) response;
                Log.i(TAG, "spy count : " + spyAndCartItemCountResponse.getData().getSpy_count());
                Log.i(TAG, "cart count : " + spyAndCartItemCountResponse.getData().getShoppingcart_count());

                MainActivity.setSpyCount(spyAndCartItemCountResponse.getData().getSpy_count());
            }

            if (!tag.equals(TAG)) {
                return;
            }

            if (response instanceof FavoriteListResponse) {
                FavoriteListResponse listResponse = (FavoriteListResponse) response;

                ArrayList<String> titles = new ArrayList();
                titles.add("Product");
                titles.add("Looks");

                LooksListingFragment looksListingFragment = LooksListingFragment.newInstance((ArrayList<Look>) listResponse.getFavoriteResponseData().getLooks());
                SpiedProductFragment spiedProductFragment = SpiedProductFragment.newInstance((ArrayList<Product>) listResponse.getFavoriteResponseData().getProducts());

                ArrayList<Fragment> fragments = new ArrayList();

                fragments.add(spiedProductFragment);
                fragments.add(looksListingFragment);

                if (null != listResponse.getFavoriteResponseData()) {
//                    LooksListingFragment looksListingFragment = LooksListingFragment.newInstance((ArrayList<Look>) listResponse.getFavoriteResponseData().getLooks());
//                    SpiedProductFragment spiedProductFragment = SpiedProductFragment.newInstance((ArrayList<Product>) listResponse.getFavoriteResponseData().getProducts());
//
//                    ArrayList<Fragment> fragments = new ArrayList();
//
//                    fragments.add(spiedProductFragment);
//                    fragments.add(looksListingFragment);

                    ProductAdapter.CURRENT_REGISTER_BUS_EVENT_TAG = TAG;

                    SpiedViewPagerAdapter spiedProductAdapter = new SpiedViewPagerAdapter(mContext, getChildFragmentManager(), titles, fragments);
                    viewPager.setAdapter(spiedProductAdapter);
                    tabs.setViewPager(viewPager);
                }
            } else {
//            Toast.makeText(mContext, "Ahhahh server encountered an error.", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception ex) {

        }

    }

    @Override
    public void onFailure(Exception error) {
        progressView.setVisibility(View.GONE);
        Log.e(TAG, error.toString());
    }

}
