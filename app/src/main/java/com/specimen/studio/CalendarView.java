package com.specimen.studio;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.specimen.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;

/**
 * Created by a7med on 28/06/2015.
 */
public class CalendarView extends LinearLayout {
    // for logging
    private static final String LOGTAG = "Calendar View";

    // how many days to show, defaults to six weeks, 42 days
    private static final int DAYS_COUNT = 42;

    // default date format
    private static final String DATE_FORMAT = "MMM yyyy";

    // date format
    private String dateFormat;

    // current displayed month
    private Calendar currentDate = Calendar.getInstance();

    //event handling
    private EventHandler eventHandler = null;

    // internal components
    private LinearLayout header;
    private ImageView btnPrev;
    private ImageView btnNext;
    private TextView txtDate;
    private GridView grid;

    HashSet<Date> mEvents;

    public CalendarView(Context context) {
        super(context);
    }

    public CalendarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initControl(context, attrs);
    }

    public CalendarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initControl(context, attrs);
    }

    /**
     * Load control xml layout
     */
    private void initControl(Context context, AttributeSet attrs) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.studio_control_calendar, this);

        loadDateFormat(attrs);
        assignUiElements();
        assignClickHandlers();

        updateCalendar();
    }

    private void loadDateFormat(AttributeSet attrs) {
        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.CalendarView);

        try {
            // try to load provided date format, and fallback to default otherwise
            dateFormat = ta.getString(R.styleable.CalendarView_dateFormat);
            if (dateFormat == null)
                dateFormat = DATE_FORMAT;
        } finally {
            ta.recycle();
        }
    }

    private void assignUiElements() {
        // layout is inflated, assign local variables to components
        header = (LinearLayout) findViewById(R.id.calendar_header);
        btnPrev = (ImageView) findViewById(R.id.calendar_prev_button);
        btnNext = (ImageView) findViewById(R.id.calendar_next_button);
        txtDate = (TextView) findViewById(R.id.calendar_date_display);
        grid = (GridView) findViewById(R.id.calendar_grid);
    }

    private void assignClickHandlers() {
        // add one month and refresh UI
        btnNext.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                currentDate.add(Calendar.MONTH, 1);
                updateCalendar();
            }
        });

        // subtract one month and refresh UI
        btnPrev.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                currentDate.add(Calendar.MONTH, -1);
                updateCalendar();
            }
        });

        // long-pressing a day
//		grid.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
//		{
//
//			@Override
//			public boolean onItemLongClick(AdapterView<?> view, View cell, int position, long id)
//			{
//				// handle long-press
//				if (eventHandler == null)
//					return false;
//
//				eventHandler.onDayLongPress((Date)view.getItemAtPosition(position));
//				return true;
//			}
//		});
    }

    /**
     * Display dates correctly in grid
     */
    public void updateCalendar() {
        if (mEvents != null)
            updateCalendar(mEvents);
        else
            updateCalendar(null);
    }

    /**
     * Display dates correctly in grid
     */
    public void updateCalendar(HashSet<Date> events) {
        ArrayList<Date> cells = new ArrayList<>();
        Calendar calendar = (Calendar) currentDate.clone();
        mEvents = events;
        // determine the cell for current month's beginning
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        int monthBeginningCell = calendar.get(Calendar.DAY_OF_WEEK) - 1;

        // move calendar backwards to the beginning of the week
        calendar.add(Calendar.DAY_OF_MONTH, -monthBeginningCell);

        // fill cells
        while (cells.size() < DAYS_COUNT) {
            cells.add(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        // update grid
        CalendarAdapter calendarAdapter = new CalendarAdapter(getContext(), cells, events);
        grid.setAdapter(calendarAdapter);

        // update title
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM");
        // update title
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy");
        txtDate.setText(sdf1.format(currentDate.getTime()) + "\n" + sdf.format(currentDate.getTime()));

        // set header color according to current season
        int month = currentDate.get(Calendar.MONTH);
//		int season = monthSeason[month];
//		int color = rainbow[season];

//		header.setBackgroundColor(getResources().getColor(color));
    }


    public class CalendarAdapter extends ArrayAdapter<Date> {
        // days with events
        private HashSet<Date> eventDays;
        // today
        Date today = new Date();
        Date dateAfterThreeMonths = today;


        // for view inflation
        private LayoutInflater inflater;

        public CalendarAdapter(Context context, ArrayList<Date> days, HashSet<Date> eventDays) {
            super(context, R.layout.studio_control_calendar_day, days);
            this.eventDays = eventDays;
            inflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            // day in question
//
//            Calendar cal = Calendar.getInstance(); //Get the Calendar instance
//            cal.set(today.getYear(), today.getMonth(), today.getDate());
////            cal.setTime(curentDate);
//            cal.add(Calendar.MONTH, 3);//Three months from now
////            cal.add(Calendar.DAY_OF_MONTH, -);//Three months from now
//            Date date = cal.getTime();
//
//            Date dateFromAPI = getItem(position);
//
//            // inflate item if it does not exist yet
//            if (view == null)
//                view = inflater.inflate(R.layout.studio_control_calendar_day, parent, false);
//
//            if (dateFromAPI.before(date) && dateFromAPI.after(today)) {
//                ((TextView) view).setTextColor(Color.BLACK);
//
//                if (eventDays != null) {
//                    for (Date eventDate : eventDays) {
//                        if (eventDate.compareTo(dateFromAPI) == 0) {
//                            // mark this day for event
////                        view.setBackgroundColor(getContext().getResources().getColor(R.color.specimen_background_grey));
//                            view.setBackgroundResource(R.drawable.radio_btn);
//                            ((TextView) view).setTextColor(Color.WHITE);
//                            view.setOnClickListener(null);
//                            break;
//                        } else {
//                            ((TextView) view).setTextColor(Color.BLACK);
//                            view.setOnClickListener(new OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    if (eventHandler == null)
//                                        return;
//                                    eventHandler.onDayLongPress(getItem(position));
//                                }
//                            });
//                        }
//                    }
//                }
//            } else if (today.getTime() == dateFromAPI.getTime()) {
//
//                view.setBackgroundResource(R.drawable.calander_today_bg);
//                ((TextView) view).setTypeface(null, Typeface.BOLD);
//                ((TextView) view).setTextColor(Color.BLACK);
//            } else {
//                ((TextView) view).setTextColor(getResources().getColor(R.color.greyed_out));
//            }

            Calendar cal = Calendar.getInstance(); //Get the Calendar instance
//            cal.set(today.getYear(), today.getMonth(), today.getDate());
            cal.setTime(new Date());
            cal.add(Calendar.MONTH, 3);//Three months from now
//            cal.add(Calendar.DAY_OF_MONTH, -);//Three months from now
            dateAfterThreeMonths = cal.getTime();

            Date date = getItem(position);
//            Date date = cal.getTime();
            int day = date.getDate();
            int month = date.getMonth();
            int year = date.getYear();

            // inflate item if it does not exist yet
            if (view == null)
                view = inflater.inflate(R.layout.studio_control_calendar_day, parent, false);

            // clear styling
            ((TextView) view).setTypeface(null, Typeface.NORMAL);
            ((TextView) view).setTextColor(getResources().getColor(R.color.ninety_percent_transparent));


            if (month == today.getMonth() && year == today.getYear() && day == today.getDate()) {
                // if it is today, set it to blue/bold
                ((TextView) view).setTypeface(null, Typeface.BOLD);
                view.setBackgroundResource(R.drawable.calander_today_bg);
//				((TextView)view).setTextColor(getResources().getColor(R.color.today));
                view.setBackground(getResources().getDrawable(R.drawable.calender_rounded_circle));
                view.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (eventHandler == null)
                            return;

                        eventHandler.onDayLongPress(getItem(position));
                    }
                });
            }

            if (month == today.getMonth() && day == today.getDate() && year != today.getYear() ) {
                ((TextView) view).setTextColor(getResources().getColor(R.color.specimen_grey));
            }

            if (month != today.getMonth() && day == today.getDate() && year == today.getYear() ) {
                ((TextView) view).setTextColor(getResources().getColor(R.color.specimen_grey));
            }

//            if  ((month == (today.getMonth()) || month == (today.getMonth() + 1) || month == (today.getMonth() + 2)) && year != today.getYear()) {
//
//            }

            if (month != today.getMonth() || year != today.getYear()) {
                // if this day is outside current month, grey it out
                ((TextView) view).setTextColor(getResources().getColor(R.color.specimen_grey));
            } else if (day == today.getDate()) {
                // if it is today, set it to blue/bold
                ((TextView) view).setTypeface(null, Typeface.BOLD);
                view.setBackgroundResource(R.drawable.calander_today_bg);
//				((TextView)view).setTextColor(getResources().getColor(R.color.today));
                view.setBackground(getResources().getDrawable(R.drawable.calender_rounded_circle));
                view.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (eventHandler == null)
                            return;

                        eventHandler.onDayLongPress(getItem(position));
                    }
                });
            }

            if ((month == (today.getMonth()) || month == (today.getMonth() + 1) || month == (today.getMonth() + 2))) {
                // clear styling
                ((TextView) view).setTypeface(null, Typeface.NORMAL);
      /*          ((TextView) view).setTextColor(getResources().getColor(android.R.color.holo_blue_dark));

                view.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (eventHandler == null)
                            return;

                        eventHandler.onDayLongPress(getItem(position));
                    }
                });*/
            }else{
                ((TextView) view).setTextColor(getResources().getColor(R.color.specimen_grey));
            }

            if (month == today.getMonth() && day < today.getDate() && year != today.getYear()) {
                ((TextView) view).setTextColor(getResources().getColor(R.color.specimen_grey));
                view.setOnClickListener(null);
            }

            if(date.before(today) && dateAfterThreeMonths.after(date)){
                ((TextView) view).setTextColor(getResources().getColor(R.color.specimen_grey));
                view.setOnClickListener(null);
            }









            // if this day has an event, specify event image
//                       view.setBackgroundResource(0);
            if (eventDays != null) {

                if (month == today.getMonth() && year == today.getYear() && day == today.getDate()) {
                    // if it is today, set it to blue/bold
                    ((TextView) view).setTypeface(null, Typeface.BOLD);
                    view.setBackgroundResource(R.drawable.calander_today_bg);
                    ((TextView)view).setTextColor(getResources().getColor(R.color.today));
                    view.setBackground(getResources().getDrawable(R.drawable.calender_rounded_circle));
                    view.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (eventHandler == null)
                                return;

                            eventHandler.onDayLongPress(getItem(position));
                        }
                    });
                }




                if(date.after(today) && date.before(dateAfterThreeMonths)){
                    ((TextView) view).setTypeface(null, Typeface.NORMAL);
                    ((TextView) view).setTextColor(getResources().getColor(android.R.color.holo_blue_dark));

                    view.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (eventHandler == null)
                                return;

                            eventHandler.onDayLongPress(getItem(position));
                        }
                    });
                }



                for (Date eventDate : eventDays) {
                    if (eventDate.getDate() == day &&
                            eventDate.getMonth() == month &&
                            eventDate.getYear() == year) {
                        // mark this day for event
//                        view.setBackgroundColor(getContext().getResources().getColor(R.color.specimen_background_grey));
                        view.setBackgroundResource(R.drawable.radio_btn);
                        ((TextView) view).setTextColor(getResources().getColor(R.color.specimen_button_text));
                        view.setOnClickListener(null);
                        break;
                    }
                }
            }

            if (0 == today.compareTo(date)) {
                view.setBackgroundResource(R.drawable.selected_date_bg);
            }
//
            // set text
            ((TextView) view).setText(String.valueOf(date.getDate()));

            return view;
        }
    }

    /**
     * Assign event handler to be passed needed events
     */
    public void setEventHandler(EventHandler eventHandler) {
        this.eventHandler = eventHandler;
    }

    /**
     * This interface defines what events to be reported to
     * the outside world
     */
    public interface EventHandler {
        void onDayLongPress(Date date);
    }
}
