package com.specimen.studio;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.specimen.R;
import com.specimen.cart.HeaderRecyclerViewAdapterV2;
import com.specimen.core.model.StudioEntity;
import com.specimen.widget.CirclePageIndicator;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by DhirajK on 12/18/2015.
 */
public class ExperienceStudioAdapter extends HeaderRecyclerViewAdapterV2 {
    final String TAG = "StudioAdapter";

    Activity mContext;
    List<StudioEntity> studioEntityList;
    int resource;
    DisplayMetrics metrics;
    int [] mImageStudioBannerArray;
    private ImageAdapter adapter;


    public ExperienceStudioAdapter(int resource, List<StudioEntity> studioEntityList, int[] imageStudioBannerArray, Activity context) {
        this.resource = resource;
        this.studioEntityList = studioEntityList;
        this.mContext = context;
        this.metrics = mContext.getResources().getDisplayMetrics();
        this.mImageStudioBannerArray = imageStudioBannerArray;
        this.adapter = new ImageAdapter(mContext);
    }

    @Override
    public boolean useHeader() {
        return true;
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_studio, parent, false);
        CustomHeaderView customViewHolder = new CustomHeaderView(view);
        return customViewHolder;
    }

    @Override
    public void onBindHeaderView(RecyclerView.ViewHolder holder, int position) {
        CustomHeaderView mHolder = (CustomHeaderView) holder;
        mHolder.studioPager.setAdapter(adapter);
        mHolder.indicator.setViewPager(mHolder.studioPager);
    }

    @Override
    public boolean useFooter() {
        return false;
    }

    @Override
    public RecyclerView.ViewHolder onCreateFooterViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindFooterView(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public RecyclerView.ViewHolder onCreateBasicItemViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);
        StudioViewHolder studioViewHolder = new StudioViewHolder(view);
        studioViewHolder.setClickListener();
        return studioViewHolder;
    }

    @Override
    public void onBindBasicItemView(RecyclerView.ViewHolder holder, int position) {
        StudioViewHolder mHolder = (StudioViewHolder)holder;
        StudioEntity studioEntity = studioEntityList.get(position);
//        Picasso.with(mContext).load(ImageUtils.getFullSizeImage(studioEntity.getImage(), metrics))
//                .placeholder(R.drawable.studio_banner_placeholder).into(holder.imgStudio);
        Picasso.with(mContext).load(studioEntity.getImage())
//                .placeholder(R.drawable.studio_banner_placeholder)
                .placeholder(R.drawable.banner_explore_placeholder)
//                .resize(metrics.widthPixels, 800)
                .centerCrop()
                .fit()
                .into(mHolder.imgStudio);
        if(position == 0 )
        {
            mHolder.tvMainDescription.setVisibility(View.VISIBLE);
            mHolder.tvSubDescription.setVisibility(View.VISIBLE);
        }
        else
        {
            mHolder.tvMainDescription.setVisibility(View.GONE);
            mHolder.tvSubDescription.setVisibility(View.GONE);
        }
    }

    @Override
    public int getBasicItemCount() {
        return studioEntityList.size();
    }

    @Override
    public int getBasicItemType(int position) {
        return 0;
    }

    public class CustomHeaderView extends RecyclerView.ViewHolder {

        private final CirclePageIndicator indicator;
        protected ViewPager studioPager;

        public CustomHeaderView(View itemView) {
            super(itemView);

            studioPager = (ViewPager) itemView.findViewById(R.id.studio_pager);
            indicator = (CirclePageIndicator) itemView.findViewById(R.id.circle_pageIndicator);

        }
    }

    public class StudioViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final TextView tvMainDescription;
        private final TextView tvSubDescription;
        ImageView imgStudio;
        Button imbBookStudio;

        StudioViewHolder(View itemView) {
            super(itemView);
            imbBookStudio = (Button) itemView.findViewById(R.id.imbBookStudio);
            imgStudio = (ImageView) itemView.findViewById(R.id.imgStudio);
            tvMainDescription = (TextView) itemView.findViewById(R.id.lblMainDescriptionLine);
            tvSubDescription = (TextView) itemView.findViewById(R.id.lblSubDescriptionLine);
        }

        public void setClickListener() {
            super.itemView.setOnClickListener(this);
            imbBookStudio.setOnClickListener(this);
        }

        /**
         * Called when a view has been clicked.
         *
         * @param v The view that was clicked.
         */
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.imbBookStudio:
                    Intent intent = new Intent(mContext, StudioCalenderActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("studio_id", studioEntityList.get(getAdapterPosition() - 1).getId());
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);
                    break;
            }
        }
    }

    public class ImageAdapter extends PagerAdapter {

        Context context;

        ImageAdapter(Context context) {
            this.context = context;
        }

        @Override
        public int getCount() {
            return mImageStudioBannerArray.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ImageView imageView = new ImageView(context);
            Picasso.with(context).load(mImageStudioBannerArray[position])
                    //.resize(360, 480)
                   // .centerInside()
                    .fit()
                    .centerInside()
                    .into(imageView);

            container.addView(imageView, 0);

            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((ImageView) object);
        }
    }

}
