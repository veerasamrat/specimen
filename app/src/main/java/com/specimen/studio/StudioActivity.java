package com.specimen.studio;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.IResponseSubscribe;

import com.specimen.core.ServiceName;
import com.specimen.core.model.StudioResponseData;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.StudioListResponse;
import com.specimen.core.studio.IStudioFacade;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by root on 23/8/15.
 */
public class StudioActivity extends AppCompatActivity implements IResponseSubscribe {

    final String TAG = "StudioActivity";
    //final int imageStudioBannerArray[] = {R.drawable.menfashion, R.drawable.menfashion, R.drawable.menfashion, R.drawable.menfashion};
    final int imageStudioBannerArray[] = {R.drawable.talkshop_1, R.drawable.talkshop_2, R.drawable.talkshop_3};
    RecyclerView mRecyclerView;
    ViewPager studioPager;
    ImageAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_studio);
        getToolbar();
        mRecyclerView = (RecyclerView) findViewById(R.id.list);
//        studioPager = (ViewPager) findViewById(R.id.studio_pager);
//        adapter = new ImageAdapter(this);
//        studioPager.setAdapter(adapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        ((IStudioFacade) IOCContainer.getInstance().getObject(ServiceName.STUDIO_SERVICE, TAG)).getStudios();
    }

    public Toolbar getToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn, null));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn));
        }
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText(getResources().getString(R.string.experience_studio_title));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        return toolbar;
    }

    @Override
    protected void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        if (!tag.equals(TAG)) {
            return;
        }

        if (response instanceof StudioListResponse) {
            StudioResponseData studioListResponseData = ((StudioListResponse) response).getData();
          //  StudioAdapter studioAdapter = new StudioAdapter(R.layout.studio_row_items_view, studioListResponseData.getStyler(), this);
            ExperienceStudioAdapter studioAdapter = new ExperienceStudioAdapter(R.layout.studio_row_items_view, studioListResponseData.getStyler(),imageStudioBannerArray, this);
            int viewHeight = 400 * studioListResponseData.getStyler().size();
//            mRecyclerView.getLayoutParams().height = viewHeight;
            mRecyclerView.setAdapter(studioAdapter);
//            mRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
//                @Override
//                public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
//                    int action = e.getAction();
//                    switch (action) {
//                        case MotionEvent.ACTION_MOVE:
//                            rv.getParent().requestDisallowInterceptTouchEvent(true);
//                            break;
//                    }
//                    return false;
//                }
//
//                @Override
//                public void onTouchEvent(RecyclerView rv, MotionEvent e) {
//
//                }
//
//                @Override
//                public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
//
//                }
//            });
        }
    }

    @Override
    public void onFailure(Exception error) {

    }

    public class ImageAdapter extends PagerAdapter {

        Context context;

        ImageAdapter(Context context) {
            this.context = context;
        }

        @Override
        public int getCount() {
            return imageStudioBannerArray.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ImageView imageView = new ImageView(context);
            Picasso.with(context).load(imageStudioBannerArray[position])
                    .fit()
                    //.resize(360, 480)
                    .centerInside()
                    .into(imageView);

            container.addView(imageView, 0);

            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((ImageView) object);
        }
    }
}
