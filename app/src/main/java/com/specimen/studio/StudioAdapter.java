package com.specimen.studio;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.specimen.R;
import com.specimen.core.model.Address;
import com.specimen.core.model.StudioEntity;
import com.specimen.util.ImageUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by root on 23/8/15.
 */
public class StudioAdapter extends RecyclerView.Adapter<StudioAdapter.StudioViewHolder> {

    final String TAG = "StudioAdapter";

    Activity mContext;
    List<StudioEntity> studioEntityList;
    int resource;
    DisplayMetrics metrics;

    /**
     * Constructor
     *
     * @param resource The resource ID for a layout file containing a TextView to use when
     */
    public StudioAdapter(int resource, List<StudioEntity> studioEntityList, Activity context) {
        this.resource = resource;
        this.studioEntityList = studioEntityList;
        this.mContext = context;
        this.metrics = mContext.getResources().getDisplayMetrics();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    /**
     * Called when RecyclerView needs a new {@link RecyclerView.ViewHolder} of the given type to represent
     * an item.
     * <p/>
     * This new ViewHolder should be constructed with a new View that can represent the items
     * of the given type. You can either create a new View manually or inflate it from an XML
     * layout file.
     * <p/>
     * The new ViewHolder will be used to display items of the adapter using
     * {@link #onBindViewHolder(RecyclerView.ViewHolder, int)}. Since it will be re-used to display different
     * items in the data set, it is a good idea to cache references to sub views of the View to
     * avoid unnecessary {@link View#findViewById(int)} calls.
     *
     * @param parent   The ViewGroup into which the new View will be added after it is bound to
     *                 an adapter position.
     * @param viewType The view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     * @see #getItemViewType(int)
     * @see #onBindViewHolder(RecyclerView.ViewHolder, int)
     */
    @Override
    public StudioViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);
        StudioViewHolder studioViewHolder = new StudioViewHolder(view);
        studioViewHolder.setClickListener();
        return studioViewHolder;
    }

    /**
     * Called by RecyclerView to display the data at the specified position. This method
     * should update the contents of the {@link RecyclerView.ViewHolder#itemView} to reflect the item at
     * the given position.
     * <p/>
     * Note that unlike {@link ListView}, RecyclerView will not call this
     * method again if the position of the item changes in the data set unless the item itself
     * is invalidated or the new position cannot be determined. For this reason, you should only
     * use the <code>position</code> parameter while acquiring the related data item inside this
     * method and should not keep a copy of it. If you need the position of an item later on
     * (e.g. in a click listener), use {@link RecyclerView.ViewHolder#getAdapterPosition()} which will have
     * the updated adapter position.
     *
     * @param holder   The ViewHolder which should be updated to represent the contents of the
     *                 item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public void onBindViewHolder(StudioViewHolder holder, int position) {
        StudioEntity studioEntity = studioEntityList.get(position);
//        Picasso.with(mContext).load(ImageUtils.getFullSizeImage(studioEntity.getImage(), metrics))
//                .placeholder(R.drawable.studio_banner_placeholder).into(holder.imgStudio);
        Picasso.with(mContext).load(studioEntity.getImage())
//                .placeholder(R.drawable.studio_banner_placeholder)
                .placeholder(R.drawable.banner_explore_placeholder)
//                .resize(metrics.widthPixels, 800)
                .centerCrop()
                .fit()
                .into(holder.imgStudio);


    }

    /**
     * Returns the total number of items in the data set hold by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return studioEntityList.size();
    }

    public class StudioViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imgStudio;
        Button imbBookStudio;

        StudioViewHolder(View itemView) {
            super(itemView);
            imbBookStudio = (Button) itemView.findViewById(R.id.imbBookStudio);
            imgStudio = (ImageView) itemView.findViewById(R.id.imgStudio);
        }

        public void setClickListener() {
            super.itemView.setOnClickListener(this);
            imbBookStudio.setOnClickListener(this);
        }

        /**
         * Called when a view has been clicked.
         *
         * @param v The view that was clicked.
         */
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.imbBookStudio:
                    Intent intent = new Intent(mContext, StudioCalenderActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("studio_id", studioEntityList.get(getAdapterPosition()).getId());
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);
                    break;
            }
        }
    }
}
