package com.specimen.studio;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.specimen.BaseActivity;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.ServiceName;
import com.specimen.core.model.SetAppointmentResponse;
import com.specimen.core.model.StudioSlotsResponse;
import com.specimen.core.response.APIResponse;
import com.specimen.core.studio.IStudioFacade;
import com.specimen.widget.ProgressView;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class StudioBookingActivity extends BaseActivity implements IResponseSubscribe, View.OnClickListener, OnBookAppointmentClick {

    private TextView tvDate;
    private RecyclerView rvTimeSlot;
    private String TAG = "ActivityStudioBookingActivity";
    private BookAppointSlotsAdapter adapter;
    private String studio_id, date;
    private ImageView ivBack;
    private Button btnBookAppointment;
    private String mSlot;
    private RelativeLayout rlAllDone;
    private long dateLong;
    private ProgressView progressView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_studio_booking);
        getToolbar();
        progressView = (ProgressView) findViewById(R.id.progressView);
        tvDate = (TextView) findViewById(R.id.tvDate);
        rvTimeSlot = (RecyclerView) findViewById(R.id.rvTimeSlot);
        btnBookAppointment = (Button) findViewById(R.id.btnBookAppointment);
        rlAllDone = (RelativeLayout) findViewById(R.id.rlAllDone);
        rlAllDone.setOnClickListener(this);
        rlAllDone.setVisibility(View.GONE);

        btnBookAppointment.setOnClickListener(this);
        Bundle bundle = getIntent().getExtras();
        studio_id = bundle.getString("studio_id");
//        date = bundle.getString("date");
        dateLong = bundle.getLong("date");
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        c.setTimeInMillis(dateLong);
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        String tempMonth = "";
        if (month < 9) {
            tempMonth = "0" + (month + 1);
        } else {
            tempMonth = (month + 1) + "";
        }

        String tempDay = "";
        tempDay = "" + day;
        if (day < 10) {
            tempDay = "0" + day;
        }

        date = year + "-" + tempMonth + "-" + tempDay;
        tvDate.setText(day + " " + getMonthForInt(month).toUpperCase() + " " + year);

        findViewById(R.id.imgClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

//        showcaseView();
    }

    public Toolbar getToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn, null));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn));
        }
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText(getResources().getString(R.string.experience_studio_title));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        return toolbar;
    }

    void showcaseView() {
        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lps.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        lps.addRule(RelativeLayout.CENTER_IN_PARENT);
        int margin = ((Number) (getResources().getDisplayMetrics().density * 2)).intValue();
        lps.setMargins(margin, margin, margin, margin);
        ViewTarget target = new ViewTarget(R.id.btnBookAppointment, this);
        ShowcaseView sv = new ShowcaseView.Builder(this)
                .setTarget(target)
                .setContentTitle("ShowcaseView")
                .setContentText("This is highlighting the Home button")
                .hideOnTouchOutside()
                .build();
        sv.setButtonPosition(lps);

    }

    private String getMonthForInt(int num) {
        String month = "wrong";
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getShortMonths();
        if (num >= 0 && num <= 11) {
            month = months[num];
        }
        return month;
    }

    @Override
    protected void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
        ((IStudioFacade) IOCContainer.getInstance().getObject(ServiceName.STUDIO_SERVICE, TAG)).getTimeSlots(studio_id, date);
    }

    @Override
    protected void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }


    @Override
    public void onSuccess(APIResponse response, String tag) {
        progressView.setVisibility(View.GONE);
        if (tag.equals(TAG)) {
            if (response instanceof StudioSlotsResponse) {
                StudioSlotsResponse studioSlotsResponse = (StudioSlotsResponse) response;
                if (studioSlotsResponse != null) {
                    adapter = new BookAppointSlotsAdapter(StudioBookingActivity.this, studioSlotsResponse.getData().getTimeslots());
                    LinearLayoutManager llm = new LinearLayoutManager(this);
                    llm.setOrientation(LinearLayoutManager.VERTICAL);
                    rvTimeSlot.setLayoutManager(llm);
                    rvTimeSlot.setAdapter(adapter);
                }
            }

            if (response instanceof SetAppointmentResponse) {
                rlAllDone.setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(), response.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public void onFailure(Exception error) {
        progressView.setVisibility(View.GONE);
        btnBookAppointment.setEnabled(true);
        btnBookAppointment.setFocusable(true);
        btnBookAppointment.setVisibility(View.VISIBLE);
        rvTimeSlot.setEnabled(true);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnBookAppointment) {
            if (mSlot != null && 0 < mSlot.length()) {
                rvTimeSlot.setEnabled(false);
                btnBookAppointment.setEnabled(false);
                btnBookAppointment.setFocusable(false);
                btnBookAppointment.setVisibility(View.GONE);
                progressView.setVisibility(View.VISIBLE);
                progressView.setAnimation();
                ((IStudioFacade) IOCContainer.getInstance().getObject(ServiceName.STUDIO_SERVICE, TAG)).setAppointment(studio_id, mSlot, date);
            } else {
                Toast.makeText(getApplicationContext(), "Please select slot.", Toast.LENGTH_SHORT).show();
            }
        }
        if (view.getId() == R.id.rlAllDone) {
            //  goTOProfileActivity();
        }
    }

    private void goTOProfileActivity() {
        Intent intent = new Intent(this, StudioActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        this.finish();
    }

    @Override
    public void onBookAppointmentClick(String slot) {
        mSlot = slot;

    }

    public String getFormatedDate(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "MMM dd, yyyy");
        Date myDate = null;
        try {
            myDate = dateFormat.parse(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd");
        return timeFormat.format(myDate);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if (rlAllDone.getVisibility() == View.VISIBLE) {
            goTOProfileActivity();
        } else {
            this.finish();
        }
    }
}
