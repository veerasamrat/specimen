package com.specimen.survey;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.BaseFragment;
import com.specimen.MainActivity;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.ServiceName;
import com.specimen.core.model.SurveyStyleProfileResponseData;
import com.specimen.core.response.APIResponse;
import com.specimen.core.survey.ISurveyFacade;
import com.specimen.survey.callback.ISurveyFragmentChanger;
import com.specimen.util.StoreAndShareProductBitmapUtil;
import com.specimen.widget.ProgressView;
import com.squareup.picasso.Picasso;

/**
 * Created by root on 19/7/15.
 */
public class SurveyEndFragment extends BaseFragment implements IResponseSubscribe {

    final String TAG = "SurveyEndFragment";
    ISurveyFragmentChanger mISurveyPageChanger;

    ImageView imgSurveyEndImage;
    TextView lblSurveyHeader1, lblSurveyEndText;
    ProgressView mProgressView;
    DisplayMetrics metrics;

    public SurveyEndFragment(ISurveyFragmentChanger iSurveyFragmentChanger) {
        this.mISurveyPageChanger = iSurveyFragmentChanger;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        metrics = getActivity().getResources().getDisplayMetrics();
    }

    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_survey_end, container, false);
        view.findViewById(R.id.btnEnterStore).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
            }
        });

        view.findViewById(R.id.btnSurveyShare).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: Share survey result
                StoreAndShareProductBitmapUtil bitmapUtil = new StoreAndShareProductBitmapUtil(getActivity());
                Bitmap bitmap = ((BitmapDrawable) imgSurveyEndImage.getDrawable()).getBitmap();
                bitmapUtil.storeImage(bitmap, true);
            }
        });

        imgSurveyEndImage = (ImageView) view.findViewById(R.id.imgSurveyEndImage);
        lblSurveyHeader1 = (TextView) view.findViewById(R.id.lblSurveyHeader1);
        lblSurveyEndText = (TextView) view.findViewById(R.id.lblSurveyEndText);

        mProgressView = (ProgressView) view.findViewById(R.id.progressView);
        mProgressView.setVisibility(View.VISIBLE);

        ((ISurveyFacade) IOCContainer.getInstance().getObject(ServiceName.SURVEY_SERVICE, TAG)).getstyleprofiledata();
        return view;
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        mProgressView.setVisibility(View.GONE);
        if (!tag.equals(TAG)) {
            return;
        }

        if (response instanceof SurveyStyleProfileResponseData) {
            SurveyStyleProfileResponseData.DataEntity surveyResponseData = ((SurveyStyleProfileResponseData) response).getData();
            if (null != surveyResponseData) {
                lblSurveyHeader1.setText("" + surveyResponseData.getTitle());
                lblSurveyEndText.setText("" + surveyResponseData.getDesc());
                lblSurveyEndText.setMovementMethod(new ScrollingMovementMethod());
                Picasso.with(getActivity())
                        .load("" + surveyResponseData.getImage())
//                    .placeholder(R.drawable.finish_survey_placeholder)
                        .placeholder(R.drawable.survey_img_placeholder)
                        .resize(300, 300)
                        .into(imgSurveyEndImage);
            } else {
                Toast.makeText(mContext, "Server encountered an error,", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onFailure(Exception error) {
        mProgressView.setVisibility(View.GONE);
    }
}
