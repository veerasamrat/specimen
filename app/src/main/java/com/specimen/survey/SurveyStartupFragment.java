package com.specimen.survey;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.specimen.BaseFragment;
import com.specimen.MainActivity;
import com.specimen.survey.callback.ISurveyFragmentChanger;
import com.specimen.R;
import com.specimen.home.HomeActivity;

/**
 * Created by root on 19/7/15.
 */
public class SurveyStartupFragment extends BaseFragment {

    ISurveyFragmentChanger mISurveyFragmentChanger;

    public static final int SURVEY_STARTUP = 0;

    public SurveyStartupFragment (ISurveyFragmentChanger iSurveyFragmentChanger) {
        this.mISurveyFragmentChanger = iSurveyFragmentChanger;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_survey_startup, container, false);

        // Set title
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.survey_title);
        toolbar.setVisibility(View.GONE);

        view.findViewById(R.id.btnStartSurvey).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mISurveyFragmentChanger.changeSurveyFragment(SURVEY_STARTUP);
            }
        });

        view.findViewById(R.id.btnEnterStore).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: navigate to home activity
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((SurveyActivity)getActivity()).visibleProgressbar(false);
    }
}
