package com.specimen.survey.callback;

/**
 * Created by root on 3/7/15.
 */
public interface ISurveyFragmentChanger {

    void changeSurveyFragment(int fragmentReplaceWith);
}
