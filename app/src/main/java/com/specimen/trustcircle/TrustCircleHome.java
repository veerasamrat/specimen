package com.specimen.trustcircle;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.specimen.BaseFragment;
import com.specimen.R;
import com.specimen.core.model.BulletinData;
import com.specimen.util.ImageUtils;
import com.specimen.widget.CirclePageIndicator;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Tejas on 6/1/2015.
 * From here user can add trustee to the circle.
 */
public class TrustCircleHome extends BaseFragment {

    Button addMateButton;
    private int [] imagesBanner = {R.drawable.askamate_1,R.drawable.askamate_2};
    ViewPager mBannerBulletin;
    CirclePageIndicator indicator;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewGroup homeView = (ViewGroup) inflater.inflate(R.layout.fragment_askamate_home, container, false);
        ((TrustCircleActivity) getActivity()).setToolbarTitle(R.string.askamate_title);
        addMateButton = (Button) homeView.findViewById(R.id.askMate_add_btn);
        mBannerBulletin = (ViewPager) homeView.findViewById(R.id.viewPager);
        indicator = (CirclePageIndicator) homeView.findViewById(R.id.circle_pageIndicator);

     /*   imagesBanner = new int[2];
        imagesBanner[0] = R.drawable.mate_banner_img;
        imagesBanner[1] = R.drawable.mate_banner_img;
*/

        mBannerBulletin.setAdapter(new ImageAdapter(mBannerBulletin.getContext(), imagesBanner));
        indicator.setViewPager(mBannerBulletin);
        indicator.setSnap(true);



        addMateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.askamate_container, new ShowMatesFragment(), "SHOW_MATES");
                transaction.addToBackStack("SHOW_MATES");
                transaction.commit();
            }
        });

        return homeView;

    }

    public class ImageAdapter extends PagerAdapter {
        private final DisplayMetrics matrix;
        Context context;
        int [] imageEntityList;

        ImageAdapter(Context context, int[] imageEntities) {
            this.context = context;
            this.imageEntityList = imageEntities;
            matrix = context.getResources().getDisplayMetrics();
        }

        @Override
        public int getCount() {
            return imageEntityList.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            final ImageView imageView = new ImageView(context);
//            imageView.setBackgroundColor(Color.BLUE);
//            Picasso.with(context)
//                    .load(imageEntityList.get(position).image.toString())
//                    .placeholder(R.drawable.pdp_banner_placeholder)
//                    .resize(matrix.widthPixels, 406)
//                    .centerInside()
//                    .into(imageView);
            if (imageEntityList.length > 0) {
                Picasso.with(context)
                        .load(imageEntityList[position])
                        .placeholder(R.drawable.product_list_placeholder)
                        .fit()
                        .into(imageView);
            } else {
                Picasso.with(context).load(R.drawable.product_list_placeholder).fit().into(imageView);
            }

            container.addView(imageView, 0);
            imageView.setTag("specimen" + position);
            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((ImageView) object);
        }
    }
}
