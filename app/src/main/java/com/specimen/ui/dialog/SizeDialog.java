package com.specimen.ui.dialog;

import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.cart.ICartFacade;
import com.specimen.core.model.ProductSizesEntity;
import com.specimen.core.model.ProductSizesListParable;
import com.specimen.home.OnSizeSelectedListener;
import com.specimen.widget.TouchImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Tejas on 6/1/2015.
 * On PDP after clicking on 'buy', transperant size dialog will be shown.
 * <p/>
 * Edited by Swapnil 8/04/2015
 */
public class SizeDialog extends DialogFragment implements View.OnClickListener {

    ProductSizesListParable mHandpickedItem;
    List<ProductSizesEntity> mSizesList;

    String productId;
    String attributeId;
    String tag;
    OnSizeSelectedListener onSizeSelectedListner;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Translucent_NoTitleBar);

        mHandpickedItem = getArguments().getParcelable("product");
        productId = getArguments().getString("productId");
        attributeId = getArguments().getString("attributeId");
        tag = getArguments().getString("tag");

        if (mHandpickedItem != null) {
            mSizesList = mHandpickedItem.getmList();
        }

//        onSizeSelectedListner = this;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.dialog_frgment_size, container, false);
        LinearLayout llBtnSizesArray = (LinearLayout) rootView.findViewById(R.id.llbtnsizearray);

        final TextView txtSizeGuide = (TextView) rootView.findViewById(R.id.txtSizeGuide);
        SpannableString content = new SpannableString(txtSizeGuide.getText());
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        txtSizeGuide.setText(content);
        txtSizeGuide.setPaintFlags(txtSizeGuide.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        if (mSizesList != null) {
            int size = mSizesList.size();
            for (int i = 0; i < size; i++) {
                if (mSizesList.get(i).getStock_count() != 0) {
                    Button btnSize = new Button(getActivity());
                    setBtnSizeParams(btnSize);
                    btnSize.setText(mSizesList.get(i).getLabel() + "");
                    btnSize.setTag(mSizesList.get(i).getId());

                    if (getSelectedIdStockCount(mSizesList.get(i).getId()) == 0) {
                        btnSize.setEnabled(false);
                        btnSize.setPaintFlags(btnSize.getPaintFlags()| Paint.STRIKE_THRU_TEXT_FLAG);
                    }

                    llBtnSizesArray.addView(btnSize);
                }
            }

            txtSizeGuide.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    RelativeLayout linearLayout = new RelativeLayout(rootView.getContext());
                    linearLayout.setPadding(5, 5, 5, 5);

                    TouchImageView imageView = new TouchImageView(rootView.getContext());
                    imageView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

                    linearLayout.addView(imageView);

                    DisplayMetrics metrics = getActivity().getResources().getDisplayMetrics();
                    AlertDialog.Builder alertDialogBuilder =
                            new AlertDialog.Builder(rootView.getContext(), android.R.style.Theme_Holo_Light_NoActionBar_Fullscreen);
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.setView(linearLayout);
                    Picasso.with(getActivity())
                            .load(mSizesList.get(0).getSize_chart())
                            .resize(metrics.widthPixels, metrics.heightPixels)
                            .placeholder(R.drawable.survey_img_placeholder)
                            .centerInside()
                            .into(imageView);
                    alertDialog.show();
                }
            });

            if (llBtnSizesArray.getChildCount() == 0) {
                Toast.makeText(rootView.getContext(), "Product is out of stock", Toast.LENGTH_SHORT).show();
                dismiss();
            }

        } else {
            dismiss();
        }

        return rootView;
    }

    // Set layout parameters to the dynamically created buttons
    void setBtnSizeParams(Button btn) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(10, 10, 10, 10);
        btn.setLayoutParams(params);
        btn.setTextColor(getActivity().getResources().getColor(R.color.specimen_green));
        btn.setBackgroundResource(R.drawable.size_btn);
        btn.setPadding(15, 5, 15, 5);
        btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String id = v.getTag().toString();
        if (onSizeSelectedListner != null) {
            if (getSelectedIdStockCount(id) == 0) {
                Toast.makeText(getActivity(), "Product is out of stock", Toast.LENGTH_SHORT).show();
                return;
            } else {
//                onSizeSelectedListner.onSizeSelected(id);
                onSizeSelected(id);
                dismiss();
            }
        } else {
            dismiss();
        }
    }

    public OnSizeSelectedListener getOnSizeSelectedListner() {
        return onSizeSelectedListner;
    }

    public void setOnSizeSelectedListner(OnSizeSelectedListener onSizeSelectedListner) {
        this.onSizeSelectedListner = onSizeSelectedListner;
    }

//    @Override
    public void onSizeSelected(String sizeId) {
        onSizeSelectedListner.onSizeSelected("");
        ((ICartFacade) IOCContainer.getInstance().getObject(ServiceName.CART_SERVICE, tag))
                .addToCart(productId, "1", sizeId,
                        attributeId);
    }

    int getSelectedIdStockCount(String id) {
        int remainingStock = 0;
        for (int i = 0; i < mSizesList.size(); i++) {
            if (id.equals(mSizesList.get(i).getId())) {
                return mSizesList.get(i).getStock_count();
            }
        }

        return remainingStock;
    }
}
