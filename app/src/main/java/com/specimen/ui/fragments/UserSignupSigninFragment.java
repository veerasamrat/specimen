package com.specimen.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.specimen.R;
import com.specimen.authentication.SelectAuthenticationProcessActivity;
import com.specimen.authentication.UserAuthenticatePagerAdapter;

/**
 * Created by Tejas on 6/24/2015.
 * The main fragment for signup signin.
 */
public class UserSignupSigninFragment extends Fragment {

    private ViewPager userAuthenticatePager;
    private PagerAdapter userAuthenticatePagerAdapter;
    private TextView textSignup, textLogin;
    AppCompatActivity mActivity;
    int trackLoginProcessPage;

    public UserSignupSigninFragment(int loginProcess) {
        trackLoginProcessPage = loginProcess;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mActivity = (AppCompatActivity) activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup userAuthenticationView = (ViewGroup) getActivity().getLayoutInflater().inflate(R.layout.fragment_usersignupsign, container, false);

        userAuthenticatePager = (ViewPager) userAuthenticationView.findViewById(R.id.userauthenticate_pager);
        userAuthenticatePagerAdapter = new UserAuthenticatePagerAdapter(getChildFragmentManager(), mActivity);
        userAuthenticatePager.setAdapter(userAuthenticatePagerAdapter);
        textSignup = (TextView) userAuthenticationView.findViewById(R.id.textSignup);
        textLogin = (TextView) userAuthenticationView.findViewById(R.id.textLogin);
        textSignup.setBackgroundColor(getResources().getColor(R.color.specimen_grey));
        textLogin.setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));

        textSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userAuthenticatePager.setCurrentItem(0, true);
                textSignup.setBackgroundColor(getResources().getColor(R.color.specimen_grey));
                textLogin.setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));
            }
        });

        textLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userAuthenticatePager.setCurrentItem(1, true);
                textLogin.setBackgroundColor(getResources().getColor(R.color.specimen_grey));
                textSignup.setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));
            }
        });

        userAuthenticatePager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                if (i == 0) {
                    textSignup.setBackgroundColor(getResources().getColor(R.color.specimen_grey));
                    textLogin.setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));
                } else if (i == 1) {
                    textLogin.setBackgroundColor(getResources().getColor(R.color.specimen_grey));
                    textSignup.setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        if (SelectAuthenticationProcessActivity.LOGIN == trackLoginProcessPage) {
            userAuthenticatePager.setCurrentItem(1, true);
        } else {
            userAuthenticatePager.setCurrentItem(0, true);
        }

        return userAuthenticationView;
    }


}
