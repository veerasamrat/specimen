package com.specimen.ui.toplevelfragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.specimen.BaseFragment;
import com.specimen.R;
import com.specimen.authentication.UserAuthenticatePagerAdapter;

/**
 * Created by Tejas on 6/1/2015.
 * Home tab, showing deal page, bulletin, handpicked.
 */
public class HomeFragment extends BaseFragment {

    PagerAdapter specimenHomePagerAdapter;
    ViewPager specimenHomePager;
    private TextView textTicker, textBulletin, textHandpicked;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View homeView = inflater.inflate(R.layout.fragment_specimenhome, container, false);
        specimenHomePager = (ViewPager) homeView.findViewById(R.id.specimenHome_pager);
        specimenHomePagerAdapter = new UserAuthenticatePagerAdapter(getChildFragmentManager(), mContext);
        specimenHomePager.setAdapter(specimenHomePagerAdapter);

        textBulletin = (TextView) homeView.findViewById(R.id.textBulletin);
        textHandpicked = (TextView) homeView.findViewById(R.id.textHandpicked);
        textTicker = (TextView) homeView.findViewById(R.id.textTicker);
        textTicker.setBackgroundColor(getResources().getColor(R.color.specimen_grey));

        specimenHomePager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        textTicker.setBackgroundColor(getResources().getColor(R.color.specimen_grey));
                        break;
                    case 1:
                        textBulletin.setBackgroundColor(getResources().getColor(R.color.specimen_grey));
                        break;
                    case 2:
                        textHandpicked.setBackgroundColor(getResources().getColor(R.color.specimen_grey));
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        textBulletin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                specimenHomePager.setCurrentItem(0, true);
            }
        });

        textHandpicked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                specimenHomePager.setCurrentItem(1, true);
            }
        });

        textTicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                specimenHomePager.setCurrentItem(2, true);
            }
        });

        return homeView;
    }
}
