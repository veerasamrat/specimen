package com.specimen.util;

import android.content.pm.ActivityInfo;
import android.os.Build;

public class AndroidUtils {

    public static int ORIENTATION_FLAG;
    private static boolean isTablet;

    public static void init(String screenType) {
        if (screenType.equalsIgnoreCase("tablet")) {
            isTablet = true;
        }
        setOrientation();
    }

    private static void setOrientation() {

        if (isTablet) {
            ORIENTATION_FLAG = ActivityInfo.SCREEN_ORIENTATION_USER;
        } else {
            ORIENTATION_FLAG = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

        }
    }

    public static boolean isTablet() {
        return isTablet;
    }

    public static boolean isJellyBean() {

        return Build.VERSION.SDK_INT > 16;
    }


}
