package com.specimen.util;

import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

import com.specimen.R;
import com.specimen.core.model.ImageEntity;

/**
 * Created by Swapnil on 8/7/2015.
 */
public class AnimationUtility {

    public static void animateSpied(Context context, ImageView view, int img) {
        ObjectAnimator anim = (ObjectAnimator) AnimatorInflater.loadAnimator(
                context, R.animator.flip);
        anim.setTarget(view);
        anim.setDuration(1000);
        anim.end();
        view.setImageResource(img);
        anim.start();
    }


}
