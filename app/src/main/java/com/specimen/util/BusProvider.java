package com.specimen.util;

import com.squareup.otto.Bus;

/**
 * Created by Jitendra Khetle on 07/07/15.
 */
public final class BusProvider {

    private static final Bus BUS = new Bus();

    public static Bus getInstance() {
        return BUS;
    }

    private BusProvider() {
        // No instances.
    }
}
