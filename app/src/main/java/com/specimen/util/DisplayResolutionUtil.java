package com.specimen.util;

import android.content.Context;
import android.util.DisplayMetrics;

/**
 * Utility class for holding and getting device resolution
 *
 * @author Anand.Shinde
 */
public class DisplayResolutionUtil {
    private static DisplayResolutionUtil instance;
    private DensityType deviceDensity = null;

    public static DisplayResolutionUtil getInstance() {
        if (instance == null) {
            synchronized (DisplayResolutionUtil.class) {
                if (instance == null) {
                    instance = new DisplayResolutionUtil();
                }
            }
        }
        return instance;
    }

    /**
     * Method which will return the device's pixel density
     *
     * @param density - device's density
     * @return DensityType
     */
    private DensityType getDisplayDensity(int density) {
        switch (density) {
            case DisplayMetrics.DENSITY_LOW:
                return DensityType.LDPI;
            case DisplayMetrics.DENSITY_MEDIUM:
                return DensityType.MDPI;
            case DisplayMetrics.DENSITY_HIGH:
                return DensityType.HDPI;
            case DisplayMetrics.DENSITY_XHIGH:
                return DensityType.XHDPI;
            case DisplayMetrics.DENSITY_XXHIGH:
                return DensityType.XXHDPI;
            case DisplayMetrics.DENSITY_XXXHIGH:
                return DensityType.XXXHDPI;
        }
        return DensityType.XHDPI;
    }

    /**
     * Method which will return the device's pixel density
     *
     * @param context - context
     * @return DensityType
     */
    public DensityType getDeviceDensity(Context context) {
        if (deviceDensity == null) {
            deviceDensity = getDisplayDensity(context.getResources().getDisplayMetrics().densityDpi);
        }
        return deviceDensity;
    }

    /**
     * Method which will return the device's pixel density
     *
     * @param displayMetrics - display metrics
     * @return DenisyType
     */
    public DensityType getDeviceDensity(DisplayMetrics displayMetrics) {
        if (deviceDensity == null) {
            deviceDensity = getDisplayDensity(displayMetrics.densityDpi);
        }
        return deviceDensity;
    }

    /**
     * Enum holding various density types applicable in android
     *
     * @author Anand.Shinde
     */
    public enum DensityType {
        LDPI, MDPI, HDPI, XHDPI, XXHDPI, XXXHDPI
    }
}
