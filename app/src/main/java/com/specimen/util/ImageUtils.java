package com.specimen.util;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.TextUtils;
import android.util.DisplayMetrics;

import com.specimen.core.model.ImageEntity;
import com.specimen.core.model.ImageSize;

public class ImageUtils {

    public static Bitmap getCircleBitmap(Bitmap bitmap) {
        return getCircleBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight());
    }

    public static Bitmap getCircleBitmap(Bitmap bitmap, int width, int height) {
        Bitmap croppedBitmap = scaleCenterCrop(bitmap, width, height);
        Bitmap output = Bitmap.createBitmap(width, height, Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();

        final Rect rect = new Rect(0, 0, width, height);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);

        int radius = 0;
        if (width > height) {
            radius = height / 2;
        } else {
            radius = width / 2;
        }

        canvas.drawCircle(width / 2, height / 2, radius, paint);
        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(croppedBitmap, rect, rect, paint);
        if (!bitmap.isRecycled()) {
            bitmap.recycle();
        }
        return output;
    }

    public static Bitmap scaleCenterCrop(Bitmap source, int newHeight,
                                         int newWidth) {
        int sourceWidth = source.getWidth();
        int sourceHeight = source.getHeight();

        float xScale = (float) newWidth / sourceWidth;
        float yScale = (float) newHeight / sourceHeight;
        float scale = Math.max(xScale, yScale);

        float scaledWidth = scale * sourceWidth;
        float scaledHeight = scale * sourceHeight;

        float left = (newWidth - scaledWidth) / 2;
        float top = (newHeight - scaledHeight) / 2;

        RectF targetRect = new RectF(left, top, left + scaledWidth, top
                + scaledHeight);
        Bitmap dest = source;
        if (source.getConfig() != null) {
            dest = Bitmap.createBitmap(newWidth, newHeight, source.getConfig());
        }
        //Resolve 467 crashlytics.
        try {
            Canvas canvas = new Canvas(dest);
            canvas.drawBitmap(source, null, targetRect, null);
        } catch (Exception ignored) {

        }
        return dest;
    }


    public static String buildImageURL(String imageUrl,
                                       String catId, DisplayMetrics metrics) {
        StringBuilder urlBuilder = new StringBuilder();
        urlBuilder.append(imageUrl);
        urlBuilder.append(catId);

        if (metrics.densityDpi >= DisplayMetrics.DENSITY_XHIGH) {
            urlBuilder.append("@2x");
        }

        if (AndroidUtils.isTablet()) {
            urlBuilder.append("~ipad.jpg");
        } else {
            urlBuilder.append("~iphone.jpg");
        }

        return urlBuilder.toString();
    }

    public static String getProductImageUrl(ImageEntity image, DisplayMetrics metrics) {

        if (image == null) {
            return null;
        }

        String url = "";
        ImageSize imageSize = getImageSize(image, metrics);

        if (imageSize == null) {
            url = image.getBest().getUrl();
        } else {
            url = imageSize.getUrl();
        }
/*
        int density = metrics.densityDpi;

        if (density <= DisplayMetrics.DENSITY_MEDIUM && image.getMedium() != null) {
            url = image.getMedium()
                    .getUrl();
        } else if (density <= DisplayMetrics.DENSITY_HIGH && image.getLarge() != null) {
            url = image.getLarge()
                    .getUrl();
        } else if (density <= DisplayMetrics.DENSITY_XHIGH && image.getxLarge() != null) {
            url = image.getxLarge().getUrl();
        } else if (image.getBest() != null) {
            url = image.getBest().getUrl();
        }*/

        return url;
    }

    private static ImageSize getImageSize(ImageEntity image, DisplayMetrics metrics) {
        ImageSize imageSize = null;
        switch (DisplayResolutionUtil.getInstance().getDeviceDensity(metrics)) {
            case LDPI:
                imageSize = image.getSmall();
                break;

            case MDPI:
                imageSize = image.getMedium();
                break;

            case HDPI:
                imageSize = image.getLarge();
                break;

            case XHDPI:
                imageSize = image.getxLarge();
                break;

            case XXHDPI:
                imageSize = image.getXxLarge();
                break;

            case XXXHDPI:
                imageSize = image.getXxxLarge();
                break;
        }
        return imageSize;
    }

    public static String getFullSizeImage(ImageEntity image, DisplayMetrics metrics) {

        if (image == null) {
            return null;
        }

        String xLargeUrl = "";
        if (image.getxLarge() != null) {
            xLargeUrl = image.getxLarge().getUrl();
        }
        //getXLargeUrl();
        String bestUrl = "";
        if (image.getBest() != null) {
            bestUrl = image.getBest().getUrl();
        }
//        List<String> fallback = image.getFullSizeFallbackList();

        return getAppropriateSizeImage(xLargeUrl, bestUrl, metrics);
    }

    public static String getMediumImage(ImageEntity image, DisplayMetrics metrics) {

        if (image == null) {
            return null;
        }

//        String iPhoneUrl = image.getIPhone().getUrl();//IphoneUrl();
//        String xLargeUrl = image.getxLarge().getUrl();

        String xLargeUrl = "";
        if (image.getxLarge() != null) {
            xLargeUrl = image.getxLarge().getUrl();
        }
        //getXLargeUrl();
        String iPhoneUrl = "";
        if (image.getIPhone() != null) {
            iPhoneUrl = image.getBest().getUrl();
        }

        return getAppropriateSizeImage(iPhoneUrl, xLargeUrl, metrics);
    }

    public static String getThumbnailImage(ImageEntity image, DisplayMetrics metrics) {
//        Log.d("*/*/*/*/*/*/*/*/*/ ", "getThumbnailImage");
        if (image == null) {
            return null;
        }

        String mediumUrl = image.getMedium().getUrl();
        String largeUrl = image.getLarge().getUrl();
//        List<String> fallback = image.getThumbnailFallbackList();

        return getAppropriateSizeImage(mediumUrl, largeUrl, metrics);
    }


    private static String getAppropriateSizeImage(String smallSize, String largeSize,
                                                  DisplayMetrics metrics) {
        boolean isHighDensity = metrics.densityDpi >= DisplayMetrics.DENSITY_XHIGH;

        // If we are on a tablet or high density device (or if there is no smaller image)
        // try to use the larger image
        if (
                ((AndroidUtils.isTablet() || isHighDensity) &&
                        (!TextUtils.isEmpty(largeSize))) ||
                        (TextUtils.isEmpty(smallSize))) {
            return largeSize;
        } else if (!TextUtils.isEmpty(smallSize)) {
            return smallSize;
        }
        return null;
    }

}
