package com.specimen.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.pinterest.android.pdk.PDKBoard;
import com.pinterest.android.pdk.PDKCallback;
import com.pinterest.android.pdk.PDKClient;
import com.pinterest.android.pdk.PDKException;
import com.pinterest.android.pdk.PDKResponse;
import com.specimen.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Tejas on 10/6/2015.
 */
public class ShareToPinterest {

    private Context mContext;
    private PDKClient mPDKClient;

    private List scopes = new ArrayList<String>();
    private SharedPreferences mSharedPreference;
    private SharedPreferences.Editor mEditor;

    public ShareToPinterest(Context context) {
        mContext = context;
        mPDKClient = PDKClient.configureInstance(context, context.getString(R.string.pinterest_app_id));
        mPDKClient.onConnect(context);
        mPDKClient.setDebugMode(true);

        scopes.add(PDKClient.PDKCLIENT_PERMISSION_READ_PUBLIC);
        scopes.add(PDKClient.PDKCLIENT_PERMISSION_WRITE_PUBLIC);

        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(context);
        mEditor = mSharedPreference.edit();
    }

    public void postToPinterest(final String productImageURL) {

        if (!isLoginToPinterest()) {
            //logInToPinterest();
        } else {
            mPDKClient.getMyBoards("id,name", new PDKCallback() {
                @Override
                public void onSuccess(PDKResponse response) {
                    super.onSuccess(response);

                    List<String> myBoardIds = new ArrayList<String>();
                    List<PDKBoard> myBoards = response.getBoardList();
                    HashMap<String, String> boardIDMap = new HashMap<String, String>();
                    for (int i = 0; i < myBoards.size(); i++) {
                        myBoardIds.add(myBoards.get(i).getUid());
                        Log.d("board id ", myBoardIds.get(i));
                        boardIDMap.put(myBoards.get(i).getName(), myBoards.get(i).getUid());
                    }

                    mPDKClient.createPin("Specimen note", boardIDMap.get("Specimen"), productImageURL, "http://specimen.intelliswift.co.in", new PDKCallback() {
                        @Override
                        public void onSuccess(PDKResponse response) {
                            super.onSuccess(response);
                            Log.d("pin published", "success");
                        }

                        @Override
                        public void onFailure(PDKException exception) {
                            super.onFailure(exception);
                            Log.d("pin published", "failed");

                        }
                    });
                }

                @Override
                public void onFailure(PDKException exception) {
                    super.onFailure(exception);
                }
            });
        }
    }

    public boolean isLoginToPinterest() {
        boolean isLogin = mSharedPreference.getBoolean("pinterestlogin", false);
        return isLogin;
    }

    public PDKClient getPDKClient() {
        return mPDKClient;
    }

    public void logInToPinterest() {

        if (!isLoginToPinterest()) {

            mPDKClient.login(mContext, scopes, new PDKCallback() {
                @Override
                public void onSuccess(PDKResponse response) {

                    mEditor.putBoolean("pinterestlogin", true);
                    mEditor.putString("pinterestuser", response.getUser().getFirstName() + " " + response.getUser().getLastName());
                    mEditor.commit();

                    //Check if Specimen Board exists...
                    mPDKClient.getMyBoards("id,name", new PDKCallback() {
                        @Override
                        public void onSuccess(PDKResponse response) {
                            super.onSuccess(response);
                            List<String> myBoardNames = new ArrayList<String>();
                            List<PDKBoard> myBoards = response.getBoardList();
                            for (int i = 0; i < myBoards.size(); i++) {
                                Log.d("Pinterest board:", "board " + i + ":" + " " + myBoards.get(i).getName());
                                myBoardNames.add(myBoards.get(i).getName());
                            }
                            if (!myBoardNames.contains("Specimen")) {
                                mPDKClient.createBoard("Specimen", "Your spied items will be shown here", new PDKCallback() {
                                    @Override
                                    public void onSuccess(PDKResponse response) {
                                        super.onSuccess(response);
                                        Log.d("Pinterest board:", "Board created... with id " + response.getBoard().getUid());
                                    }

                                    @Override
                                    public void onFailure(PDKException exception) {
                                        super.onFailure(exception);
                                    }
                                });
                            }
                        }

                        @Override
                        public void onFailure(PDKException exception) {
                            super.onFailure(exception);
                        }
                    });
                    Toast.makeText(mContext, "You are now loggedin as " + response.getUser().getFirstName() + response.getUser().getLastName() + " to Pinterest", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(PDKException exception) {
                    Log.e(getClass().getName(), exception.getDetailMessage());
                }
            });
        } else {
            Toast.makeText(mContext, "You are already loggedin as " + mSharedPreference.getString("pinterestuser", ""), Toast.LENGTH_SHORT).show();

        }
    }
}
