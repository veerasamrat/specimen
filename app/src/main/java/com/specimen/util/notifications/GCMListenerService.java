package com.specimen.util.notifications;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;
import com.specimen.MainActivity;
import com.specimen.R;
import com.specimen.authentication.SelectAuthenticationProcessActivity;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.util.SharedPreferenceHelper;
import com.specimen.trustcircle.TrustCircleActivity;

/**
 * Created by Tejas on 8/11/2015.
 */
public class GCMListenerService extends GcmListenerService {
    private static final String TAG = "RPGcmListenerService";
    public static boolean comingFromNotification = false;

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString("message");
        String title = data.getString("title");
        String product_id = data.getString("prod_id");
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + message);
        Log.d(TAG, "ProductId: " + product_id);
        /**
         * Production applications would usually process the message here.
         * Eg: - Syncing with server.
         *     - Store message in local database.
         *     - Update UI.
         */

        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */
        sendNotification(message, title, product_id);
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */
    private void sendNotification(String message, String title, String product_id) {

        IApplicationFacade applicationFacade = (IApplicationFacade) IOCContainer.getInstance().getObject(ServiceName.APPLICATION_SERVICE, "");
        if (applicationFacade.getUserResponse() == null) {
            Intent loginIntent = new Intent(this, SelectAuthenticationProcessActivity.class);
            PendingIntent loginPending = PendingIntent.getActivity(this, 0, loginIntent, PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.logo)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setContentIntent(loginPending);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
        } else {
            comingFromNotification = true;
            Intent notificationIntent = null;

           boolean isSwitchImageClicked = SharedPreferenceHelper.get(SharedPreferenceHelper.APP_PROPERTIES, "IS_PUSH_NOTIFICATION_ON", false);
             Log.i("IS_PUSH_NOTIFICATION_ON",""+isSwitchImageClicked);
            if (title.equals("Trust Circle Feedback")&& isSwitchImageClicked) {
                notificationIntent = new Intent(this, TrustCircleActivity.class);
                notificationIntent.putExtra("trustee_fragment", "trustee_alert");
                notificationIntent.putExtra("NOTIFICATION", true);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                if (product_id != null)
                    notificationIntent.putExtra("product_id", product_id);
            } else if (title.contains("Deal Starting")) {
                notificationIntent = new Intent(this, MainActivity.class);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                notificationIntent.putExtra("tag", MainActivity.HOME);
//                startActivity(notificationIntent);
            } else if (title.contains("Wishlist Product")) {
                //open PDP here...
//                notificationIntent.putExtra("PRODUCT_ID", product_id);

                notificationIntent = new Intent(this, MainActivity.class);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                notificationIntent.putExtra("tag", MainActivity.SPY);
//                startActivity(notificationIntent);
            }
            if(notificationIntent != null) {
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, notificationIntent,
                        PendingIntent.FLAG_ONE_SHOT);

                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.logo)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setContentIntent(pendingIntent);

                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
            }
        }
    }
}
