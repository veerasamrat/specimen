package com.specimen.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import com.specimen.R;

public class AutoAdjustLayout extends ViewGroup {

    private int line_height;
    private int horizontal_spacing;
    private int vertical_spacing;


    public AutoAdjustLayout(Context context) {
        super(context);
        initViews(context, null);
    }

    public AutoAdjustLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews(context, attrs);
    }

    public AutoAdjustLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews(context, attrs);
    }

    public AutoAdjustLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
        initViews(context, attrs);
    }

    private void initViews(Context context, AttributeSet attrs) {

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.AutoAdjustLayout, 0, 0);

        try {
            // get the text and colors specified using the names in attrs.xml
            horizontal_spacing = a.getDimensionPixelSize(R.styleable.AutoAdjustLayout_horizontalSpacing, 0);
            vertical_spacing = a.getDimensionPixelSize(R.styleable.AutoAdjustLayout_verticalSpacing, 0);

        } finally {
            a.recycle();
        }

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        assert (MeasureSpec.getMode(widthMeasureSpec) != MeasureSpec.UNSPECIFIED);

        final int width = MeasureSpec.getSize(widthMeasureSpec);

        // The next line is WRONG!!! Doesn't take into account requested MeasureSpec mode!
        int height = MeasureSpec.getSize(heightMeasureSpec) - getPaddingTop() - getPaddingBottom();
        final int count = getChildCount();
        int line_height = 0;

        int xpos = getPaddingLeft() + horizontal_spacing;
        int ypos = getPaddingTop() + vertical_spacing;

        int maxSize = 00;
        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
//            final LayoutParams lp = child.getLayoutParams();
            child.measure(
                    MeasureSpec.makeMeasureSpec(width, MeasureSpec.AT_MOST),
                    MeasureSpec.makeMeasureSpec(height, MeasureSpec.UNSPECIFIED));

            int childw = child.getMeasuredWidth();
            if (childw > maxSize) {
                maxSize = childw;
            }
        }


        for (int i = 0; i < count; i++) {
            final View child = getChildAt(i);
            if (child.getVisibility() != GONE) {
                final LayoutParams lp = child.getLayoutParams();
//                child.measure(
//                        MeasureSpec.makeMeasureSpec(width, MeasureSpec.AT_MOST),
//                        MeasureSpec.makeMeasureSpec(height, MeasureSpec.UNSPECIFIED));
////                lp.width=maxSize;
                child.setMinimumWidth(maxSize);
//                final int childw = child.getMeasuredWidth();

                line_height = Math.max(line_height, child.getMeasuredHeight() + lp.height);

                if (xpos + maxSize > width) {
                    xpos = getPaddingLeft();
                    ypos += line_height + vertical_spacing;
                }

                xpos += maxSize + lp.width;
            }
        }
        this.line_height = line_height;

        if (MeasureSpec.getMode(heightMeasureSpec) == MeasureSpec.UNSPECIFIED) {
            height = ypos + line_height;

        } else if (MeasureSpec.getMode(heightMeasureSpec) == MeasureSpec.AT_MOST) {
            if (ypos + line_height < height) {
                height = ypos + line_height;
            }
        }
        setMeasuredDimension(width, height);
    }

    @Override
    protected LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(1, 1); // default of 1px spacing
    }

    @Override
    protected boolean checkLayoutParams(LayoutParams p) {
        return (p instanceof LayoutParams);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        final int count = getChildCount();
        final int width = r - l;
        int xpos = getPaddingLeft() + horizontal_spacing;
        int ypos = getPaddingTop() + vertical_spacing;

        for (int i = 0; i < count; i++) {
            final View child = getChildAt(i);
            if (child.getVisibility() != GONE) {
                final int childw = child.getMeasuredWidth();
                final int childh = child.getMeasuredHeight();
                final LayoutParams lp =  child.getLayoutParams();

                if (xpos + childw > width) {
                    xpos = getPaddingLeft() + horizontal_spacing;
                    ypos += line_height + vertical_spacing;
                }

                child.layout(xpos, ypos, xpos + childw, ypos + childh);
                xpos += childw + lp.width + horizontal_spacing;
            }
        }
    }
}