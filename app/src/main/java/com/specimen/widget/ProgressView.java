package com.specimen.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.specimen.R;

public class ProgressView extends ImageView {

    private Animation a;

    public ProgressView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setAnimation();
    }

    public ProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setAnimation();
    }

    public ProgressView(Context context) {
        super(context);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        switch (getResources().getDisplayMetrics().densityDpi) {
            case DisplayMetrics.DENSITY_LOW:
                setMeasuredDimension(50, 50);
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                setMeasuredDimension(80, 80);
                break;
            case DisplayMetrics.DENSITY_HIGH:
                setMeasuredDimension(100, 100);
                break;
            case DisplayMetrics.DENSITY_XHIGH:
                setMeasuredDimension(120, 120);
                break;
            case DisplayMetrics.DENSITY_XXHIGH:
                setMeasuredDimension(200, 200);
                break;
            case DisplayMetrics.DENSITY_XXXHIGH:
                setMeasuredDimension(200, 200);
                break;
        }
    }

    public void setAnimation() {
        a = AnimationUtils.loadAnimation(getContext(), R.anim.rotate);

    }

    @Override
    public void setVisibility(int visibility) {
        if (visibility == VISIBLE) {
            startAnimation(a);
        } else {
            clearAnimation();
        }
        super.setVisibility(visibility);
    }
}