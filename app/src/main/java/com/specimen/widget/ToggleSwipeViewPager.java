package com.specimen.widget;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by Intelliswift on 7/24/2015.
 */
public class ToggleSwipeViewPager extends ViewPager {

    private boolean isSwipeable=true;

    public ToggleSwipeViewPager(Context context) {
        super(context);
    }

    public ToggleSwipeViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ToggleSwipeViewPager(Context context, AttributeSet attrs,int defStyle) {
        super(context, attrs);
    }


    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        // Never allow swiping to switch between pages
        return isSwipeable;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // Never allow swiping to switch between pages
        return isSwipeable;
    }

    public boolean isSwipeable() {
        return isSwipeable;
    }

    public void setIsSwipeable(boolean isSwipeable) {
        this.isSwipeable = isSwipeable;
    }
}
