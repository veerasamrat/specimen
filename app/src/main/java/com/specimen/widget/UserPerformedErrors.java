package com.specimen.widget;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Tejas on 8/6/2015.
 */
public class UserPerformedErrors {


    private TextView errorTextView;
    private Toast errorToast;

    public UserPerformedErrors(TextView textView) {
        this.errorTextView = textView;
    }

    public UserPerformedErrors(Toast errorToast) {
        this.errorToast = errorToast;
    }
    public UserPerformedErrors(TextView textView, Toast errorToast) {
        this.errorTextView = textView;
        this.errorToast = errorToast;
    }
    public void setErrorMessage(String message) {
        errorTextView.setText(message);
    }

    public void setErrorMessageToast(Context context, String message, View view, int textResource, int duration) {

        errorToast.setDuration(Toast.LENGTH_SHORT);
        if(view != null) {
            errorToast.setView(view);
            TextView textMessage = (TextView) view.findViewById(textResource);
            textMessage.setText(message);
        } else {
            Toast.makeText(context, message, duration).show();
        }
    }

}
