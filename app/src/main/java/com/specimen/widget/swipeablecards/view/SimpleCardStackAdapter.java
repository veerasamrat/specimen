package com.specimen.widget.swipeablecards.view;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.specimen.R;
import com.specimen.util.ImageUtils;
import com.specimen.widget.swipeablecards.model.CardModel;
import com.squareup.picasso.Picasso;


public final class SimpleCardStackAdapter extends CardStackAdapter {

    private DisplayMetrics matrix;

    public SimpleCardStackAdapter(Context mContext) {
        super(mContext);
        matrix = mContext.getResources().getDisplayMetrics();
    }

    @Override
    public View getCardView(int position, CardModel model, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.handpicked_card_layout, parent, false);
            assert convertView != null;
        }

        Picasso.with(getContext())
                .load(ImageUtils.getProductImageUrl(model.getcardImageUrl(), matrix))
                .resize(matrix.widthPixels, matrix.heightPixels)
                .centerInside()
                .placeholder(R.drawable.product_list_placeholder)
                .into(((ImageView) convertView.findViewById(R.id.image)));

        ((TextView) convertView.findViewById(R.id.title)).setText(model.getTitle());
        ((TextView) convertView.findViewById(R.id.description)).setText(getContext().getString(R.string.rupee_symbole)
                + " " + model.getDescription());
        return convertView;
    }
}
