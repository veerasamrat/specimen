package com.specimen.core;

import android.content.Context;

import com.specimen.core.address.AddressFacade;
import com.specimen.core.application.ApplicationFacade;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.askastylist.AskAStylistFacade;
import com.specimen.core.authentication.AuthenticationFacade;
import com.specimen.core.bulletin.BulletinFacade;
import com.specimen.core.cart.CartFacade;
import com.specimen.core.category.CategoryFacade;
import com.specimen.core.citruspayment.CitrusFacade;
import com.specimen.core.deal.DealFacade;
import com.specimen.core.feedback.FeedbackFacade;
import com.specimen.core.filter.FilterFacade;
import com.specimen.core.home.handpicked.HandpickedFacade;
import com.specimen.core.look.LookFacade;
import com.specimen.core.order.OrdersFacade;
import com.specimen.core.product.ProductFacade;
import com.specimen.core.productdetails.ProductDetailsFacade;
import com.specimen.core.search.SearchFacade;
import com.specimen.core.spy.SpyFacade;
import com.specimen.core.spyandcartitemcount.SpyAndCartItemCountFacade;
import com.specimen.core.studio.StudioFacade;
import com.specimen.core.survey.SurveyFacade;
import com.specimen.core.trustcircle.TrustCircleFacade;

import java.util.WeakHashMap;

/**
 * Created by Intelliswift on 6/16/2015.
 */

public class IOCContainer {

    public static String BaseURL;
    private final static WeakHashMap<Integer, Object> objectContainer = new WeakHashMap<>();
    private static IOCContainer instance;
    public ResponsePublisher publisher;
    private Context context;


    private IOCContainer() {
        publisher = new ResponsePublisher();
    }

    public static IOCContainer getInstance() {

        if (instance == null) {
            instance = new IOCContainer();
        }
        return instance;
    }


    public Context getContext() {
        return context;
    }

    public void init(Context context) {
        this.context = context;
    }

    public IBaseFacade getObject(Integer name, String tag) {
        IBaseFacade object = (IBaseFacade) objectContainer.get(name);
        switch (name) {
            case ServiceName.AUTHENTICATION_SERVICE:
                object = new AuthenticationFacade(publisher, (IApplicationFacade) getObject(ServiceName.APPLICATION_SERVICE, tag));
                break;
            case ServiceName.DEAL_SERVICE:
                object = new DealFacade(publisher, (IApplicationFacade) getObject(ServiceName.APPLICATION_SERVICE, tag));
                break;
            case ServiceName.CATEGORY_SERVICE:
                object = new CategoryFacade(publisher, (IApplicationFacade) getObject(ServiceName.APPLICATION_SERVICE, tag));
                break;
            case ServiceName.PRODUCT_SERVICE:
                object = new ProductFacade(publisher, (IApplicationFacade) getObject(ServiceName.APPLICATION_SERVICE, tag));
                break;
            case ServiceName.PRODUCT_DETAILS_SERVICE:
                object = new ProductDetailsFacade(publisher, (IApplicationFacade) getObject(ServiceName.APPLICATION_SERVICE, tag));
                break;
            case ServiceName.SPY_SERVICE:
                object = new SpyFacade(publisher, (IApplicationFacade) getObject(ServiceName.APPLICATION_SERVICE, tag));
                break;
            case ServiceName.CART_SERVICE:
                object = new CartFacade(publisher, (IApplicationFacade) getObject(ServiceName.APPLICATION_SERVICE, tag));
                break;
            case ServiceName.HANDPICKED_SERVICE:
                object = new HandpickedFacade(publisher, (IApplicationFacade) getObject(ServiceName.APPLICATION_SERVICE, tag));
                break;
            case ServiceName.APPLICATION_SERVICE:
                object = new ApplicationFacade();
                break;
            case ServiceName.FILTER_SERVICE:
                object = new FilterFacade(publisher, (IApplicationFacade) getObject(ServiceName.APPLICATION_SERVICE, tag));
                break;
            case ServiceName.SURVEY_SERVICE:
                object = new SurveyFacade(publisher, (IApplicationFacade) getObject(ServiceName.APPLICATION_SERVICE, tag));
                break;
            case ServiceName.ADDRESS_SERVICE:
                object = new AddressFacade(publisher, (IApplicationFacade) getObject(ServiceName.APPLICATION_SERVICE, tag));
                break;
            case ServiceName.TRUST_CIRCLE_SERVICE:
                object = new TrustCircleFacade(publisher, (IApplicationFacade) getObject(ServiceName.APPLICATION_SERVICE, tag));
                break;
            case ServiceName.ORDER_SERVICE:
                object = new OrdersFacade(publisher, (IApplicationFacade) getObject(ServiceName.APPLICATION_SERVICE, tag));
                break;
            case ServiceName.LOOK_SERVICE:
                object = new LookFacade(publisher, (IApplicationFacade) getObject(ServiceName.APPLICATION_SERVICE, tag));
                break;
            case ServiceName.STUDIO_SERVICE:
                object = new StudioFacade(publisher, (IApplicationFacade) getObject(ServiceName.APPLICATION_SERVICE, tag));
                break;
            case ServiceName.ASKSTYLIST:
                object = new AskAStylistFacade(publisher, (IApplicationFacade) getObject(ServiceName.APPLICATION_SERVICE, tag));
                break;
            case ServiceName.SEARCH_SERVICE:
                object = new SearchFacade(publisher, (IApplicationFacade) getObject(ServiceName.APPLICATION_SERVICE, tag));
                break;
            case ServiceName.PAYMENT_SERVICE:
                object = new CitrusFacade(publisher, (IApplicationFacade) getObject(ServiceName.APPLICATION_SERVICE, tag));
                break;
            case ServiceName.FEEDBACK_SERVICE:
                object = new FeedbackFacade(publisher, (IApplicationFacade) getObject(ServiceName.APPLICATION_SERVICE, tag));
                break;
            case ServiceName.BULLETIN_SERVICE:
                object = new BulletinFacade(publisher, (IApplicationFacade) getObject(ServiceName.APPLICATION_SERVICE, tag));
                break;
            case ServiceName.SPYANDCART_ITEM_COUNT_SERVICE:
                object = new SpyAndCartItemCountFacade(publisher, (IApplicationFacade) getObject(ServiceName.APPLICATION_SERVICE, tag));
                break;
        }
        if (object != null) {
            object.setTag(tag);
        }
        return object;
    }
}
