package com.specimen.core;

import com.specimen.core.response.APIResponse;

public interface IResponseSubscribe {

    void onSuccess(APIResponse response, String tag);

    void onFailure(Exception error);


}
