package com.specimen.core;

import com.specimen.core.response.APIResponse;

import java.util.concurrent.CopyOnWriteArrayList;

public class ResponsePublisher implements IResponseSubscribe {

    private final CopyOnWriteArrayList<IResponseSubscribe> responseObservers;

    public ResponsePublisher() {
        responseObservers = new CopyOnWriteArrayList<>();
    }

    public void registerResponseSubscribe(IResponseSubscribe responseObserver) {
        if (!responseObservers.contains(responseObserver)) {
            responseObservers.add(responseObserver);
        }

    }

    public void unregisterResponseSubscribe(IResponseSubscribe responseObserver) {
        responseObservers.remove(responseObserver);
    }


    @Override
    public void onSuccess(APIResponse response, String tag) {
        System.err.println("ResponsePublisher.onResponse()");

        for (IResponseSubscribe observer : responseObservers) {
            observer.onSuccess(response, tag);
        }

    }

    @Override
    public void onFailure(Exception error) {
        System.err.println("ResponsePublisher.onErrorResponse()");

        for (IResponseSubscribe observer : responseObservers) {
            observer.onFailure(error);
        }
    }


}
