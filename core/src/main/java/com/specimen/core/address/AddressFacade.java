package com.specimen.core.address;

import com.specimen.core.BaseFacade;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.model.Address;
import com.specimen.core.request.AddressRequestBuilder;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.AddressResponse;
import com.specimen.core.response.LocalityResponse;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Intelliswift on 7/2/2015.
 */
public class AddressFacade extends BaseFacade implements IAddressFacade {


    private final AddressRequestBuilder addressRequestBuilder;

    public AddressFacade(IResponseSubscribe responseSubscribe, IApplicationFacade applicationFacade) {
        super(responseSubscribe, applicationFacade);
        this.addressRequestBuilder = new AddressRequestBuilder();
    }

    @Override
    public void getStoredAddress() {
        try {

//            AddressResponse addressResponse = (AddressResponse) mApplicationFacade.getOfflineResponse(AddressResponse.class.getSimpleName());
//
//            if (addressResponse != null) {
//                mResponseSubscribe.onSuccess(addressResponse, TAG);
//                return;
//            }
//

//            HashMap<String, Object> bodyParameter = getRequestBody("getStoredAddress");

            addressRequestBuilder.getService().getStoredAddress(getRequestBody("getStoredAddress"), new Callback<AddressResponse>() {
                @Override
                public void success(AddressResponse addressResponse, Response response) {

//                    if (addressResponse.getData() != null || addressResponse.getData().size() > 0) {
//                        //TODO:Kept offline
//                        mApplicationFacade.setResponse(AddressResponse.class.getSimpleName(), addressResponse);
//                    }

                    mResponseSubscribe.onSuccess(addressResponse, TAG);
                }

                @Override
                public void failure(RetrofitError error) {
                    mResponseSubscribe.onFailure(error);
                }
            });
        } catch (Exception e) {

//            AddressResponse response = new AddressResponse();
//            response.setData(null);
//            response.setResult("Fail");
//            /**
//             * User yet not logged in
//             */
//            if (e instanceof NullPointerException) {
//                //TODO work on this custom error message strings
//                response.setMessage("NullPointerException");
//            }
//            mResponseSubscribe.onSuccess(response, TAG);
        }
    }

    @Override
    public void addNewAddress(final Address address) {

        try {

            HashMap<String, Object> bodyParameter = getRequestBody("addNewAddress");
            bodyParameter.put("name", address.getName());
            bodyParameter.put("address", address.getAddress());
            bodyParameter.put("address1", address.getAddress1());
            bodyParameter.put("address2", address.getAddress2());
            bodyParameter.put("pincode", address.getPincode());
            bodyParameter.put("landmark", address.getLandmark());
            bodyParameter.put("locality", address.getLocality());
            bodyParameter.put("district", address.getDistrict());
            bodyParameter.put("mobile_no", address.getMobile_no());
            bodyParameter.put("isPrimary", address.getIsPrimary());
            bodyParameter.put("city", address.getCity());
            bodyParameter.put("state", address.getState());
            bodyParameter.put("country", "IN");

//            addressRequestBuilder.getService().addNewAddress(bodyParameter, new Callback<Address>() {
            addressRequestBuilder.getService().addNewAddress(bodyParameter, new Callback<AddressResponse>() {
                @Override
                public void success(AddressResponse addressResponse, Response response) {
//                    address.setId(addressResponse.getId());
//                    ((AddressResponse) mApplicationFacade.getOfflineResponse(AddressResponse.class.getSimpleName())).getData().add(address);
                    mResponseSubscribe.onSuccess(addressResponse, TAG);
                }

                @Override
                public void failure(RetrofitError error) {
                    mResponseSubscribe.onFailure(error);
                }
            });

        } catch (Exception e) {

//            AddressResponse response = new AddressResponse();
//            response.setData(null);
//            response.setResult("Fail");
//            /**
//             * User yet not logged in
//             */
//            if (e instanceof NullPointerException) {
//                //TODO work on this custom error message strings
//                response.setMessage("NullPointerException");
//            }
//            mResponseSubscribe.onSuccess(response, TAG);
        }

    }

    @Override
    public void deleteStoreAddress(final Address address) {

        HashMap<String, Object> bodyParameter = getRequestBody("deleteStoredAddress");
        bodyParameter.put("addressId", address.getId());
        addressRequestBuilder.getService().deleteStoreAddress(bodyParameter, new Callback<AddressResponse>() {
            @Override
            public void success(AddressResponse addressResponse, Response response) {

//                ((AddressResponse) mApplicationFacade.getOfflineResponse(AddressResponse.class.getSimpleName())).getData().remove(address);
                mResponseSubscribe.onSuccess(addressResponse, TAG);

            }

            @Override
            public void failure(RetrofitError error) {
                failure(error);
            }
        });

    }

    @Override
    public void changeStoreAddress(Address address) {
        HashMap<String, Object> bodyParameter = getRequestBody("changeStoredAddress");
        bodyParameter.put("addressId", address.getId());
        bodyParameter.put("name", address.getName());
        bodyParameter.put("address", address.getAddress());
        bodyParameter.put("address1", address.getAddress1());
        bodyParameter.put("address2", address.getAddress2());
        bodyParameter.put("pincode", address.getPincode());
        bodyParameter.put("landmark", address.getLandmark());
        bodyParameter.put("locality", address.getLocality());
        bodyParameter.put("district", address.getDistrict());
        bodyParameter.put("mobile_no", address.getMobile_no());
        bodyParameter.put("isPrimary", address.getIsPrimary());
        bodyParameter.put("city", address.getCity());
        bodyParameter.put("state", address.getState());
        bodyParameter.put("country", "IN");

        addressRequestBuilder.getService().changeStoreAddress(bodyParameter, new Callback<AddressResponse>() {
            @Override
            public void success(AddressResponse addressResponse, Response response) {
                mResponseSubscribe.onSuccess(addressResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }

    @Override
    public void changePrimaryAddress(Address address) {
        HashMap<String, Object> bodyParameter = getRequestBody("changeSelectedAddress");
        bodyParameter.put("addressId", address.getId());
        addressRequestBuilder.getService().changePrimaryAddress(bodyParameter, new Callback<AddressResponse>() {
            @Override
            public void success(AddressResponse addressResponse, Response response) {
                mResponseSubscribe.onSuccess(addressResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }

    @Override
    public void checkavailableZipcodes(String zipCode) {
        HashMap<String, Object> bodyParameter = getRequestBody("checkavailableZipcodes");
        bodyParameter.put("zipcode", zipCode);
        addressRequestBuilder.getService().checkavailableZipcodes(bodyParameter, new Callback<APIResponse>() {
            @Override
            public void success(APIResponse apiResponse, Response response) {
                mResponseSubscribe.onSuccess(apiResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }

    @Override
    public void zipcodeAutoArea(String zipCode) {
        HashMap<String, Object> bodyParameter = getRequestBody("zipcodeAutoArea");
        bodyParameter.put("zipcode", zipCode);
        addressRequestBuilder.getService().zipcodeAutoArea(bodyParameter, new Callback<LocalityResponse>() {
            @Override
            public void success(LocalityResponse apiResponse, Response response) {
                mResponseSubscribe.onSuccess(apiResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }
}
