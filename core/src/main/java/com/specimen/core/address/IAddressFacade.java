package com.specimen.core.address;

import com.specimen.core.IBaseFacade;
import com.specimen.core.model.Address;

/**
 * Created by Intelliswift on 6/17/2015.
 */
public interface IAddressFacade extends IBaseFacade {

    void getStoredAddress();

    void addNewAddress(Address address);

    void deleteStoreAddress(Address address);

    void changeStoreAddress(Address address);

    void changePrimaryAddress(Address address);

    void checkavailableZipcodes(String zipCode);

    void zipcodeAutoArea(String zipCode);
}
