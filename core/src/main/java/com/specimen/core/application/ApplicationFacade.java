package com.specimen.core.application;

import android.text.TextUtils;

import com.specimen.core.ApplicationObjectsCollectionPool;
import com.specimen.core.BaseFacade;
import com.specimen.core.response.UserResponse;
import com.specimen.core.util.GSONHelper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Intelliswift on 6/18/2015.
 */
public class ApplicationFacade extends BaseFacade implements IApplicationFacade {

    private IApplicationPersistence applicationPersistence;

    public ApplicationFacade() {
        super(null, null);
        applicationPersistence = new ApplicationPersistence();
    }


    @Override
    public Object getResponse(String key) {
        return ApplicationObjectsCollectionPool.getInstance().get(key);
    }



    @Override
    public void setResponse(String key, Object value) {
        ApplicationObjectsCollectionPool.getInstance().put(key, value);
    }


    @Override
    public UserResponse getUserResponse() {

        UserResponse response = getUserResponseFromContext();

        if (response == null) {

            if (isOfflineLogin()) {

                return getUserResponseFromContext();
            }

        }

        return response;
    }

    private UserResponse getUserResponseFromContext() {
        UserResponse userResponse = (UserResponse) ApplicationObjectsCollectionPool.getInstance()
                .get(UserResponse.class.getSimpleName());
        if (userResponse != null && userResponse.getData() != null && userResponse.getData().getId() != null ) {
            return userResponse;
        } else {
            return null;
        }
//        return userResponse;
    }

    public boolean isOfflineLogin() {

        String response = applicationPersistence.getOfflineUser();

        if (!TextUtils.isEmpty(response)) {
            setUserResponse(GSONHelper.getInstance().fromJson(response,
                    UserResponse.class));

            return true;
        }

        return false;
    }

    private void putUserResponse(UserResponse response) {
        ApplicationObjectsCollectionPool.getInstance().put(
                UserResponse.class.getSimpleName(), response);
    }

    @Override
    public void setUserResponse(UserResponse response) {
        putUserResponse(response);
        applicationPersistence.saveUserResponse(response);
    }

    @Override
    public String getOfflineResponse(String key) {
        return applicationPersistence.getOfflineResponse(key);
    }
    @Override
    public void setOfflineResponse(String key, String value) {
        applicationPersistence.setOfflineResponse(key,value);
    }

}
