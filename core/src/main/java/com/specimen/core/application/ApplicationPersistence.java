package com.specimen.core.application;

import com.specimen.core.response.UserResponse;
import com.specimen.core.util.GSONHelper;
import com.specimen.core.util.SharedPreferenceHelper;


class ApplicationPersistence implements IApplicationPersistence {


    public String getOfflineUser() {
        return SharedPreferenceHelper.get(
                SharedPreferenceHelper.CACHED_RESPONSES,
                UserResponse.class.getSimpleName(), "");
    }

    public void saveUserResponse(Object response) {
        SharedPreferenceHelper.put(SharedPreferenceHelper.CACHED_RESPONSES,
                UserResponse.class.getSimpleName(), GSONHelper.getInstance()
                        .toJson(response));
    }

    @Override
    public String getOfflineResponse(String key) {

        return SharedPreferenceHelper.get(
                SharedPreferenceHelper.CACHED_RESPONSES, key, null);
    }

    @Override
    public void setOfflineResponse(String key, String value) {
        SharedPreferenceHelper.put(
                SharedPreferenceHelper.CACHED_RESPONSES,
                key, value);
    }


}
