package com.specimen.core.application;

import com.specimen.core.IBaseFacade;
import com.specimen.core.response.UserResponse;

/**
 * Created by Intelliswift on 6/18/2015.
 */
public interface IApplicationFacade extends IBaseFacade {

    UserResponse getUserResponse();

    void setUserResponse(UserResponse userResponse);

    void setResponse(String key, Object value);

    String getOfflineResponse(String key);

    Object getResponse(String key);

    void setOfflineResponse(String key, String value);
}
