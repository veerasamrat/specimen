package com.specimen.core.askastylist;

import com.specimen.core.IBaseFacade;
import com.specimen.core.response.AskStylistImagesData;

import java.util.List;

/**
 * Created by root on 5/9/15.
 */
public interface IAskAStylistFacade extends IBaseFacade {

    void askastylistOccasion();

    void askastylist(String occassion, List<AskStylistImagesData.AskStylistImagesEntity> stylistImagesDataList, String question, String size);

}

