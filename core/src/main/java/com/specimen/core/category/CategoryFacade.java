package com.specimen.core.category;

import com.specimen.core.BaseFacade;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.request.CategoryRequestBuilder;
import com.specimen.core.response.CategoryListResponse;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Intelliswift on 6/17/2015.
 */
public class CategoryFacade extends BaseFacade implements ICategoryFacade {

    private final CategoryRequestBuilder categoryRequestBuilder;

    public CategoryFacade(IResponseSubscribe responseSubscribe, IApplicationFacade applicationFacade) {
       super(responseSubscribe,applicationFacade);
        this.categoryRequestBuilder = new CategoryRequestBuilder();

    }


//    @Override
//    public void getCategoryInfo(String catId) {
//
//        HashMap<String, String> bodyParameter = getBodyParameterMap("getCategoryDetails");
//        bodyParameter.put("cat_id", catId);//: 3
//
//        categoryRequestBuilder.getService().getCategoryInfo(bodyParameter, new Callback<CategoryInfoResponse>() {
//            @Override
//            public void success(CategoryInfoResponse categoryInfoResponse, Response response) {
//
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//
//            }
//        });
//    }
//
//    @Override
//    public void getTopCategories() {
//        HashMap<String, String> bodyParameter = getBodyParameterMap("getTopCategories");
//
//        categoryRequestBuilder.getService().getTopCategories(bodyParameter, new Callback<TopCategoryResponse>() {
//            @Override
//            public void success(TopCategoryResponse topCategoryResponse, Response response) {
//
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//
//            }
//        });
//    }

    @Override
    public void getCategoryList() {

        CategoryListResponse categoryListResponse = (CategoryListResponse) mApplicationFacade.getResponse(CategoryListResponse.class.getSimpleName());

        if (categoryListResponse != null) {
            mResponseSubscribe.onSuccess(categoryListResponse, TAG);
            return;
        }

        HashMap<String, Object> bodyParameter = getRequestBody("categoryList");
        categoryRequestBuilder.getService().getCategoryList(bodyParameter, new Callback<CategoryListResponse>() {
            @Override
            public void success(CategoryListResponse categoryListResponse, Response response) {
                /**
                 * need to kept offline and send it back to publisher
                 **/
                if (categoryListResponse!=null && categoryListResponse.getData() != null || categoryListResponse.getData().size() > 0) {
                    //TODO:Kept offline
                    mApplicationFacade.setResponse(CategoryListResponse.class.getSimpleName(), categoryListResponse);
                }
                mResponseSubscribe.onSuccess(categoryListResponse, TAG);

            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }

}
