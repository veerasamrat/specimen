package com.specimen.core.citruspayment;

import android.app.Activity;
import android.widget.Toast;

import com.citrus.sdk.Callback;
import com.citrus.sdk.CitrusClient;
import com.citrus.sdk.CitrusUser;
import com.citrus.sdk.TransactionResponse;
import com.citrus.sdk.classes.Amount;
import com.citrus.sdk.classes.CitrusException;
import com.citrus.sdk.classes.Month;
import com.citrus.sdk.classes.Year;
import com.citrus.sdk.payment.DebitCardOption;
import com.citrus.sdk.payment.PaymentType;
import com.citrus.sdk.response.CitrusError;

/**
 * Created by root on 30/9/15.
 */
public class CitrusCardPayment {

    Activity mActivity;

    public CitrusCardPayment(Activity activity) {
        this.mActivity = activity;
    }


    public void debitCardPayment(String cardHolderName,
                                 String cardNumber,
                                 String cardCVV,
                                 String cardExpiryMonth,
                                 String cardExpiryYear,
                                 String totalAmount) {

        CitrusClient citrusClient = CitrusClient.getInstance(mActivity); // Activity Context
        citrusClient.enableLog(Constants.enableLogging);
        citrusClient.init(Constants.SIGNUP_ID, Constants.SIGNUP_SECRET, Constants.SIGNIN_ID, Constants.SIGNIN_SECRET, Constants.VANITY, Constants.environment);
        // No need to call init on CitrusClient if already done.
        DebitCardOption debitCardOption
                = new DebitCardOption(cardHolderName, cardNumber, cardCVV, Month.getMonth(cardExpiryMonth), Year.getYear(cardExpiryYear));
        Amount amount = new Amount(totalAmount);

        // Init PaymentType
        PaymentType.PGPayment pgPayment = null;
        try {
            pgPayment = new PaymentType.PGPayment(amount, Constants.BILL_URL + totalAmount, debitCardOption,
                    new CitrusUser("mohanish.nerurkar@intelliswift.co.in", "9833996894"));
        } catch (CitrusException e) {
            e.printStackTrace();
        }

        citrusClient.pgPayment(pgPayment, new Callback<TransactionResponse>() {
                    @Override
                    public void success(TransactionResponse transactionResponse) {
                        Toast.makeText(mActivity, "" + transactionResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void error(CitrusError error) {
                        Toast.makeText(mActivity, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }
}
