package com.specimen.core.citruspayment;

/**
 * Created by root on 3/10/15.
 */
public interface ICitrusFacade {

    void createOrder(String addressId, String paymentMethod);

    void paymentByDebitCard();

    void paymentByCreditCard();
}
