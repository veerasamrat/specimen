package com.specimen.core.filter;

import com.specimen.core.IBaseFacade;

/**
 * Created by Intelliswift on 7/10/2015.
 */
public interface IFilterFacade extends IBaseFacade {

    void getFilter();

    void clearFilterResponse();
}
