package com.specimen.core.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Address implements Parcelable {


    private String pincode;
    private String address;
    private String address1, address2;
    public boolean isPrimary;
    private String district;
    private String name;
    private String locality;
    private String mobile_no;
    private String id;
    private String landmark;
    private String city;
    private String state;
    private String country;


    private String cod;

    public Address() {

    }

    protected Address(Parcel in) {
        pincode = in.readString();
        address = in.readString();
        address1 = in.readString();
        address2 = in.readString();
        isPrimary = Boolean.parseBoolean(in.readString());
        district = in.readString();
        name = in.readString();
        locality = in.readString();
        mobile_no = in.readString();
        id = in.readString();
        landmark = in.readString();
        city = in.readString();
        state = in.readString();
        country = in.readString();
    }

    public static final Creator<Address> CREATOR = new Creator<Address>() {
        @Override
        public Address createFromParcel(Parcel in) {
            return new Address(in);
        }

        @Override
        public Address[] newArray(int size) {
            return new Address[size];
        }
    };

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean getIsPrimary() {
        return isPrimary;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public void setIsPrimary(boolean isPrimary) {
        this.isPrimary = isPrimary;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(pincode);
        parcel.writeString(address);
        parcel.writeString(address1);
        parcel.writeString(address2);
        parcel.writeString(String.valueOf(isPrimary));
        parcel.writeString(district);
        parcel.writeString(name);
        parcel.writeString(locality);
        parcel.writeString(mobile_no);
        parcel.writeString(id);
        parcel.writeString(landmark);
        parcel.writeString(city);
        parcel.writeString(state);
        parcel.writeString(country);
    }
}