package com.specimen.core.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AssociateProduct {

@SerializedName("asso_pid")
@Expose
private String assoPid;
@SerializedName("asso_sku")
@Expose
private String assoSku;
@SerializedName("asso_name")
@Expose
private String assoName;
@SerializedName("asso_qty")
@Expose
private String assoQty;

/**
* 
* @return
* The assoPid
*/
public String getAssoPid() {
return assoPid;
}

/**
* 
* @param assoPid
* The asso_pid
*/
public void setAssoPid(String assoPid) {
this.assoPid = assoPid;
}

/**
* 
* @return
* The assoSku
*/
public String getAssoSku() {
return assoSku;
}

/**
* 
* @param assoSku
* The asso_sku
*/
public void setAssoSku(String assoSku) {
this.assoSku = assoSku;
}

/**
* 
* @return
* The assoName
*/
public String getAssoName() {
return assoName;
}

/**
* 
* @param assoName
* The asso_name
*/
public void setAssoName(String assoName) {
this.assoName = assoName;
}

/**
* 
* @return
* The assoQty
*/
public String getAssoQty() {
return assoQty;
}

/**
* 
* @param assoQty
* The asso_qty
*/
public void setAssoQty(String assoQty) {
this.assoQty = assoQty;
}

}