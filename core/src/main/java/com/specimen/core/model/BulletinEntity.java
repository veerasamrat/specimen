package com.specimen.core.model;

import java.util.List;

public class BulletinEntity {
    /**
     * name : Banner
     * type : 0
     * position : 1
     * data : [{"image":"http://specimen.intelliswift.in/media//trending/trending/images3_b1_1.jpg","type":"cat/prod","id":1},{"image":"http://specimen.intelliswift.in/media//trending/trending/images3_b1_1.jpg","type":"cat/prod","id":2},{"image":"http://specimen.intelliswift.in/media//trending/trending/images3_b1_1.jpg","type":"cat/prod","id":3}]
     */

    private String name;
    private int type;
    private int position;
    private List<BulletinData> data;

    public void setName(String name) {
        this.name = name;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setData(List<BulletinData> data) {
        this.data = data;
    }

    public String getName() {
        return name;
    }

    public int getType() {
        return type;
    }

    public int getPosition() {
        return position;
    }

    public List<BulletinData> getData() {
        return data;
    }

}