package com.specimen.core.model;

import com.specimen.core.response.APIResponse;

import java.util.List;

/**
 * Created by Rohit on 10/23/2015.
 */
public class BulletinResponse extends APIResponse {


    /**
     * data : [{"name":"Banner","type":0,"position":1,"data":[{"image":"http://specimen.intelliswift.in/media//trending/trending/images3_b1_1.jpg","type":"cat/prod","id":1},{"image":"http://specimen.intelliswift.in/media//trending/trending/images3_b1_1.jpg","type":"cat/prod","id":2},{"image":"http://specimen.intelliswift.in/media//trending/trending/images3_b1_1.jpg","type":"cat/prod","id":3}]},{"name":"Banner","type":0,"position":5,"data":[{"image":"http://specimen.intelliswift.in/media//trending/trending/images3_b1_1.jpg","type":"cat/prod","id":10},{"image":"http://specimen.intelliswift.in/media//trending/trending/images3_b1_1.jpg","type":"cat/prod","id":10},{"image":"http://specimen.intelliswift.in/media//trending/trending/images3_b1_1.jpg","type":"cat/prod","id":10}]},{"name":"collection","type":1,"position":6,"data":[]},{"name":"explore","type":2,"position":7,"data":[]},{"name":"bestselling","type":3,"position":8,"data":[]},{"name":"recently_viewed","type":4,"position":9,"data":[]},{"name":"videos","type":5,"position":9,"data":[]},{"name":"blogs","type":6,"position":10,"data":[{"id":"1","title":"New Trendy Look ","url":" ","image":"http://specimen.intelliswift.in/media//blog/1445596904_Flannel.jpg"},{"id":"2","title":"New Trendy Look ","url":" ","image":"http://specimen.intelliswift.in/media//blog/1445596904_Flannel.jpg"}]}]
     */

    private List<BulletinEntity> data;

    public void setData(List<BulletinEntity> data) {
        this.data = data;
    }

    public List<BulletinEntity> getData() {
        return data;
    }


}
