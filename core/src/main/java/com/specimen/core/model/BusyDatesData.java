package com.specimen.core.model;
import java.util.ArrayList;
import java.util.List;

public class BusyDatesData {

    private String studioId;
    private List<String> busy_dates = new ArrayList<>();

    /**
     *
     * @return
     * The studioId
     */
    public String getStudioId() {
        return studioId;
    }

    /**
     *
     * @param studioId
     * The studio_id
     */
    public void setStudioId(String studioId) {
        this.studioId = studioId;
    }

    /**
     *
     * @return
     * The busyDates
     */
    public List<String> getBusyDates() {
        return busy_dates;
    }

    /**
     *
     * @param busyDates
     * The busy_dates
     */
    public void setBusyDates(List<String> busyDates) {
        this.busy_dates = busyDates;
    }

}