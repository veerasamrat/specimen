package com.specimen.core.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Collection extends Category {
    protected Collection(Parcel in) {
        super(in);
    }
    public static final Parcelable.Creator<Collection> CREATOR = new Parcelable.Creator<Collection>() {
        @Override
        public Collection createFromParcel(Parcel in) {
            return new Collection(in);
        }

        @Override
        public Collection[] newArray(int size) {
            return new Collection[size];
        }
    };
}