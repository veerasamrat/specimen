package com.specimen.core.model;

/**
 * Created by DhirajK on 12/7/2015.
 */
public class CreateOrder {
    private String txn_id;

    /**
     * @return The txnId
     */
    public String getTxnId() {
        return txn_id;
    }

    /**
     * @param txnId The txn_id
     */
    public void setTxnId(String txnId) {
        this.txn_id = txnId;
    }

}
