package com.specimen.core.model;

import java.util.List;

/**
 * Created by root on 19/8/15.
 */
public class DealsEntity {

    /**
     * image : {"Small":{"sizeName":"Small","actualWidth":31,"width":32,"actualHeight":40,"url":"http://resources.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_small.jpg","height":40},"XLarge":{"sizeName":"XLarge","actualWidth":264,"width":328,"actualHeight":344,"url":"http://resources.shopstyle.com/xim/36/30/3630ea5625adac7abc61f61e8cf7b926.jpg","height":410},"Medium":{"sizeName":"Medium","actualWidth":107,"width":112,"actualHeight":140,"url":"http://resources.shopstyle.com/sim/36/30/3630ea5625adac7abc61f61e8cf7b926_medium/burberry-infant-s-cashmere-dress.jpg","height":140},"Large":{"sizeName":"Large","actualWidth":157,"width":164,"actualHeight":205,"url":"http://resources.shopstyle.com/sim/36/30/3630ea5625adac7abc61f61e8cf7b926/burberry-infant-s-cashmere-dress.jpg","height":205},"IPhoneSmall":{"sizeName":"IPhoneSmall","actualWidth":96,"width":100,"actualHeight":125,"url":"http://resources.shopstyle.com/mim/36/30/3630ea5625adac7abc61f61e8cf7b926_small.jpg","height":125},"Best":{"sizeName":"Best","actualWidth":264,"width":720,"actualHeight":344,"url":"http://bim.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_best.jpg","height":900},"Original":{"sizeName":"Original","actualWidth":264,"actualHeight":344,"url":"http://bim.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_best.jpg"},"IPhone":{"sizeName":"IPhone","actualWidth":264,"width":288,"actualHeight":344,"url":"http://resources.shopstyle.com/mim/36/30/3630ea5625adac7abc61f61e8cf7b926.jpg","height":360}}
     * spied_count : 222
     * end_time : 2015-04-30 00:00:00
     * remaining_stock : 500
     * total_stock : 2000
     * isSpied : true
     * start_time : 2015-04-10 00:00:00
     * remaining_time : 02:30:00
     * sizes : [{"stock_count":200,"label":"28","id":"35"},{"stock_count":200,"label":"32","id":"5"}]
     * price : USD 88.00
     * display_msg :
     * name : Fernbuckles washed-chinos
     * id : 11
     * specialprice : $88.00
     * status : open
     */
    private List<ImageEntity> images;
    private int spied_count;
    private String end_time;
    private int remaining_stock;
    private int total_stock;
    private boolean isSpied;
    private int view_count;
    private String start_time;
    private String remaining_time;
    private List<ProductSizesEntity> sizes;
    private String original_price;
    private String low_price;
    private String high_price;
    private String current_price;
    private String display_msg;
    private String name;
    private String id;
    private String specialprice;
    private String status;

    public int getView_count() {
        return view_count;
    }

    public void setView_count(int view_count) {
        this.view_count = view_count;
    }

    public String getCurrent_price() {
        return current_price;
    }

    public void setCurrent_price(String current_price) {
        this.current_price = current_price;
    }

    public String getOriginal_price() {
        return original_price;
    }

    public void setOriginal_price(String original_price) {
        this.original_price = original_price;
    }

    public String getLow_price() {
        return low_price;
    }

    public void setLow_price(String low_price) {
        this.low_price = low_price;
    }

    public String getHigh_price() {
        return high_price;
    }

    public void setHigh_price(String high_price) {
        this.high_price = high_price;
    }

    public void setImage(List<ImageEntity> images) {
        this.images = images;
    }

    public void setSpied_count(int spied_count) {
        this.spied_count = spied_count;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public void setRemaining_stock(int remaining_stock) {
        this.remaining_stock = remaining_stock;
    }

    public void setTotal_stock(int total_stock) {
        this.total_stock = total_stock;
    }

    public void setIsSpied(boolean isSpied) {
        this.isSpied = isSpied;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public void setRemaining_time(String remaining_time) {
        this.remaining_time = remaining_time;
    }

    public void setSizes(List<ProductSizesEntity> sizes) {
        this.sizes = sizes;
    }

    public void setDisplay_msg(String display_msg) {
        this.display_msg = display_msg;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setSpecialprice(String specialprice) {
        this.specialprice = specialprice;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ImageEntity> getImage() {
        return images;
    }

    public int getSpied_count() {
        return spied_count;
    }

    public String getEnd_time() {
        return end_time;
    }

    public int getRemaining_stock() {
        return remaining_stock;
    }

    public int getTotal_stock() {
        return total_stock;
    }

    public boolean getIsSpied() {
        return isSpied;
    }

    public String getStart_time() {
        return start_time;
    }

    public String getRemaining_time() {
        return remaining_time;
    }

    public List<ProductSizesEntity> getSizes() {
        return sizes;
    }

    public String getDisplay_msg() {
        return display_msg;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getSpecialprice() {
        return specialprice;
    }

    public String getStatus() {
        return status;
    }
}
