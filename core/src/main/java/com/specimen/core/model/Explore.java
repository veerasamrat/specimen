package com.specimen.core.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Explore extends Category {
    protected Explore(Parcel in) {
        super(in);
    }
    public static final Parcelable.Creator<Explore> CREATOR = new Parcelable.Creator<Explore>() {
        @Override
        public Explore createFromParcel(Parcel in) {
            return new Explore(in);
        }

        @Override
        public Explore[] newArray(int size) {
            return new Explore[size];
        }
    };
}