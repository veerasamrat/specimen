package com.specimen.core.model;

import java.util.List;

public class Filter {
    /**
     * attr_code : categoryId
     * attr_value : ["10"]
     */
    private String attr_code;
    private List<String> attr_value;

    public String getAttr_code() {
        return attr_code;
    }

    public void setAttr_code(String attr_code) {
        this.attr_code = attr_code;
    }

    public List<String> getAttr_value() {
        return attr_value;
    }

    public void setAttr_value(List<String> attr_value) {
        this.attr_value = attr_value;
    }

    @Override
    public String toString() {
        return "Filter{" +
                "attr_code='" + attr_code + '\'' +
                ", attr_value=" + attr_value +
                '}';
    }
}