package com.specimen.core.model;

import com.google.gson.annotations.Expose;

public class FilterAttr {

    @Expose
    private String id;
    @Expose
    private String value;

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value The value
     */
    public void setValue(String value) {
        this.value = value;
    }

}