package com.specimen.core.model;

import java.util.List;

public class FilterType {

    private String code;
    private String label;
    private String id;
    private List<FilterTypeOption> options;

    public List<FilterTypeOption> getOptions() {
        return options;
    }

    public void setOptions(List<FilterTypeOption> options) {
        this.options = options;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
