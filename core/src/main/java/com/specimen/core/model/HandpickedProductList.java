package com.specimen.core.model;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

public class HandpickedProductList {

    @Expose
    private List<HandpickedProduct> products = new ArrayList<>();

    /**
     * @return The products
     */
    public List<HandpickedProduct> getProducts() {
        return products;
    }

    /**
     * @param products The products
     */
    public void setProducts(List<HandpickedProduct> products) {
        this.products = products;
    }

}