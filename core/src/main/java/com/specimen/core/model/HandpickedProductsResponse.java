package com.specimen.core.model;

import com.google.gson.annotations.Expose;
import com.specimen.core.response.APIResponse;

public class HandpickedProductsResponse extends APIResponse {

    @Expose
    private HandpickedProductList data;
//    @Expose
//    private String status;
//    @Expose
//    private String message;

    /**
     * @return The data
     */
    public HandpickedProductList getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(HandpickedProductList data) {
        this.data = data;
    }

//    /**
//     * @return The status
//     */
//    public String getStatus() {
//        return status;
//    }
//
//    /**
//     * @param status The status
//     */
//    public void setStatus(String status) {
//        this.status = status;
//    }
//
//    /**
//     * @return The message
//     */
//    public String getMessage() {
//        return message;
//    }
//
//    /**
//     * @param message The message
//     */
//    public void setMessage(String message) {
//        this.message = message;
//    }

}