package com.specimen.core.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ImageEntity implements Parcelable {
    /**
     * small : {"sizeName":"small","actualWidth":31,"width":32,"actualHeight":40,"url":"http://resources.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_small.jpg","height":40}
     * xLarge : {"sizeName":"xLarge","actualWidth":264,"width":328,"actualHeight":344,"url":"http://resources.shopstyle.com/xim/36/30/3630ea5625adac7abc61f61e8cf7b926.jpg","height":410}
     * Medium : {"sizeName":"Medium","actualWidth":107,"width":112,"actualHeight":140,"url":"http://resources.shopstyle.com/sim/36/30/3630ea5625adac7abc61f61e8cf7b926_medium/burberry-infant-s-cashmere-dress.jpg","height":140}
     * Large : {"sizeName":"Large","actualWidth":157,"width":164,"actualHeight":205,"url":"http://resources.shopstyle.com/sim/36/30/3630ea5625adac7abc61f61e8cf7b926/burberry-infant-s-cashmere-dress.jpg","height":205}
     * IPhoneSmall : {"sizeName":"IPhoneSmall","actualWidth":96,"width":100,"actualHeight":125,"url":"http://resources.shopstyle.com/mim/36/30/3630ea5625adac7abc61f61e8cf7b926_small.jpg","height":125}
     * Best : {"sizeName":"Best","actualWidth":264,"width":720,"actualHeight":344,"url":"http://bim.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_best.jpg","height":900}
     * Original : {"sizeName":"Original","actualWidth":264,"actualHeight":344,"url":"http://bim.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_best.jpg"}
     * IPhone : {"sizeName":"IPhone","actualWidth":264,"width":288,"actualHeight":344,"url":"http://resources.shopstyle.com/mim/36/30/3630ea5625adac7abc61f61e8cf7b926.jpg","height":360}
     */
    private ImageSize small;
    private ImageSize xLarge;
    private ImageSize xxLarge;
    private ImageSize xxxLarge;
    private ImageSize Medium;
    private ImageSize Large;
    private ImageSize IPhoneSmall;
    private ImageSize Best;
    private ImageSize Original;
    private ImageSize IPhone;

    public ImageSize getSmall() {
        return small;
    }

    public void setSmall(ImageSize Small) {
        this.small = Small;
    }

    public ImageSize getxLarge() {
        return xLarge;
    }

    public void setxLarge(ImageSize xLarge) {
        this.xLarge = xLarge;
    }

    public ImageSize getMedium() {
        return Medium;
    }

    public void setMedium(ImageSize Medium) {
        this.Medium = Medium;
    }

    public ImageSize getLarge() {
        return Large;
    }

    public void setLarge(ImageSize Large) {
        this.Large = Large;
    }

    public ImageSize getIPhoneSmall() {
        return IPhoneSmall;
    }

    public void setIPhoneSmall(ImageSize IPhoneSmall) {
        this.IPhoneSmall = IPhoneSmall;
    }

    public ImageSize getBest() {
        return Best;
    }

    public void setBest(ImageSize Best) {
        this.Best = Best;
    }

    public ImageSize getOriginal() {
        return Original;
    }

    public void setOriginal(ImageSize Original) {
        this.Original = Original;
    }

    public ImageSize getIPhone() {
        return IPhone;
    }

    public void setIPhone(ImageSize IPhone) {
        this.IPhone = IPhone;
    }





    protected ImageEntity(Parcel in) {
        small = (ImageSize) in.readValue(ImageSize.class.getClassLoader());
        xLarge = (ImageSize) in.readValue(ImageSize.class.getClassLoader());
        xxLarge = (ImageSize) in.readValue(ImageSize.class.getClassLoader());
        xxxLarge = (ImageSize) in.readValue(ImageSize.class.getClassLoader());
        Medium = (ImageSize) in.readValue(ImageSize.class.getClassLoader());
        Large = (ImageSize) in.readValue(ImageSize.class.getClassLoader());
        IPhoneSmall = (ImageSize) in.readValue(ImageSize.class.getClassLoader());
        Best = (ImageSize) in.readValue(ImageSize.class.getClassLoader());
        Original = (ImageSize) in.readValue(ImageSize.class.getClassLoader());
        IPhone = (ImageSize) in.readValue(ImageSize.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(small);
        dest.writeValue(xLarge);
        dest.writeValue(xxLarge);
        dest.writeValue(xxxLarge);
        dest.writeValue(Medium);
        dest.writeValue(Large);
        dest.writeValue(IPhoneSmall);
        dest.writeValue(Best);
        dest.writeValue(Original);
        dest.writeValue(IPhone);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ImageEntity> CREATOR = new Parcelable.Creator<ImageEntity>() {
        @Override
        public ImageEntity createFromParcel(Parcel in) {
            return new ImageEntity(in);
        }

        @Override
        public ImageEntity[] newArray(int size) {
            return new ImageEntity[size];
        }
    };

    public ImageSize getXxLarge() {
        return xxLarge;
    }

    public void setXxLarge(ImageSize xxLarge) {
        this.xxLarge = xxLarge;
    }

    public ImageSize getXxxLarge() {
        return xxxLarge;
    }

    public void setXxxLarge(ImageSize xxxLarge) {
        this.xxxLarge = xxxLarge;
    }
}