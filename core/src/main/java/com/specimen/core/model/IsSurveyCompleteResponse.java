package com.specimen.core.model;

import com.specimen.core.response.APIResponse;

/**
 * Created by Swapnil on 8/3/2015.
 */
public class IsSurveyCompleteResponse extends APIResponse {
    /**
     * data : null
     * is_survey_completed : true
     * status : Success
     */

    private Object data;
    private String is_survey_completed;



    private String isHandpickedAvailable;

    public void setData(Object data) {
        this.data = data;
    }

    public void setIs_survey_completed(String is_survey_completed) {
        this.is_survey_completed = is_survey_completed;
    }

    public Object getData() {
        return data;
    }

    public String getIs_survey_completed() {
        return is_survey_completed;
    }


    public String getIsHandpickedAvailable() {
        return isHandpickedAvailable;
    }

    public void setIsHandpickedAvailable(String isHandpickedAvailable) {
        this.isHandpickedAvailable = isHandpickedAvailable;
    }

}
