package com.specimen.core.model;

import com.specimen.core.response.APIResponse;

public class LikeNotificationResponse extends APIResponse {

    private TrusteeLikesData data;

    /**
     * @return The data
     */
    public TrusteeLikesData getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(TrusteeLikesData data) {
        this.data = data;
    }


}