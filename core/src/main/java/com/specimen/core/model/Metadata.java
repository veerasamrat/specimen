package com.specimen.core.model;

import java.util.List;

public class Metadata {

    private List<Filter> filter;
    private int total;
    private int limit;
    private int page;

    public List<Filter> getFilter() {
        return filter;
    }

    public void setFilter(List<Filter> filter) {
        this.filter = filter;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public PageRequest getNextPageRequest() {
        if ((page * limit) + limit >= total) {
            return null;
        } else {
            return new PageRequest().setPage(page * limit).setLimit(limit);
        }
    }
}