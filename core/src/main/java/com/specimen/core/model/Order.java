package com.specimen.core.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Swapnil on 8/20/2015.
 */
public class Order {
    private String orderId;
    private String orderDate;
    private String orderStatus;
    private String orderTotalValue;
    private List<OrderProduct> orderItems = new ArrayList<>();

    /**
     *
     * @return
     * The orderId
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     *
     * @param orderId
     * The orderId
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    /**
     *
     * @return
     * The orderDate
     */
    public String getOrderDate() {
        return orderDate;
    }

    /**
     *
     * @param orderDate
     * The orderDate
     */
    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    /**
     *
     * @return
     * The orderStatus
     */
    public String getOrderStatus() {
        return orderStatus;
    }

    /**
     *
     * @param orderStatus
     * The orderStatus
     */
    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    /**
     *
     * @return
     * The orderTotalValue
     */
    public String getOrderTotalValue() {
        return orderTotalValue;
    }

    /**
     *
     * @param orderTotalValue
     * The orderTotalValue
     */
    public void setOrderTotalValue(String orderTotalValue) {
        this.orderTotalValue = orderTotalValue;
    }

    /**
     *
     * @return
     * The orderItems
     */
    public List<OrderProduct> getOrderItems() {
        return orderItems;
    }



}
