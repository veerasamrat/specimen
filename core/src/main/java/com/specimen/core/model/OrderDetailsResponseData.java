package com.specimen.core.model;

import java.util.List;

public class OrderDetailsResponseData {
    /**
     * totalTax :
     * address :
     * orderId :
     * paidBy :
     * orderStatus :
     * orderTotalValue :
     * paymentDetails :
     * orderDate :
     * estimatedDeliveryDate :
     * orderItems : [{"isSpied":"true","image":{"Small":{"sizeName":"Small","actualWidth":31,"width":32,"actualHeight":40,"url":"http://resources.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_small.jpg","height":40},"XLarge":{"sizeName":"XLarge","actualWidth":264,"width":328,"actualHeight":344,"url":"http://resources.shopstyle.com/xim/36/30/3630ea5625adac7abc61f61e8cf7b926.jpg","height":410},"Medium":{"sizeName":"Medium","actualWidth":107,"width":112,"actualHeight":140,"url":"http://resources.shopstyle.com/sim/36/30/3630ea5625adac7abc61f61e8cf7b926_medium/burberry-infant-s-cashmere-dress.jpg","height":140},"Large":{"sizeName":"Large","actualWidth":157,"width":164,"actualHeight":205,"url":"http://resources.shopstyle.com/sim/36/30/3630ea5625adac7abc61f61e8cf7b926/burberry-infant-s-cashmere-dress.jpg","height":205},"IPhoneSmall":{"sizeName":"IPhoneSmall","actualWidth":96,"width":100,"actualHeight":125,"url":"http://resources.shopstyle.com/mim/36/30/3630ea5625adac7abc61f61e8cf7b926_small.jpg","height":125},"Best":{"sizeName":"Best","actualWidth":264,"width":720,"actualHeight":344,"url":"http://bim.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_best.jpg","height":900},"Original":{"sizeName":"Original","actualWidth":264,"actualHeight":344,"url":"http://bim.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_best.jpg"},"IPhone":{"sizeName":"IPhone","actualWidth":264,"width":288,"actualHeight":344,"url":"http://resources.shopstyle.com/mim/36/30/3630ea5625adac7abc61f61e8cf7b926.jpg","height":360}},"spied_count":222,"quantity":1,"color":"red","price":"USD 88.00","name":"Fernbuckles washed-chinos","attribute":[{"lable":"Size","value":"L"},{"lable":"Lenght","value":"42"}],"specialprice":"$88.00"}]
     */
    private String totalTax;
    private String totalDiscount;
    private String address;
    private String orderId;
    private String paidBy;



    private String orderNo;

    private String orderStatus;
    private String orderTotalValue;
    private String paymentDetails;
    private String orderDate;
    private String estimatedDeliveryDate;
    private List<Product> orderItems;

    public String getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(String totalTax) {
        this.totalTax = totalTax;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPaidBy() {
        return paidBy;
    }

    public void setPaidBy(String paidBy) {
        this.paidBy = paidBy;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderTotalValue() {
        return orderTotalValue;
    }

    public void setOrderTotalValue(String orderTotalValue) {
        this.orderTotalValue = orderTotalValue;
    }

    public String getPaymentDetails() {
        return paymentDetails;
    }

    public void setPaymentDetails(String paymentDetails) {
        this.paymentDetails = paymentDetails;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getEstimatedDeliveryDate() {
        return estimatedDeliveryDate;
    }

    public void setEstimatedDeliveryDate(String estimatedDeliveryDate) {
        this.estimatedDeliveryDate = estimatedDeliveryDate;
    }

    public List<Product> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<Product> orderItems) {
        this.orderItems = orderItems;
    }

    public String getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(String totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
}