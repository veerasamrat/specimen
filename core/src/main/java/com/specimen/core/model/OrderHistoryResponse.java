package com.specimen.core.model;

import com.specimen.core.response.APIResponse;

/**
 * Created by Swapnil on 8/20/2015.
 */
public class OrderHistoryResponse extends APIResponse {

    private OrderHistoryResponseData data;


    /**
     * @return The data
     */
    public OrderHistoryResponseData getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(OrderHistoryResponseData data) {
        this.data = data;
    }


}
