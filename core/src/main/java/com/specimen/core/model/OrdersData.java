package com.specimen.core.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Swapnil on 8/20/2015.
 */
public class OrdersData {

    private List<OnGoingOrder> onGoingOrders = new ArrayList<>();
    private List<ClosedOrder> closedOrders = new ArrayList<>();
    private List<ReturnOrder> returnOrders = new ArrayList<>();

    /**
     * @return The onGoingOrders
     */
    public List<OnGoingOrder> getOnGoingOrders() {
        return onGoingOrders;
    }

    /**
     * @param onGoingOrders The onGoingOrders
     */
    public void setOnGoingOrders(List<OnGoingOrder> onGoingOrders) {
        this.onGoingOrders = onGoingOrders;
    }

    /**
     * @return The closedOrders
     */
    public List<ClosedOrder> getClosedOrders() {
        return closedOrders;
    }

    /**
     * @param closedOrders The closedOrders
     */
    public void setClosedOrders(List<ClosedOrder> closedOrders) {
        this.closedOrders = closedOrders;
    }

    /**
     * @return The returnOrders
     */
    public List<ReturnOrder> getReturnOrders() {
        return returnOrders;
    }

    /**
     * @param returnOrders The returnOrders
     */
    public void setReturnOrders(List<ReturnOrder> returnOrders) {
        this.returnOrders = returnOrders;
    }


}
