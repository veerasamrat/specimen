package com.specimen.core.model;

import java.util.Map;

/**
 * Created by Intelliswift on 7/9/2015.
 */
public class PageRequest {

    private int page, total, limit;

    public PageRequest() {
    }

    public int getTotal() {
        return total;
    }

    public PageRequest setTotal(int total) {
        this.total = total;
        return this;
    }

    public int getPage() {
        return page;
    }

    public PageRequest setPage(int page) {
        this.page = page;
        return this;

    }

    public int getLimit() {
        return limit;
    }

    public PageRequest setLimit(int limit) {
        this.limit = limit;
        return this;
    }


    public void addParameters(Map<String, Object> parameters) {

        parameters.put("page", String.valueOf(page));

        parameters.put("limit", String.valueOf(limit));

    }

}
