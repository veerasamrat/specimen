package com.specimen.core.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Intelliswift on 6/18/2015.
 */
public class ProductDetails {

    @Expose
    private String id;
    @Expose
    private String sku;
    @Expose
    private String name;
    @Expose
    private String qty;
    @Expose
    private String price;
    @Expose
    private String description;
    @Expose
    private List<String> images = new ArrayList<>();
    @SerializedName("associate_products")
    @Expose
    private List<AssociateProduct> associateProducts = new ArrayList<>();
    /**
     * need to expose
     */
//    @SerializedName("attribute_values")
//    @Expose
//    private AttributeValues attributeValues;
    @SerializedName("reviews_data")
    @Expose
    private ReviewsData reviewsData;
    @SerializedName("related_product_datas")
    @Expose
    private RelatedProductDatas relatedProductDatas;

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The sku
     */
    public String getSku() {
        return sku;
    }

    /**
     * @param sku The sku
     */
    public void setSku(String sku) {
        this.sku = sku;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The qty
     */
    public String getQty() {
        return qty;
    }

    /**
     * @param qty The qty
     */
    public void setQty(String qty) {
        this.qty = qty;
    }

    /**
     * @return The price
     */
    public String getPrice() {
        return price;
    }

    /**
     * @param price The price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The images
     */
    public List<String> getImages() {
        return images;
    }

    /**
     * @param images The images
     */
    public void setImages(List<String> images) {
        this.images = images;
    }

    /**
     * @return The associateProducts
     */
    public List<AssociateProduct> getAssociateProducts() {
        return associateProducts;
    }

    /**
     * @param associateProducts The associate_products
     */
    public void setAssociateProducts(List<AssociateProduct> associateProducts) {
        this.associateProducts = associateProducts;
    }

    /**
     * @return The attributeValues
     */
//    public AttributeValues getAttributeValues() {
//        return attributeValues;
//    }

    /**
     * @param attributeValues The attribute_values
     */
//    public void setAttributeValues(AttributeValues attributeValues) {
//        this.attributeValues = attributeValues;
//    }

    /**
     * @return The reviewsData
     */
    public ReviewsData getReviewsData() {
        return reviewsData;
    }

    /**
     * @param reviewsData The reviews_data
     */
    public void setReviewsData(ReviewsData reviewsData) {
        this.reviewsData = reviewsData;
    }

    /**
     * @return The relatedProductDatas
     */
    public RelatedProductDatas getRelatedProductDatas() {
        return relatedProductDatas;
    }

    /**
     * @param relatedProductDatas The related_product_datas
     */
    public void setRelatedProductDatas(RelatedProductDatas relatedProductDatas) {
        this.relatedProductDatas = relatedProductDatas;
    }

}
