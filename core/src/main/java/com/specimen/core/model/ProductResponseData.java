package com.specimen.core.model;

import java.util.List;

public class ProductResponseData {

        private Metadata metadata;
        private List<Product> products;

        public Metadata getMetadata() {
            return metadata;
        }

        public void setMetadata(Metadata metadata) {
            this.metadata = metadata;
        }

        public List<Product> getProducts() {
            return products;
        }

        public void setProducts(List<Product> products) {
            this.products = products;
        }
    private List<Curated> curated_way;

    public List<Curated> getCurated_format() {
        return curated_way;
    }

    public void setCurated_format(List<Curated> curated_format) {
        this.curated_way = curated_format;
    }

    }