package com.specimen.core.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Swapnil on 8/7/2015.
 */
public class ProductSizesListParable implements Parcelable {

    public List<ProductSizesEntity> mList;

    public List<ProductSizesEntity> getmList() {
        return mList;
    }

    public void setmList(List<ProductSizesEntity> mList) {
        this.mList = mList;
    }

    public ProductSizesListParable(List<ProductSizesEntity> list) {
        this.mList = list;
    }

    protected ProductSizesListParable(Parcel in) {
        if (in.readByte() == 0x01) {
            mList = new ArrayList<>();
            in.readList(mList, ProductSizesEntity.class.getClassLoader());
        } else {
            mList = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (mList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(mList);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ProductSizesListParable> CREATOR = new Parcelable.Creator<ProductSizesListParable>() {
        @Override
        public ProductSizesListParable createFromParcel(Parcel in) {
            return new ProductSizesListParable(in);
        }

        @Override
        public ProductSizesListParable[] newArray(int size) {
            return new ProductSizesListParable[size];
        }
    };
}