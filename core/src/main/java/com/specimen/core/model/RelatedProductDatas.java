package com.specimen.core.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class RelatedProductDatas {

@SerializedName("related_product_ids")
@Expose
private List<String> relatedProductIds = new ArrayList<>();

/**
* 
* @return
* The relatedProductIds
*/
public List<String> getRelatedProductIds() {
return relatedProductIds;
}

/**
* 
* @param relatedProductIds
* The related_product_ids
*/
public void setRelatedProductIds(List<String> relatedProductIds) {
this.relatedProductIds = relatedProductIds;
}

}