package com.specimen.core.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ReviewsData {

@SerializedName("review_title")
@Expose
private List<String> reviewTitle = new ArrayList<>();
@SerializedName("review_details")
@Expose
private List<Object> reviewDetails = new ArrayList<>();
@SerializedName("review_nickname")
@Expose
private List<String> reviewNickname = new ArrayList<>();
@SerializedName("review_date")
@Expose
private List<String> reviewDate = new ArrayList<>();

/**
* 
* @return
* The reviewTitle
*/
public List<String> getReviewTitle() {
return reviewTitle;
}

/**
* 
* @param reviewTitle
* The review_title
*/
public void setReviewTitle(List<String> reviewTitle) {
this.reviewTitle = reviewTitle;
}

/**
* 
* @return
* The reviewDetails
*/
public List<Object> getReviewDetails() {
return reviewDetails;
}

/**
* 
* @param reviewDetails
* The review_details
*/
public void setReviewDetails(List<Object> reviewDetails) {
this.reviewDetails = reviewDetails;
}

/**
* 
* @return
* The reviewNickname
*/
public List<String> getReviewNickname() {
return reviewNickname;
}

/**
* 
* @param reviewNickname
* The review_nickname
*/
public void setReviewNickname(List<String> reviewNickname) {
this.reviewNickname = reviewNickname;
}

/**
* 
* @return
* The reviewDate
*/
public List<String> getReviewDate() {
return reviewDate;
}

/**
* 
* @param reviewDate
* The review_date
*/
public void setReviewDate(List<String> reviewDate) {
this.reviewDate = reviewDate;
}

}