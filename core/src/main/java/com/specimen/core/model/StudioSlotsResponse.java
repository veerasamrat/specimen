package com.specimen.core.model;

import com.specimen.core.response.APIResponse;

import java.util.List;

/**
 * Created by Swapnil on 9/1/2015.
 */
public class StudioSlotsResponse extends APIResponse {


    /**
     * data : {"timeslots":[{"id":"10:00","status":"AVAILABLE"},{"id":"11:00","status":"BOOKED"},{"id":"12:00","status":"AVAILABLE"},{"id":"13:00","status":"BOOKED"},{"id":"14:00","status":"AVAILABLE"},{"id":"15:00","status":"BOOKED"}]}
     * status : Success
     * message : Timeslots fetched successfully
     */

    private DataEntity data;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public DataEntity getData() {
        return data;
    }

    public class DataEntity {
        /**
         * timeslots : [{"id":"10:00","status":"AVAILABLE"},{"id":"11:00","status":"BOOKED"},{"id":"12:00","status":"AVAILABLE"},{"id":"13:00","status":"BOOKED"},{"id":"14:00","status":"AVAILABLE"},{"id":"15:00","status":"BOOKED"}]
         */

        private List<TimeslotsEntity> timeslots;

        public void setTimeslots(List<TimeslotsEntity> timeslots) {
            this.timeslots = timeslots;
        }

        public List<TimeslotsEntity> getTimeslots() {
            return timeslots;
        }

        public class TimeslotsEntity {
            /**
             * id : 10:00
             * status : AVAILABLE
             * slot : 10-11
             */

            private String id;
            private String status;
            private String slot;

            public void setId(String id) {
                this.id = id;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getId() {
                return id;
            }

            public String getStatus() {
                return status;
            }

            public String getSlot() {
                return slot;
            }

            public void setSlot(String slot) {
                this.slot = slot;
            }
        }
    }
}
