package com.specimen.core.model;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by root on 27/8/15.
 */
public class SubmitSurveyQuery {

    /**
     * survey : [{"questionId":"weekday_look","answerId":["66"]},{"questionId":"weekend_look","answerId":["70"]},{"questionId":"style_icon","answerId":["83"]},{"questionId":"buddy","answerId":["62"]},{"questionId":"shirt_size","answerId":["74"]},{"questionId":"shirt_fit","answerId":["87"]},{"questionId":"pant_size","answerId":["89"]},{"questionId":"pant_fit","answerId":["81"]},{"questionId":"color","answerId":["12","13"]}]
     */

    private List<SurveyEntity> surveyEntityList;

    public void setSurvey(List<SurveyEntity> survey) {
        this.surveyEntityList = survey;
    }

    public List<SurveyEntity> getSurvey() {
        return surveyEntityList;
    }

    public class SurveyEntity {
        /**
         * questionId : weekday_look
         * answerId : ["66"]
         */

        private String questionId;
        private List<String> answerId;

        public void setQuestionId(String questionId) {
            this.questionId = questionId;
        }

        public void setAnswerId(List<String> answerId) {
            this.answerId = answerId;
        }

        public String getQuestionId() {
            return questionId;
        }

        public List<String> getAnswerId() {
            return answerId;
        }
    }

    public void convertMapInList (Map<String, List<String>> surveyAnswersMap) {
        surveyEntityList = new ArrayList<>();
        for (Map.Entry<String, List<String>> entry : surveyAnswersMap.entrySet()){
            System.out.println(entry.getKey() + "/" + entry.getValue());
            SurveyEntity surveyEntity = new SurveyEntity();
            surveyEntity.questionId = entry.getKey().toString();

            List<String> answerList = entry.getValue();
            surveyEntity.answerId = new ArrayList<>(Arrays.asList(answerList.get(0).split("[\\s,]+")));

            surveyEntityList.add(surveyEntity);
        }
    }

}
