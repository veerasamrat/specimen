package com.specimen.core.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

public class SuggestionEntity implements Parcelable {


    /**
     * parent_id : 5
     * sug_id : 12
     * string : Formal IN Tops
     * type : category
     */

    int parent_id;
    String sug_id;


    String string;
    String type;

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    public void setSug_id(String sug_id) {
        this.sug_id = sug_id;
    }

    public void setString(String string) {
        this.string = string;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getParent_id() {
        return parent_id;
    }

    public String getSug_id() {
        return sug_id;
    }

    public String getString() {
        return string;
    }

    public String getType() {
        return type;
    }

//    @Override
//    public void save() {
//        super.save();
//    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.parent_id);
        dest.writeString(this.sug_id);
        dest.writeString(this.string);
        dest.writeString(this.type);
    }

    public SuggestionEntity() {
    }

    protected SuggestionEntity(Parcel in) {
        this.parent_id = in.readInt();
        this.sug_id = in.readString();
        this.string = in.readString();
        this.type = in.readString();
    }

    public static final Parcelable.Creator<SuggestionEntity> CREATOR = new Parcelable.Creator<SuggestionEntity>() {
        public SuggestionEntity createFromParcel(Parcel source) {
            return new SuggestionEntity(source);
        }

        public SuggestionEntity[] newArray(int size) {
            return new SuggestionEntity[size];
        }
    };
}