package com.specimen.core.model;

import java.util.List;

public class SurveyResponseData {

//    private String name;
//    private String title;
//    private String desc;
    private List<Questions> questions;
//    private ImageEntity image;

//    public String getDesc() {
//        return desc;
//    }
//
//    public void setDesc(String desc) {
//        this.desc = desc;
//    }

//    public String getTitle() {
//        return title;
//    }
//
//    public void setTitle(String title) {
//        this.title = title;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }

    public void setQuestions(List<Questions> questions) {
        this.questions = questions;
    }

    public List<Questions> getQuestions() {
        return questions;
    }

//    public void setImage(ImageEntity image) {
//        this.image = image;
//    }
//
//    public ImageEntity getImage() {
//        return image;
//    }

}