package com.specimen.core.model;

import com.specimen.core.response.APIResponse;

/**
 * Created by root on 29/8/15.
 */
public class SurveyStyleProfileResponseData extends APIResponse {


    /**
     * data : {"name":"Classic","title":"You\u2019re a CLASSIC guy!","desc":"You go for the crisp white shirt and blue jeans or that sharp black suit because well, you love it! It looks great, you can never go wrong with it, and let\u2019s face it: You are not inclined to spending time to put together a \u201cdifferent\u201d look. You know what you can carry off, and you enjoy sticking to the basics.","image":{"Small":{"sizeName":"Small","url":"http://resources.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_small.jpg","width":32,"height":40,"actualWidth":31,"actualHeight":40},"XLarge":{"sizeName":"XLarge","url":"http://resources.shopstyle.com/xim/36/30/3630ea5625adac7abc61f61e8cf7b926.jpg","width":328,"height":410,"actualWidth":264,"actualHeight":344},"Medium":{"sizeName":"Medium","url":"http://resources.shopstyle.com/sim/36/30/3630ea5625adac7abc61f61e8cf7b926_medium/burberry-infant-s-cashmere-dress.jpg","width":112,"height":140,"actualWidth":107,"actualHeight":140},"Large":{"sizeName":"Large","url":"http://resources.shopstyle.com/sim/36/30/3630ea5625adac7abc61f61e8cf7b926/burberry-infant-s-cashmere-dress.jpg","width":164,"height":205,"actualWidth":157,"actualHeight":205},"IPhoneSmall":{"sizeName":"IPhoneSmall","url":"http: //resources.shopstyle.com/mim/36/30/3630ea5625adac7abc61f61e8cf7b926_small.jpg","width":100,"height":125,"actualWidth":96,"actualHeight":125},"Best":{"sizeName":"Best","url":"http://bim.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_best.jpg","width":720,"height":900,"actualWidth":264,"actualHeight":344},"Original":{"sizeName":"Original","url":"http://bim.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_best.jpg","actualWidth":264,"actualHeight":344},"IPhone":{"sizeName":"IPhone","url":"http://resources.shopstyle.com/mim/36/30/3630ea5625adac7abc61f61e8cf7b926.jpg","width":288,"height":360,"actualWidth":264,"actualHeight":344}},"status":"Success","message":"Style Profile Available"}
     */

    private DataEntity data;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public DataEntity getData() {
        return data;
    }

    public class DataEntity {
        /**
         * name : Classic
         * title : You’re a CLASSIC guy!
         * desc : You go for the crisp white shirt and blue jeans or that sharp black suit because well, you love it! It looks great, you can never go wrong with it, and let’s face it: You are not inclined to spending time to put together a “different” look. You know what you can carry off, and you enjoy sticking to the basics.
         * image : {"Small":{"sizeName":"Small","url":"http://resources.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_small.jpg","width":32,"height":40,"actualWidth":31,"actualHeight":40},"XLarge":{"sizeName":"XLarge","url":"http://resources.shopstyle.com/xim/36/30/3630ea5625adac7abc61f61e8cf7b926.jpg","width":328,"height":410,"actualWidth":264,"actualHeight":344},"Medium":{"sizeName":"Medium","url":"http://resources.shopstyle.com/sim/36/30/3630ea5625adac7abc61f61e8cf7b926_medium/burberry-infant-s-cashmere-dress.jpg","width":112,"height":140,"actualWidth":107,"actualHeight":140},"Large":{"sizeName":"Large","url":"http://resources.shopstyle.com/sim/36/30/3630ea5625adac7abc61f61e8cf7b926/burberry-infant-s-cashmere-dress.jpg","width":164,"height":205,"actualWidth":157,"actualHeight":205},"IPhoneSmall":{"sizeName":"IPhoneSmall","url":"http: //resources.shopstyle.com/mim/36/30/3630ea5625adac7abc61f61e8cf7b926_small.jpg","width":100,"height":125,"actualWidth":96,"actualHeight":125},"Best":{"sizeName":"Best","url":"http://bim.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_best.jpg","width":720,"height":900,"actualWidth":264,"actualHeight":344},"Original":{"sizeName":"Original","url":"http://bim.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_best.jpg","actualWidth":264,"actualHeight":344},"IPhone":{"sizeName":"IPhone","url":"http://resources.shopstyle.com/mim/36/30/3630ea5625adac7abc61f61e8cf7b926.jpg","width":288,"height":360,"actualWidth":264,"actualHeight":344}}
         * status : Success
         * message : Style Profile Available
         */

        private String name;
        private String title;
        private String desc;
//        private ImageEntity image;

        private String image;

        public void setName(String name) {
            this.name = name;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getName() {
            return name;
        }

        public String getTitle() {
            return title;
        }

        public String getDesc() {
            return desc;
        }

        public String getImage() {
            return image;
        }

    }
}