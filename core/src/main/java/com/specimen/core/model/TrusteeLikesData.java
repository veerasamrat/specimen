package com.specimen.core.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TrusteeLikesData {

    private List<Like> likes = new ArrayList<Like>();
    private Integer count;
    private String like_count;
    private Product product;

    /**
     * @return The likes
     */
    public List<Like> getLikes() {
        return likes;
    }

    /**
     * @param likes The likes
     */
    public void setLikes(List<Like> likes) {
        this.likes = likes;
    }

    /**
     * @return The count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * @param count The count
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     * @return The likeCount
     */
    public String getLikeCount() {
        return like_count;
    }

    /**
     * @param likeCount The like_count
     */
    public void setLikeCount(String likeCount) {
        this.like_count = likeCount;
    }

    /**
     * @return The product
     */
    public Product getProduct() {
        return product;
    }

    /**
     * @param product The product
     */
    public void setProduct(Product product) {
        this.product = product;
    }

}