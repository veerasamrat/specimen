package com.specimen.core.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.specimen.core.response.APIResponse;


public class User {

    @Expose
    private String id;

    @Expose
    private String email;

    @Expose
    private String customer_fname;

   /* @SerializedName("customer_lname")
    @Expose
    private String customerLname;*/

    @Expose
    private Object cust_profileimg;

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The customerFname
     */
    public String getCustomerFname() {
        return customer_fname;
    }

    /**
     *
     * @param customerFname
     * The customer_fname
     */
    public void setCustomerFname(String customerFname) {
        this.customer_fname = customerFname;
    }

    /**
     *
     * @return
     * The custProfileimg
     */
    public Object getCustProfileimg() {
        return cust_profileimg;
    }

    /**
     *
     * @param custProfileimg
     * The cust_profileimg
     */
    public void setCustProfileimg(Object custProfileimg) {
        this.cust_profileimg = custProfileimg;
    }

}
