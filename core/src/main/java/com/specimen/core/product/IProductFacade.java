package com.specimen.core.product;

import com.specimen.core.IBaseFacade;
import com.specimen.core.model.PageRequest;
import com.specimen.core.model.Query;

/**
 * Created by Intelliswift on 7/9/2015.
 */
public interface IProductFacade extends IBaseFacade {

    void getProduct(PageRequest pageRequest);

    void getProduct(PageRequest pageRequest,String subcategoryId,String tag);

    void getProduct(PageRequest pageRequest,Query categoryId,String tag);

    void searchProducts(PageRequest pageRequest);

    void getProductDetail(String productID);

    void resetProductCache();

    void productsharecount(String productID);
}
