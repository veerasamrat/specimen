package com.specimen.core.productdetails;

import com.specimen.core.BaseFacade;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.request.ProductDetailsRequestBuilder;
import com.specimen.core.response.ProductDetailsResponse;

import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by root on 6/8/15.
 */
public class ProductDetailsFacade extends BaseFacade implements IProductDetailsFacade {

    private ProductDetailsRequestBuilder productDetailsRequestBuilder;

    public ProductDetailsFacade(IResponseSubscribe responseSubscribe, IApplicationFacade applicationFacade) {
        super(responseSubscribe, applicationFacade);
        productDetailsRequestBuilder = new ProductDetailsRequestBuilder();
    }

    @Override
    public void getProductDetailsData(String productId) {
        HashMap<String, Object> map = getRequestBody("getProductDetails");
        map.put("product_id", productId);

        productDetailsRequestBuilder.getService().getProductDetails(map, new Callback<ProductDetailsResponse>() {
            @Override
            public void success(ProductDetailsResponse productDetailsResponse, Response response) {
                mResponseSubscribe.onSuccess(productDetailsResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }
}

