package com.specimen.core.request;

import com.specimen.core.model.Address;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.AddressResponse;
import com.specimen.core.response.LocalityResponse;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by Intelliswift on 6/17/2015.
 */
public class AddressRequestBuilder extends
        RetroRequestBuilder {

    public AddressService getService() {

        return super.build().create(AddressService.class);
    }

    public interface AddressService {

        //        @POST("/getStoredAddress")
        @POST("/")
        void getStoredAddress(
                @Body HashMap<String, Object> bodyParameter,
                Callback<AddressResponse> callback);

        //        @POST("/addNewAddress")
        @POST("/")
        void addNewAddress(
                @Body HashMap<String, Object> bodyParameter,
//                Callback<Address> callback);
                Callback<AddressResponse> callback);

        //        @POST("/deleteStoredAddress")
        @POST("/")
        void deleteStoreAddress(
                @Body HashMap<String, Object> bodyParameter,
                Callback<AddressResponse> callback);

        //        @POST("/changeStoredAddress")
        @POST("/")
        void changeStoreAddress(
                @Body HashMap<String, Object> bodyParameter,
                Callback<AddressResponse> callback);

        //                @POST("/changePrimaryAddress")
        @POST("/")
        void changePrimaryAddress(
                @Body HashMap<String, Object> bodyParameter,
                Callback<AddressResponse> callback);

        @POST("/")
        void checkavailableZipcodes(@Body HashMap<String, Object> bodyParameter,
                                    Callback<APIResponse> callback);

        @POST("/")
        void zipcodeAutoArea(@Body HashMap<String, Object> bodyParameter,
                             Callback<LocalityResponse> callback);

    }

}
