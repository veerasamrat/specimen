package com.specimen.core.request;

import com.specimen.core.response.AskStylistResponse;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by root on 5/9/15.
 */
public class AskStylistRequestBuilder extends RetroRequestBuilder {

    public AskStylistService getService() {

        return super.build().create(AskStylistService.class);
    }

    public interface AskStylistService {

        //        @POST("/askastylistOccasion")
        @POST("/")
        void askastylistOccasion(
                @Body HashMap<String, Object> bodyParameter,
                Callback<AskStylistResponse> callback);

        //        @POST("/askastylist")
        @POST("/")
        void askastylist(
                @Body HashMap<String, Object> bodyParameter,
                Callback<AskStylistResponse> callback);

    }
}
