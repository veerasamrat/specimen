package com.specimen.core.request;

import com.specimen.core.response.BulletinResponseData;
import com.specimen.core.response.CartDetailsResponse;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by root on 20/10/15.
 */
public class BulletinRequestBuilder extends RetroRequestBuilder {

    public BulletinService getService() {

        return super.build().create(BulletinService.class);
    }

    public interface BulletinService {
        @POST("/")
        void bulletin(@Body HashMap<String, Object> bodyParameter,
                     Callback<BulletinResponseData> callback);

    }
}

