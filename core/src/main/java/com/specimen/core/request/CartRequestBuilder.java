package com.specimen.core.request;

import com.specimen.core.response.CartDetailsResponse;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by Intelliswift on 6/18/2015.
 */
public class CartRequestBuilder extends RetroRequestBuilder {

    public CartService getService() {

        return super.build().create(CartService.class);
    }

    public interface CartService {
//        @POST("/getCart")
        @POST("/")
        void getCart(@Body HashMap<String, Object> bodyParameter,
                     Callback<CartDetailsResponse> callback);

//        @POST("/savetoCart")
        @POST("/")
        void addToCart(@Body HashMap<String, Object> bodyParameter,
                       Callback<CartDetailsResponse> callback);

//        @POST("/deleteFromCart")
@POST("/")
        void deleteFromCart(@Body HashMap<String, Object> bodyParameter,
                            Callback<CartDetailsResponse> callback);


//        @POST("/quantityUpdate")
        @POST("/")
        void getQuantityUpdate(@Body HashMap<String, Object> bodyParameter,
                               Callback<CartDetailsResponse> callback);

//        @POST("/applyPromoCode")
        @POST("/")
        void applyPromoCode(@Body HashMap<String, Object> bodyParameter,

                            Callback<CartDetailsResponse> callback);

        void getCartStock(HashMap<String, Object> bodyParameter, Callback<CartDetailsResponse> callback);
    }
}
