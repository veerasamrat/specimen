package com.specimen.core.request;

import com.specimen.core.response.DealResponse;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by root on 19/8/15.
 */
public class DealRequestBuilder extends RetroRequestBuilder {

    public ProductDetailsService getService() {

        return super.build().create(ProductDetailsService.class);
    }

    public interface ProductDetailsService {
//        @POST("/deal")
        @POST("/")
        void deal(@Body HashMap<String, Object> bodyParameter,
                               Callback<DealResponse> callback);

    }
}