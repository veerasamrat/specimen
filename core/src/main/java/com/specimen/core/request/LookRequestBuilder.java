package com.specimen.core.request;

import com.specimen.core.response.LookDetailsResponse;
import com.specimen.core.response.LookResponse;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by Intelliswift on 7/20/2015.
 */
public class LookRequestBuilder extends RetroRequestBuilder {

    public LookService getService() {

        return super.build().create(LookService.class);
    }

    public interface LookService {
        @POST("/")
//        @POST("/getLooks")
        void getLooks(@Body HashMap<String, Object> bodyParameter,
                      Callback<LookResponse> callback);

        @POST("/")
//        @POST("/looksDetails")
        void getLookDetails(@Body HashMap<String, Object> bodyParameter,
                            Callback<LookDetailsResponse> callback);

    }
}
