//package com.specimen.core.request;
//
//import com.specimen.core.model.OrderHistoryResponse;
//
//import java.util.HashMap;
//
//import retrofit.Callback;
//import retrofit.http.Body;
//import retrofit.http.POST;
//
///**
// * Created by Swapnil on 8/20/2015.
// */
//public class MyOrdersRequestBuilder extends RetroRequestBuilder {
//
//    public MyOrdersService getService() {
//
//        return super.build().create(MyOrdersService.class);
//    }
//
//    public interface MyOrdersService {
//        @POST("/")
//        void getOrderInfo(@Body
//                          HashMap<String, Object> bodyParameter,
//                          Callback<OrderHistoryResponse> callback);
//    }
//}
