package com.specimen.core.request;

import com.specimen.core.model.OrderHistoryResponse;
import com.specimen.core.response.OrderTrackingResponse;
import com.specimen.core.response.OrderedDetailsResponse;
import com.specimen.core.response.OrderedHistoryResponse;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by Intelliswift on 6/18/2015.
 */
public class OrderRequestBuilder extends RetroRequestBuilder {

    public OrderService getService() {

        return super.build().create(OrderService.class);
    }

    public interface OrderService {
        @POST("/")
        void getOrderInfo(@Body HashMap<String, Object> bodyParameter,
                          Callback<OrderHistoryResponse> callback);

        @POST("/")
        void getOrderDetail(@Body HashMap<String, Object> bodyParameter,
                            Callback<OrderedDetailsResponse> callback);

        @POST("/")
        void trackShipment(@Body HashMap<String, Object> bodyParameter, Callback<OrderTrackingResponse> callback);
    }
}
