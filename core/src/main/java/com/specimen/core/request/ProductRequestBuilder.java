package com.specimen.core.request;

import com.specimen.core.response.ProductDetailsResponse;
import com.specimen.core.response.ProductListResponse;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by Intelliswift on 6/18/2015.
 */
public class ProductRequestBuilder extends RetroRequestBuilder {

    public ProductService getService() {

        return super.build().create(ProductService.class);
    }

    public interface ProductService {

//        @POST("/getProduct")
@POST("/")
        void getProduct(@Body HashMap<String, Object> bodyParameter,
                        Callback<ProductListResponse> callback);


//        @POST("/getProductDetails")
        @POST("/")
        void getProductDetails(@Body HashMap<String, Object> bodyParameter,
                               Callback<ProductDetailsResponse> callback);


//        @POST("/getProduct")
        @POST("/")
        void searchProduct(@Body HashMap<String, Object> bodyParameter,
                           Callback<ProductListResponse> callback);

    }

}
