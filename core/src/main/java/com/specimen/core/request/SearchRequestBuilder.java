package com.specimen.core.request;

import com.specimen.core.response.AutoCompleteResponse;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by Intelliswift on 7/20/2015.
 */
public class SearchRequestBuilder extends RetroRequestBuilder {

    public SearchService getService() {

        return super.build().create(SearchService.class);
    }

    public interface SearchService {
        @POST("/")
        void getAutoCompleteSearchString(@Body HashMap<String, Object> bodyParameter,
                             Callback<AutoCompleteResponse> callback);

    }
}