package com.specimen.core.request;

import com.specimen.core.response.AddressResponse;
import com.specimen.core.response.SpyAndCartItemCountResponse;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by DhirajK on 11/28/2015.
 */
public class SpyAndCartItemCountRequestBuilder extends RetroRequestBuilder {

    public AddressService getService() {

        return super.build().create(AddressService.class);
    }

    public interface AddressService {

        @POST("/")
        void getTotalSpyCartCount(
                @Body HashMap<String, Object> bodyParameter,
                Callback<SpyAndCartItemCountResponse> callback);


    }

}