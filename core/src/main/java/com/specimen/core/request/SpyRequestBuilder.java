package com.specimen.core.request;

import com.specimen.core.response.AddOrRemoveFromWishListResponse;
import com.specimen.core.response.FavoriteListResponse;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by Intelliswift on 6/18/2015.
 */
public class SpyRequestBuilder extends RetroRequestBuilder {


    public SpyService getService() {

        return super.build().create(SpyService.class);
    }

    public interface SpyService {

//        @POST("/savetoWishlist")
@POST("/")
        void setSpied(@Body HashMap<String, Object> bodyParameter,
                      Callback<AddOrRemoveFromWishListResponse> callback);

//        @POST("/removeFromWishlist")
        @POST("/")
        void removeSpied(@Body HashMap<String, Object> bodyParameter,
                         Callback<AddOrRemoveFromWishListResponse> callback);

//        @POST("/getWishlistProducts")
        @POST("/")
        void spiedProductList(@Body HashMap<String, Object> bodyParameter,
                              Callback<FavoriteListResponse> callback);
    }

}
