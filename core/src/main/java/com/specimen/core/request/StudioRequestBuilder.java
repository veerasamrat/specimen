package com.specimen.core.request;

import com.specimen.core.model.BusyDatesResponse;
import com.specimen.core.model.SetAppointmentResponse;
import com.specimen.core.model.StudioResponseData;
import com.specimen.core.model.StudioSlotsResponse;
import com.specimen.core.response.StudioListResponse;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by Intelliswift on 7/20/2015.
 */
public class StudioRequestBuilder extends RetroRequestBuilder {

    public StudioService getService() {

        return super.build().create(StudioService.class);
    }

    public interface StudioService {

        //        @POST("/getStudios")
        @POST("/")
        void getStudios(@Body HashMap<String, Object> bodyParameter,
                        Callback<StudioListResponse> callback);

        //        @POST("/bookAppointment")
        @POST("/")
        void setAppointment(@Body HashMap<String, Object> bodyParameter,
                            Callback<SetAppointmentResponse> callback);


        //        @POST("/getTimeSlots")
        @POST("/")
        void getTimeSlots(@Body HashMap<String, Object> bodyParameter,
                          Callback<StudioSlotsResponse> callback);

        //        @POST("/storeDates")
        @POST("/")
        void getBusyDates(@Body HashMap<String, Object> bodyParameter,
                          Callback<BusyDatesResponse> callback);

    }

}
