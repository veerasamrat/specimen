package com.specimen.core.request;

import com.specimen.core.model.Answer;
import com.specimen.core.model.IsSurveyCompleteResponse;
import com.specimen.core.model.SurveyStyleProfileResponseData;
import com.specimen.core.response.AutoCompleteResponse;
import com.specimen.core.response.SurveyResponse;

import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by Intelliswift on 7/20/2015.
 */
public class SurveyRequestBuilder extends RetroRequestBuilder {

    public SurveyService getService() {

        return super.build().create(SurveyService.class);
    }

    public interface SurveyService {
//                @POST("/getSurveyQuestion")
        @POST("/")
        void getSurveyQuestion(@Body HashMap<String, Object> bodyParameter,
                               Callback<SurveyResponse> callback);


//                        @POST("/submitSurvey")
        @POST("/")
        void submitSurvey(@Body HashMap<String, Object> bodyParameter,
                          Callback<SurveyResponse> callback);

//                                @POST("/getstyleprofiledata")
        @POST("/")
        void getstyleprofiledata(@Body HashMap<String, Object> bodyParameter,
                                 Callback<SurveyStyleProfileResponseData> callback);

//                                @POST("/isSurveyCompleted")
        @POST("/")
        void isSurveyCompleted(@Body HashMap<String, Object> bodyParameter,
                               Callback<IsSurveyCompleteResponse> callback);

    }
}