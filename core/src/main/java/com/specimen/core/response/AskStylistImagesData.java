package com.specimen.core.response;

import java.util.List;

/**
 * Created by root on 14/9/15.
 */
public class AskStylistImagesData {


    /**
     * images : [{"filename":"avv.png","image_code":"base64"},{"filename":"avasdsadv.png","image_code":"base64"}]
     */

    private List<AskStylistImagesEntity> images;

    public void setImages(List<AskStylistImagesEntity> images) {
        this.images = images;
    }

    public List<AskStylistImagesEntity> getImages() {
        return images;
    }

    public class AskStylistImagesEntity {
        /**
         * filename : avv.png
         * image_code : base64
         */

        private String filename;
        private String image_code;

        public void setFilename(String filename) {
            this.filename = filename;
        }

        public void setImage_code(String image_code) {
            this.image_code = image_code;
        }

        public String getFilename() {
            return filename;
        }

        public String getImage_code() {
            return image_code;
        }
    }
}
