package com.specimen.core.response;

import java.util.List;

/**
 * Created by root on 5/9/15.
 */
public class    AskStylistResponse extends APIResponse{

    /**
     * data : [{"value":"106","label":"Clubbing"},{"value":"105","label":"Office Party"},{"value":"107","label":"Other"},{"value":"104","label":"Wedding"}]
     */

    private List<DataStylistOccassionEntity> data;

    public void setData(List<DataStylistOccassionEntity> data) {
        this.data = data;
    }

    public List<DataStylistOccassionEntity> getData() {
        return data;
    }

    public class DataStylistOccassionEntity {
        /**
         * value : 106
         * label : Clubbing
         */

        private String value;
        private String label;

        public void setValue(String value) {
            this.value = value;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getValue() {
            return value;
        }

        public String getLabel() {
            return label;
        }
    }
}
