package com.specimen.core.response;

import com.specimen.core.model.TopCategory;

import java.util.List;

/**
 * Created by Intelliswift on 6/23/2015.
 */
public class CategoryListResponse extends APIResponse {

    private List<TopCategory> data;

    public List<TopCategory> getData() {
        return data;
    }

    public void setData(List<TopCategory> data) {
        this.data = data;
    }


}
