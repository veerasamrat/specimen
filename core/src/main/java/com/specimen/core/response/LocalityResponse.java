package com.specimen.core.response;

import java.util.List;

/**
 * Created by root on 3/1/16.
 */
public class LocalityResponse extends APIResponse{


    /**
     * locality : [{"name":"S. K.Nagar "},{"name":"Daulat Nagar  (Mumbai)"},{"name":"Rajendra Nagar  (Mumbai)"},{"name":"Borivali East "},{"name":"Magthane "}]
     * city : Mumbai
     * state : MAHARASHTRA
     * country : INDIA
     */

    private DataEntity data;
    /**
     * data : {"locality":[{"name":"S. K.Nagar "},{"name":"Daulat Nagar  (Mumbai)"},{"name":"Rajendra Nagar  (Mumbai)"},{"name":"Borivali East "},{"name":"Magthane "}],"city":"Mumbai","state":"MAHARASHTRA","country":"INDIA"}
     * status : Success
     * message : Zipcode Data Found.
     */

    public void setData(DataEntity data) {
        this.data = data;
    }


    public DataEntity getData() {
        return data;
    }

    public static class DataEntity {
        private String city;
        private String state;
        private String country;
        /**
         * name : S. K.Nagar
         */

        private List<LocalityEntity> locality;

        public void setCity(String city) {
            this.city = city;
        }

        public void setState(String state) {
            this.state = state;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public void setLocality(List<LocalityEntity> locality) {
            this.locality = locality;
        }

        public String getCity() {
            return city;
        }

        public String getState() {
            return state;
        }

        public String getCountry() {
            return country;
        }

        public List<LocalityEntity> getLocality() {
            return locality;
        }

        public static class LocalityEntity {
            private String name;

            public void setName(String name) {
                this.name = name;
            }

            public String getName() {
                return name;
            }
        }
    }
}
