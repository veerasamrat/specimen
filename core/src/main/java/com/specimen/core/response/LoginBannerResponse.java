package com.specimen.core.response;

import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by Intelliswift on 6/22/2015.
 */
public class LoginBannerResponse extends APIResponse {

    @Expose
    private List<String> data ;

    public List<String> getData() {
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }
}
