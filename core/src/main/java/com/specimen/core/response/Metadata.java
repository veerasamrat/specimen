package com.specimen.core.response;

import java.util.HashMap;
import java.util.Map;

public class Metadata {

private String totalTax;
private String totalDiscount;
private String total;
private Map<String, Object> additionalProperties = new HashMap<>();

/**
* 
* @return
* The totalTax
*/
public String getTotalTax() {
return totalTax;
}

/**
* 
* @param totalTax
* The total_tax
*/
public void setTotalTax(String totalTax) {
this.totalTax = totalTax;
}

/**
* 
* @return
* The totalDiscount
*/
public String getTotalDiscount() {
return totalDiscount;
}

/**
* 
* @param totalDiscount
* The total_discount
*/
public void setTotalDiscount(String totalDiscount) {
this.totalDiscount = totalDiscount;
}

/**
* 
* @return
* The total
*/
public String getTotal() {
return total;
}

/**
* 
* @param total
* The total
*/
public void setTotal(String total) {
this.total = total;
}

public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}