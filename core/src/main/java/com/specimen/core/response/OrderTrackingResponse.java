package com.specimen.core.response;

/**
 * Created by DhirajK on 12/12/2015.
 */
public class OrderTrackingResponse extends APIResponse {

    private OrderTrackingResponseData data;

    public OrderTrackingResponseData getData() {
        return data;
    }

    public void setData(OrderTrackingResponseData data) {
        this.data = data;
    }


}
