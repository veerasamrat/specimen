package com.specimen.core.response;

/**
 * Created by DhirajK on 12/12/2015.
 */
public class OrderTrackingResponseData {


    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
