package com.specimen.core.response;

import com.specimen.core.model.ProductDetailResponseData;

public class ProductDetailsResponse extends APIResponse{

    private ProductDetailResponseData data;

    public void setData(ProductDetailResponseData data) {
        this.data = data;
    }

    public ProductDetailResponseData getData() {
        return data;
    }


}
