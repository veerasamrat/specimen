package com.specimen.core.response;

import com.specimen.core.model.Curated;
import com.specimen.core.model.ProductResponseData;

import java.util.List;

public class ProductListResponse extends APIResponse {

    private ProductResponseData data;


    public ProductResponseData getProductResponseData() {
        return data;
    }

    public void setProductResponseData(ProductResponseData data) {
        this.data = data;
    }





}