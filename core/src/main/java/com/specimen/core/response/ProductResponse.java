package com.specimen.core.response;

import com.google.gson.annotations.Expose;
import com.specimen.core.model.ProductListResponseData;

public class ProductResponse extends APIResponse {

    @Expose
    private ProductListResponseData data;

    /**
     * @return The data
     */
    public ProductListResponseData getWishedProductList() {
        return data;
    }

    /**
     * @param wishedProductList The data
     */
    public void setWishedProductList(ProductListResponseData wishedProductList) {
        this.data = wishedProductList;
    }


}