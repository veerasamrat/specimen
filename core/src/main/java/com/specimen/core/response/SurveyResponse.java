package com.specimen.core.response;

import com.specimen.core.model.SurveyResponseData;

/**
 * Created by Intelliswift on 7/20/2015.
 */
public class SurveyResponse extends APIResponse {

    /**
     * data : {"questions":[{"question":"How do you spend your weekend?","answer":{"id":"1","value":"BARS"},"options":[{"id":"1","value":"BARS"},{"id":"2","value":"PARTY"}],"id":"1"},{"question":"How do you spend your money?","answer":null,"options":[{"id":"1","value":"BARS"},{"id":"2","value":"PARTY"}],"id":"2"}]}
     * message :
     * status : success
     */
    private SurveyResponseData data;

    public void setData(SurveyResponseData data) {
        this.data = data;
    }


    public SurveyResponseData getData() {
        return data;
    }



}
