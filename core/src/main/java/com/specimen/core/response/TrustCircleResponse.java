package com.specimen.core.response;

import com.specimen.core.model.TrusteesEntity;

import java.util.List;

/**
 * Created by Intelliswift on 7/20/2015.
 */
public class TrustCircleResponse extends APIResponse {
    List<TrusteesEntity> data;

    public void setData(List<TrusteesEntity> data) {
        this.data = data;
    }

    public List<TrusteesEntity> getData() {
        return data;
    }


}
