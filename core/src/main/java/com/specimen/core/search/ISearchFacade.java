package com.specimen.core.search;

import com.specimen.core.IBaseFacade;
import com.specimen.core.model.SuggestionEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rohit on 23/09/15.
 */
public interface ISearchFacade extends IBaseFacade {

    List<SuggestionEntity> getHistory();

    void getAutoSearchCompleteText(String searchText);

    void addInHistory(String suggestion);

    void addInHistory(SuggestionEntity suggestionEntity);
}
