package com.specimen.core.search;

import com.specimen.core.BaseFacade;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.model.SuggestionEntity;
import com.specimen.core.request.SearchRequestBuilder;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.AutoCompleteResponse;

import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Rohit on 23/09/15.
 */
public class SearchFacade extends BaseFacade implements ISearchFacade, IResponseSubscribe {


    private final SearchRequestBuilder mSearchRequestBuilder;

    private final SearchPersistance mSearchPersistance;

    public SearchFacade(IResponseSubscribe responseSubscribe, IApplicationFacade applicationFacade) {
        super(responseSubscribe, applicationFacade);
        this.mSearchRequestBuilder = new SearchRequestBuilder();
        mSearchPersistance = new SearchPersistance();

    }

    @Override
    public List<SuggestionEntity> getHistory() {

        return mSearchPersistance.fetchAll();
    }

    @Override
    public void getAutoSearchCompleteText(String searchText) {
        HashMap<String, Object> bodyMap = getRequestBody("autoCompleteText");
        bodyMap.put("searchString", searchText);

        mSearchRequestBuilder.getService().getAutoCompleteSearchString(bodyMap, new Callback<AutoCompleteResponse>() {
            @Override
            public void success(AutoCompleteResponse autoCompleteResponse, Response response) {
                onSuccess(autoCompleteResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                onFailure(error);
            }
        });
    }

    @Override
    public void addInHistory(String suggestion) {

        SuggestionEntity suggestionEntity = new SuggestionEntity();
        suggestionEntity.setString(suggestion);
        suggestionEntity.setSug_id(null);
        suggestionEntity.setParent_id(0);
        mSearchPersistance.insertSuggestionEntitys(suggestionEntity);
    }

    @Override
    public void addInHistory(SuggestionEntity suggestionEntity) {
        if (suggestionEntity != null)
            mSearchPersistance.insertSuggestionEntitys(suggestionEntity);
    }


    @Override
    public void onSuccess(APIResponse response, String tag) {
        mResponseSubscribe.onSuccess(response, tag);
    }

    @Override
    public void onFailure(Exception error) {
        mResponseSubscribe.onFailure(error);
    }
}
