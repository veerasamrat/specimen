package com.specimen.core.search;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.specimen.core.model.SuggestionEntity;
import com.specimen.core.util.GSONHelper;
import com.specimen.core.util.SharedPreferenceHelper;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rohit on 9/28/2015.
 */
public class SearchPersistance {


    public List<SuggestionEntity> fetchAll() {
        return getSuggestionList();
    }


    public void insertSuggestionEntitys(SuggestionEntity suggestionEntity) {

        String addedTemp = GSONHelper.getInstance().toJson(suggestionEntity);


        String listString;
        List<SuggestionEntity> suggestions = getSuggestionList();
        if (suggestions != null) {
            suggestions.add(suggestionEntity);

            listString = GSONHelper.getInstance().toJson(suggestions);
        } else

        {
            ArrayList<SuggestionEntity> entities = new ArrayList<>();
            entities.add(suggestionEntity);
            listString = GSONHelper.getInstance().toJson(entities);
        }

        SharedPreferenceHelper.put(SharedPreferenceHelper.LOCAL, "History", listString);
    }


    private List<SuggestionEntity> getSuggestionList() {
        String listString = SharedPreferenceHelper.get(SharedPreferenceHelper.LOCAL, "History", null);

        if (listString != null) {
            Type type = new TypeToken<List<SuggestionEntity>>() {
            }.getType();
            List<SuggestionEntity> suggestions = new Gson().fromJson(listString, type);

            return suggestions;
        }
        return null;
    }


    public void clearSuggestionEntitys() {

        SharedPreferenceHelper.put(SharedPreferenceHelper.LOCAL, "History", null);
    }
}
