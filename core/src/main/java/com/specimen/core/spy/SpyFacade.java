package com.specimen.core.spy;

import com.specimen.core.BaseFacade;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.request.SpyRequestBuilder;
import com.specimen.core.response.AddOrRemoveFromWishListResponse;
import com.specimen.core.response.FavoriteListResponse;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Intelliswift on 7/10/2015.
 */
public class SpyFacade extends BaseFacade implements ISpyFacade {

    private SpyRequestBuilder mSpyRequestBuilder;

    public SpyFacade(IResponseSubscribe responseSubscribe, IApplicationFacade applicationFacade) {
        super(responseSubscribe, applicationFacade);
        mSpyRequestBuilder = new SpyRequestBuilder();
    }

    @Override
    public void setSpied(String productId) {//}, PageRequest pageRequest) {
        HashMap<String, Object> bodyParameter = getRequestBody("savetoWishlist");
        // pageRequest.addParameters(bodyParameter);
        bodyParameter.put("product_id", productId);//: 1
        mSpyRequestBuilder.getService().setSpied(bodyParameter, new Callback<AddOrRemoveFromWishListResponse>() {
            @Override
            public void success(AddOrRemoveFromWishListResponse favoriteListResponse, Response response) {
                mResponseSubscribe.onSuccess(favoriteListResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });

    }

    @Override
    public void removeSpied(String productId) {

        HashMap<String, Object> bodyParameter = getRequestBody("removeFromWishlist");
        bodyParameter.put("product_id", productId);//: 1
        mSpyRequestBuilder.getService().removeSpied(bodyParameter, new Callback<AddOrRemoveFromWishListResponse>() {
            @Override
            public void success(AddOrRemoveFromWishListResponse favoriteListResponse, Response response) {
                mResponseSubscribe.onSuccess(favoriteListResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });

    }

    @Override
    public void spiedProductList() {
        HashMap<String, Object> bodyParameter = getRequestBody("getWishlistProducts");
        mSpyRequestBuilder.getService().spiedProductList(bodyParameter, new Callback<FavoriteListResponse>() {
            @Override
            public void success(FavoriteListResponse favoriteListResponse, Response response) {
                mResponseSubscribe.onSuccess(favoriteListResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }
}
