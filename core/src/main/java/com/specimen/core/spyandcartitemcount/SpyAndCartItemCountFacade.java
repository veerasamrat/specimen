package com.specimen.core.spyandcartitemcount;

import com.specimen.core.BaseFacade;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.address.IAddressFacade;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.request.AddressRequestBuilder;
import com.specimen.core.request.SpyAndCartItemCountRequestBuilder;
import com.specimen.core.response.AddressResponse;
import com.specimen.core.response.SpyAndCartItemCountResponse;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by DhirajK on 11/28/2015.
 */
public class SpyAndCartItemCountFacade extends BaseFacade implements ISpyAndCartItemCountFacade {


    private final SpyAndCartItemCountRequestBuilder spyAndCartItemCountRequestBuilder;

    public SpyAndCartItemCountFacade(IResponseSubscribe responseSubscribe, IApplicationFacade applicationFacade) {
        super(responseSubscribe, applicationFacade);
        this.spyAndCartItemCountRequestBuilder = new SpyAndCartItemCountRequestBuilder();
    }

    @Override
    public void getTotalSpyCartCount() {

        spyAndCartItemCountRequestBuilder.getService().getTotalSpyCartCount(getRequestBody("getTotalSpyCartCount"), new Callback<SpyAndCartItemCountResponse>() {

            @Override
            public void success(SpyAndCartItemCountResponse spyAndCartItemCountResponse, Response response) {
                mResponseSubscribe.onSuccess(spyAndCartItemCountResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });

    }
}
