package com.specimen.core.studio;

import android.util.Log;

import com.specimen.core.BaseFacade;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.model.BusyDatesResponse;
import com.specimen.core.model.SetAppointmentResponse;
import com.specimen.core.model.StudioSlotsResponse;
import com.specimen.core.request.StudioRequestBuilder;
import com.specimen.core.response.StudioListResponse;

import java.util.HashMap;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by root on 23/8/15.
 */
public class StudioFacade extends BaseFacade implements IStudioFacade {

    StudioRequestBuilder mStudioRequestBuilder;

    public StudioFacade(IResponseSubscribe responseSubscribe, IApplicationFacade applicationFacade) {
        super(responseSubscribe, applicationFacade);
        this.mStudioRequestBuilder = new StudioRequestBuilder();
    }

    @Override
    public void getStudios() {

        mStudioRequestBuilder.getService().getStudios(getRequestBody("getStudios"), new Callback<StudioListResponse>() {
            @Override
            public void success(StudioListResponse favoriteListResponse, Response response) {
                mResponseSubscribe.onSuccess(favoriteListResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("NULL", error.toString());
                mResponseSubscribe.onFailure(error);
            }
        });

    }

    @Override
    public void getTimeSlots(String studioId, String date) {
        HashMap<String, Object> map = getRequestBody("getTimeSlots");
        map.put("studio_id", studioId);
        map.put("date", date);
        mStudioRequestBuilder.getService().getTimeSlots(map, new Callback<StudioSlotsResponse>() {
            @Override
            public void success(StudioSlotsResponse favoriteListResponse, Response response) {
                mResponseSubscribe.onSuccess(favoriteListResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }

    @Override
    public void getBusyDates(String studioId) {
        HashMap<String, Object> map = getRequestBody("storeDates");
        map.put("studio_id", studioId);
        mStudioRequestBuilder.getService().getBusyDates(map, new Callback<BusyDatesResponse>() {
            @Override
            public void success(BusyDatesResponse favoriteListResponse, Response response) {
                mResponseSubscribe.onSuccess(favoriteListResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }

    @Override
    public void setAppointment(String studioId, String slotId, String date) {
        HashMap<String, Object> map = getRequestBody("bookAppointment");
        map.put("studio_id", studioId);
        map.put("slot_id", slotId);
        map.put("date", date);
        mStudioRequestBuilder.getService().setAppointment(map, new Callback<SetAppointmentResponse>() {
            @Override
            public void success(SetAppointmentResponse favoriteListResponse, Response response) {
                mResponseSubscribe.onSuccess(favoriteListResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }
}
