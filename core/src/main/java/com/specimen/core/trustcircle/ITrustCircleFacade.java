package com.specimen.core.trustcircle;

import com.specimen.core.IBaseFacade;

import java.util.List;

/**
 * Created by root on 8/21/2015.
 */
public interface ITrustCircleFacade extends IBaseFacade{

    void getTrustCircle(String product_id);

    void addTrustee(String email, String name, String number);

    void deleteTrustee(String email);

    void sendProducttoTrustCircle(String product_id, List<String> trustee);

    void trusteeProductLikeNotification(String product_id);

}
