package com.specimen.core.trustcircle;

import com.specimen.core.BaseFacade;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.model.LikeNotificationResponse;
import com.specimen.core.request.TrustCircleRequestBuilder;
import com.specimen.core.response.TrustCircleResponse;

import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by DhirajK on 8/21/2015.
 */
public class TrustCircleFacade extends BaseFacade implements ITrustCircleFacade {

    private final TrustCircleRequestBuilder trustCircleRequestBuilder;

    public TrustCircleFacade(IResponseSubscribe responseSubscribe, IApplicationFacade applicationFacade) {
        super(responseSubscribe, applicationFacade);
        trustCircleRequestBuilder = new TrustCircleRequestBuilder();
    }

    @Override
    public void getTrustCircle(String product_id) {
        HashMap<String, Object> bodyParameter = getRequestBody("customerTrustCircle");
        bodyParameter.put("product_id", product_id);
        trustCircleRequestBuilder.getService().getTrustCircle(bodyParameter, new Callback<TrustCircleResponse>() {
            @Override
            public void success(TrustCircleResponse trustCircleResponse, Response response) {
                mResponseSubscribe.onSuccess(trustCircleResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }

    @Override
    public void addTrustee(String email, String name, String number) {
        HashMap<String, Object> bodyParameter = getRequestBody("addTrustee");
        bodyParameter.put("trustee_email", email);//: "vishal.patil@intelliswift.co.in",
        bodyParameter.put("trustee_name", name);//: "vishal",
        bodyParameter.put("trustee_no", number);//: "9586897458"
        trustCircleRequestBuilder.getService().addTrustee(bodyParameter, new Callback<TrustCircleResponse>() {
            @Override
            public void success(TrustCircleResponse trustCircleResponse, Response response) {
                mResponseSubscribe.onSuccess(trustCircleResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }

    @Override
    public void deleteTrustee(String email) {
        HashMap<String, Object> bodyParameter = getRequestBody("deleteTrustee");
        bodyParameter.put("trustee_email", email);
        trustCircleRequestBuilder.getService().deleteTrustee(bodyParameter, new Callback<TrustCircleResponse>() {
            @Override
            public void success(TrustCircleResponse trustCircleResponse, Response response) {
                mResponseSubscribe.onSuccess(trustCircleResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }

    @Override
    public void sendProducttoTrustCircle(String product_id, List<String> trustee) {
        HashMap<String, Object> bodyParameter = getRequestBody("sendProducttoTrustCircle");
        bodyParameter.put("product_id", product_id);
        bodyParameter.put("trustee",trustee);
        trustCircleRequestBuilder.getService().sendProducttoTrustCircle(bodyParameter, new Callback<TrustCircleResponse>() {
            @Override
            public void success(TrustCircleResponse trustCircleResponse, Response response) {
                mResponseSubscribe.onSuccess(trustCircleResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }

    @Override
    public void trusteeProductLikeNotification(String product_id) {
        HashMap<String, Object> bodyParameter = getRequestBody("trusteeProductLikeNotification");
        bodyParameter.put("product_id", product_id);
        trustCircleRequestBuilder.getService().trusteeProductLikeNotification(bodyParameter, new Callback<LikeNotificationResponse>() {
            @Override
            public void success(LikeNotificationResponse trustCircleResponse, Response response) {
                mResponseSubscribe.onSuccess(trustCircleResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }
}
