package com.specimen.core.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GSONHelper {

	private static Gson gson;

	public static Gson getInstance() {
		if (gson == null) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.setDateFormat("yyyy-mm-dd");
			gson = gsonBuilder.create();

		}

		return gson;
	}

	
	
	
}
