package com.specimen.core.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.specimen.core.IOCContainer;


public class SharedPreferenceHelper {

    public static final String APP_PROPERTIES = "App Properties";
	public static final String CACHED_RESPONSES = "Cached_Response";
	public static final String LOCAL = "Local_Response";
	public static final String UPCOMING_DELAS = "Upcoming_Deals";

    public static boolean put(String sharedPreference, String key, Object value) {
		SharedPreferences prefs = IOCContainer.getInstance().getContext()
				.getSharedPreferences(sharedPreference, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		if (value instanceof String) {
			String strValue = (String) value;
			editor.putString(key, strValue);
		} else if (value instanceof Integer) {
			Integer intValue = (Integer) value;
			editor.putInt(key, intValue);
		} else if (value instanceof Boolean) {
			Boolean boolValue = (Boolean) value;
			editor.putBoolean(key, boolValue);
		}
		return editor.commit();
	}

	public static String get(String sharedPreference, String key,
			String defaultValue) {
		SharedPreferences prefs = IOCContainer.getInstance().getContext()
				.getSharedPreferences(sharedPreference, Context.MODE_PRIVATE);
		return prefs.getString(key, defaultValue);
	}

	public static boolean remove(String sharedPreference, String key) {
		SharedPreferences prefs = IOCContainer.getInstance().getContext()
				.getSharedPreferences(sharedPreference, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.remove(key);
		return editor.commit();
	}

	public static boolean clear(String sharedPreference) {
		SharedPreferences prefs = IOCContainer.getInstance().getContext()
				.getSharedPreferences(sharedPreference, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.clear();
		return editor.commit();
	}

	public static boolean get(String sharedPreference, String key,
			boolean defaultValue) {
		SharedPreferences prefs = IOCContainer.getInstance().getContext()
				.getSharedPreferences(sharedPreference, Context.MODE_PRIVATE);
		return prefs.getBoolean(key, defaultValue);
	}

	public static int get(String sharedPreference, String key, int intValue) {
		SharedPreferences prefs = IOCContainer.getInstance().getContext()
				.getSharedPreferences(sharedPreference, Context.MODE_PRIVATE);
		return prefs.getInt(key, intValue);
	}

}
